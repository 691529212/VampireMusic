package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/25.
 */
public class PlayMode {
    int playMode;
    String name;
    String sing;

    public PlayMode( int playMode) {
//        this.name = name;
        this.playMode = playMode;
//        this.sing = sing;
    }

    public String getName() {
        return name;
    }

    public int getPlayMode() {
        return playMode;
    }

    public String getSing() {
        return sing;
    }
}
