package vampire.com.baidumusic.tools.dbtools;

import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.enums.AssignType;

import cn.bmob.v3.BmobObject;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/25.
 */
public class DBMyMusicList extends BmobObject{

    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;

    private String songId;

    private String songName;

    private String author;

    private String username;

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public DBMyMusicList(String author, int id, String songId, String songName, String username) {
        this.author = author;
        this.id = id;
        this.songId = songId;
        this.songName = songName;
        this.username = username;
    }


    public DBMyMusicList(String author, String songId, String songName) {
        this.author = author;
        this.songId = songId;
        this.songName = songName;
    }

    public String getAuthor() {
        return author;
    }

    public int getId() {
        return id;
    }

    public String getSongId() {
        return songId;
    }

    public String getSongName() {
        return songName;
    }
}
