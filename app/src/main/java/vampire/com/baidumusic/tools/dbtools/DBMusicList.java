package vampire.com.baidumusic.tools.dbtools;

import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.enums.AssignType;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/25.
 */
public class DBMusicList {

    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;

    private String author;// 歌手名

    private String song; // 歌曲名

    private String songId; // 歌曲ID;

    private String pic;

    public String getPic() {
        return pic;
    }

    private int playing;// 播放中

    public int isPlaying() {
        return playing;
    }

    public void setPlaying(int playing) {
        this.playing = playing;
    }

    public DBMusicList(String author, String song, String songId,String pic, int playing) {
        this.author = author;
        this.song = song;
        this.songId = songId;
        this.pic =pic;
        this.playing = playing;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }
}
