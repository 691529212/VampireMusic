package vampire.com.baidumusic.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dllo on 16/8/4.
 */
public class SPUtil {

    private static String Name = "vampire.com.baidumusic.tools.SAVE_DATA";

    public  static void saveMessage (Context context, String phoneNumber, String msg){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Name,Context.MODE_APPEND);
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString(phoneNumber,msg);
        editor.apply();
    }
    public static String getMessage (Context context, String phone) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Name,Context.MODE_APPEND);
        return  sharedPreferences.getString(phone,"");

    }
}
