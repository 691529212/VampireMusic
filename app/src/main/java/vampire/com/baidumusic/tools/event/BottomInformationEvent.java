package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/27.
 */
public class BottomInformationEvent {
    private String author;
    private String song;
    private String pic ;
    private boolean isPlaying;

    public boolean isPlaying() {
        return isPlaying;
    }

    public String getAuthor() {
        return author;
    }

    public String getPic() {
        return pic;
    }

    public String getSong() {
        return song;
    }

    public BottomInformationEvent(String author, String pic, String song , boolean isPlaying) {

        this.author = author;
        this.pic = pic;
        this.song = song;
        this.isPlaying = isPlaying;
    }

    public BottomInformationEvent() {
    }
}
