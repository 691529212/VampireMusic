package vampire.com.baidumusic.tools.dbtools;

import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.enums.AssignType;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/25.
 */
public class DBHistoryPlay {

    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;

    private String author;// 歌手名

    private String song; // 歌曲名

    private String songId; // 歌曲ID;

    public DBHistoryPlay(String author, String songId, String song) {
        this.author = author;
        this.songId = songId;
        this.song = song;
    }

    public String getAuthor() {
        return author;
    }

    public int getId() {
        return id;
    }

    public String getSong() {
        return song;
    }

    public String getSongId() {
        return songId;
    }

}
