package vampire.com.baidumusic.tools.dbtools;

import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.enums.AssignType;

import cn.bmob.v3.BmobObject;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/29.
 */
public class DBMyFavoriteList extends BmobObject{
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    private int id;
    private String listName;
    private String pic;
    private String url;
    private String username;

    public void setUrl(String url) {
        this.url = url;
    }

    public DBMyFavoriteList() {
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public int getTypeId() {
        return typeId;
    }

    private int typeId;

    public DBMyFavoriteList( String listName, String pic,  String url, int typeId, String username) {

        this.listName = listName;
        this.pic = pic;
        this.typeId = typeId;
        this.url = url;
        this.username = username;
    }

    public DBMyFavoriteList(String listName, String pic, String url, int typeId) {
        this.listName = listName;
        this.pic = pic;
        this.typeId =typeId;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getListName() {
        return listName;
    }

    public String getPic() {
        return pic;
    }

    public String getUrl() {
        return url;
    }
}
