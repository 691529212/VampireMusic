package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/27.
 */
public class IsSelected {
    boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public IsSelected() {
    }

    public IsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
