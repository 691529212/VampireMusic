package vampire.com.baidumusic.tools;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class TimeTurn {
    private static final String TAG = "Vampire_TimeTurn";
    private static String m;
    private static String ss;

    public static String TimeTurn(int ms) {
        int s = ms/1000;
        int min =s /60;
        int second =s%60;
        m =""+min;
        ss= ""+second;
        if (min<10){
            m = "0"+min;
        }
        if (second<10){
            ss = "0"+second;
        }
        String time =m+":"+ss;
        return time;
    }
}
