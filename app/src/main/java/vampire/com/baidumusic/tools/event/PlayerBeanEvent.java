package vampire.com.baidumusic.tools.event;

import vampire.com.baidumusic.fragment.player.PlayerBean;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class PlayerBeanEvent {
    PlayerBean playerBean;

    public PlayerBean getPlayerBean() {
        return playerBean;
    }

    public void setPlayerBean(PlayerBean playerBean) {
        this.playerBean = playerBean;
    }
}
