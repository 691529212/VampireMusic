package vampire.com.baidumusic.tools;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.IOException;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/20.
 */
public class MusicPlayer {
    private static MusicPlayer ourInstance;
    private Context context;
    private AudioManager audioManager;// AudioManager引用
    private MediaPlayer mediaPlayer;// MediaPlayer引用;

    private MusicPlayer(Context context,String url) {
        mediaPlayer = new MediaPlayer();
        audioManager = (AudioManager) context.getSystemService(context.AUDIO_SERVICE); // 系统服务 ???

        try {
            mediaPlayer.setDataSource(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.prepareAsync(); // 进入prepared状态(就绪状态)。
        mediaPlayer.start();
    }

    public static MusicPlayer getInstance(Context context, String url) {
        if (ourInstance == null) {
            synchronized (MusicPlayer.class) {
                if (ourInstance == null) {
                    ourInstance = new MusicPlayer(context,url);
                }
            }
        }
        return ourInstance;
    }


}
