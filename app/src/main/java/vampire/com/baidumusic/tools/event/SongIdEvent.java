package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class SongIdEvent {
    SongIdEventBean songIdEventBean;

    public SongIdEventBean getSongIdEventBean() {
        return songIdEventBean;
    }

    public void setSongIdEventBean(SongIdEventBean songIdEventBean) {
        this.songIdEventBean = songIdEventBean;
    }
}
