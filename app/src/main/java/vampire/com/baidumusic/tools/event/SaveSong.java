package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/27.
 */
public class SaveSong {
    private static final String TAG = "Vampire_SaveSong";
    private boolean aBoolean;

    public SaveSong(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public boolean isaBoolean() {

        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public SaveSong() {
    }
}
