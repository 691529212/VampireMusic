package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class ProgressBean {
    int progress;// 进度

    int time; // 当前时间

    int totalTime; // 总时间

    String songName;// 歌名

    String authour;//歌手名

    String pic; // 图片网址

    String lrc;

    Boolean state;// 播放状态

    Boolean selected; // 是否被收藏

    int mode; // 播放模式

    String musicId; // 歌曲id

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getSelected() {

        return selected;
    }

    public String getLrc() {
        return lrc;
    }

    public String getAuthour() {
        return authour;
    }

    public String getSongName() {
        return songName;
    }

    public String getPic() {
        return pic;
    }

    public Boolean getState() {
        return state;
    }


    public void setState(Boolean state) {
        this.state = state;
    }

    public ProgressBean(String authour, int progress, String songName, int time, int totalTime, String pic, String lrc, Boolean state, int mode, String musicId) {
        this.authour = authour;
        this.pic = pic;
        this.progress = progress;
        this.songName = songName;
        this.time = time;
        this.totalTime = totalTime;
        this.lrc = lrc;
        this.state = state;
        this.mode = mode;
        this.musicId = musicId;

    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getProgress() {
        return progress;
    }

    public int getTime() {
        return time;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public ProgressBean(int progress, int time, int totalTime) {

        this.progress = progress;
        this.time = time;
        this.totalTime = totalTime;
    }
}
