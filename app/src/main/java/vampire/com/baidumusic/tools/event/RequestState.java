package vampire.com.baidumusic.tools.event;

/**
 * Created vampires by *Vampire* on 16/9/8.
 */
public class RequestState {
    boolean isPlaying;

    public boolean isPlaying() {
        return isPlaying;
    }

    public RequestState(boolean isPlaying) {

        this.isPlaying = isPlaying;
    }
}
