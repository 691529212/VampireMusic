package vampire.com.baidumusic.tools.event;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class SongIdEventBean {
    private static final String TAG = "Vampire_EventDataBean";
    int position;

    List songIdList;
    List songName;
    List author;
    List<String> pic;

    public List<String> getPic() {
        return pic;
    }

    public void setPic(List<String> pic) {
        this.pic = pic;
    }

    public SongIdEventBean(List author, List<String> pic, int position, List songIdList, List songName) {

        this.author = author;
        this.pic = pic;
        this.position = position;
        this.songIdList = songIdList;
        this.songName = songName;
    }

    public List getAuthor() {
        return author;
    }

    public List getSongName() {
        return songName;
    }

    public SongIdEventBean(List author, int position, List songIdList, List songName) {

        this.author = author;
        this.position = position;
        this.songIdList = songIdList;
        this.songName = songName;
    }


    public int getPosition() {
        return position;
    }

    public List getSongIdList() {
        return songIdList;
    }

}
