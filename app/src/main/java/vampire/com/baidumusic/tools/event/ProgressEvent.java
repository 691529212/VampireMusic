package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class ProgressEvent {

    private ProgressBean progressBean;
    private NameSongIdBean nameSongIdBean;

    public void setNameSongIdBean(NameSongIdBean nameSongIdBean) {
        this.nameSongIdBean = nameSongIdBean;
    }

    public NameSongIdBean getNameSongIdBean() {
        return nameSongIdBean;
    }

    public ProgressBean getProgressBean() {
        return progressBean;
    }

    public void setProgressBean(ProgressBean progressBean) {
        this.progressBean = progressBean;
    }
}
