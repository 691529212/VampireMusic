package vampire.com.baidumusic.tools.event;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/27.
 */
public class NameSongIdBean {
    private String name ;
    private String songId;
    private  String songname;

    public String getName() {
        return name;
    }

    public String getSongId() {
        return songId;
    }

    public String getSongname() {
        return songname;
    }

    public NameSongIdBean(String name, String songId, String songname) {

        this.name = name;
        this.songId = songId;
        this.songname = songname;
    }
}
