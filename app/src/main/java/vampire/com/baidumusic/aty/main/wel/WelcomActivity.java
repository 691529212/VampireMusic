package vampire.com.baidumusic.aty.main.wel;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.base.BaseActivity;
import vampire.com.baidumusic.service.PlayerService;
import vampire.com.baidumusic.tools.event.PlayerStateEvent;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/24.
 */
public class WelcomActivity extends BaseActivity {
    private static final String TAG = "Vampire_Welcom";

    private PlayerService.PlayerBinder playerBinder;
    private CountDownTimer countDownTimer;
    private Toast toast;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            playerBinder = (PlayerService.PlayerBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected int setLayout() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void initView() {

        // 初始化Bmob
        Bmob.initialize(this,"6113211fabe54e10515c17d3d3270ed3");

        // 开启服务
        Intent playerService = new Intent(this, PlayerService.class);
        startService(playerService);



        PlayerStateEvent playerStateEvent = new PlayerStateEvent();
        EventBus.getDefault().post(playerStateEvent);


        // 倒计时
        countDownTimer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {
                String time = l / 1000 + "s";
                showToast(time);
            }
            @Override
            public void onFinish() {
                Intent intent = new Intent(WelcomActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
        ImageView imageView =bindView(R.id.iv_welcome);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                Intent intent = new Intent(WelcomActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

    @Override
    protected void initData() {
        // 默认登录系统账号
        BmobUser bmobUser =BmobUser.getCurrentUser();
        if (bmobUser==null){
                BmobUser my = new BmobUser();
                my.setUsername("0");
                my.setPassword("0");
                my.login(new SaveListener<BmobUser>() {
                    @Override
                    public void done(BmobUser o, BmobException e) {
                        if (e == null) {
                            Log.d(TAG, "默认登录成功");
                        }else {

                        }
                    }
                });
        }

    }

    public void showToast(String time) {

        if (toast == null) {
            toast = Toast.makeText(getApplicationContext(), time, Toast.LENGTH_SHORT);
        } else {
            toast.cancel();//关闭吐司显示
            toast = Toast.makeText(getApplicationContext(), time, Toast.LENGTH_SHORT);
        }
        toast.show();//重新显示吐司
    }
}