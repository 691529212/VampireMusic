package vampire.com.baidumusic.aty.main.game.plane.object;

import android.content.res.Resources;
import android.graphics.BitmapFactory;

import vampire.com.baidumusic.R;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public class BulletGoods extends GameGoods{
    public BulletGoods(Resources resources) {
        super(resources);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void initBitmap() {
        // TODO Auto-generated method stub
        bmp = BitmapFactory.decodeResource(resources, R.mipmap.bullet_goods);
        object_width = bmp.getWidth();
        object_height = bmp.getHeight();
    }
}