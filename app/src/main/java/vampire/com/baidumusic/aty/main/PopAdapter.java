package vampire.com.baidumusic.aty.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.dbtools.DBMusicList;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/23.
 */
public class PopAdapter extends BaseAdapter {
    private static final String TAG = "Vampire_PopAdapter";
    private Context context;
    private List<DBMusicList> list;

    public void setList(List<DBMusicList> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public PopAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list!=null?list.size():0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder;
        if (convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.popup_list,null);
            myViewHolder = new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        }else {
            myViewHolder= (MyViewHolder) convertView.getTag();
        }

        myViewHolder.author.setText(list.get(position).getAuthor());
        myViewHolder.song.setText(list.get(position).getSong());

        return convertView;
    }

    private class MyViewHolder {
        TextView song;
        TextView author;
        TextView del;
        public MyViewHolder(View view) {
            song = (TextView) view.findViewById(R.id.tv_pop_song);
            author = (TextView) view.findViewById(R.id.tv_pop_author);
            del = (TextView) view.findViewById(R.id.delete);

        }

    }
}
