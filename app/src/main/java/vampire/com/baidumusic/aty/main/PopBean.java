package vampire.com.baidumusic.aty.main;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/23.
 */
public class PopBean {
    private static final String TAG = "Vampire_PopBean";
    String id;
    String name;
    String song;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSong() {
        return song;
    }


    public PopBean(String id, String name, String song) {

        this.id = id;
        this.name = name;
        this.song = song;
    }
}
