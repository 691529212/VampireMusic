package vampire.com.baidumusic.aty.main.logon;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseActivity;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/28.
 */
public class LogOnActivity extends BaseActivity {
    private static final String TAG = "Vampire_LogOnActivity";

    @Override
    protected int setLayout() {
        return R.layout.activity_logon;
    }

    @Override
    protected void initView() {
        final EditText user =bindView(R.id.et_user_sign);
        final EditText passWord = bindView(R.id.et_password_sign);
        Button signIn = bindView(R.id.btn_sign);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (user.getText().length()>0&&passWord.getText().length()>0){

                   BmobUser bmobUser =new BmobUser();
                   bmobUser.setUsername(user.getText().toString());
                   bmobUser.setPassword(passWord.getText().toString());
                   bmobUser.signUp(new SaveListener<BmobUser>() {
                       @Override
                       public void done(BmobUser bmobUser, BmobException e) {
                            if (e==null){
                                Toast.makeText(LogOnActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
                                finish();
                            }else {
                                Toast.makeText(LogOnActivity.this, "e:" + e, Toast.LENGTH_LONG).show();
                            }

                       }
                   });
               }else {
                   Toast.makeText(LogOnActivity.this, "输入账号/密码,你地明白?", Toast.LENGTH_SHORT).show();
               }
            }
        });

    }

    @Override
    protected void initData() {

    }
}
