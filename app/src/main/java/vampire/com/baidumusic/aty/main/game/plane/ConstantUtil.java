package vampire.com.baidumusic.aty.main.game.plane;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public class ConstantUtil {
    // 恒定值
    public static int DIR_LEFT_UP = 1;
    public static int DIR_RIGHT_UP = 2;
    public static int DIR_LEFT_DOWN = 3;
    public static int DIR_RIGHT_DOWN = 4;
    public static int DIR_LEFT = 5;
    public static int DIR_RIGHT = 6;
    public static int TO_MAIN_VIEW = 7;
    public static int TO_END_VIEW = 8;
    public static int END_GAME = 9;

}
