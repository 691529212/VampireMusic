package vampire.com.baidumusic.aty.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import vampire.com.baidumusic.fragment.search.SearchFragment;

/**
 * Created vampires by *Vampire* on 16/8/30.
 */
   // 用于规避搜索页 editText 点击无效
public class ActivityTemp extends AppCompatActivity {
    private static final String TAG = "Vampire_ActivityTemp";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SearchFragment searchFragment = new SearchFragment();
        getSupportFragmentManager().beginTransaction().add(android.R.id.content,searchFragment).commit();
    }
}
