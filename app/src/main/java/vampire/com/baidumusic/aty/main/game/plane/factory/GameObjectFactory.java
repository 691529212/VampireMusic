package vampire.com.baidumusic.aty.main.game.plane.factory;

import android.content.res.Resources;

import vampire.com.baidumusic.aty.main.game.plane.object.BigPlane;
import vampire.com.baidumusic.aty.main.game.plane.object.BossBullet;
import vampire.com.baidumusic.aty.main.game.plane.object.BossPlane;
import vampire.com.baidumusic.aty.main.game.plane.object.BulletGoods;
import vampire.com.baidumusic.aty.main.game.plane.object.GameObject;
import vampire.com.baidumusic.aty.main.game.plane.object.MiddlePlane;
import vampire.com.baidumusic.aty.main.game.plane.object.MissileGoods;
import vampire.com.baidumusic.aty.main.game.plane.object.MyBullet;
import vampire.com.baidumusic.aty.main.game.plane.object.MyBullet2;
import vampire.com.baidumusic.aty.main.game.plane.object.MyPlane;
import vampire.com.baidumusic.aty.main.game.plane.object.SmallPlane;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public class GameObjectFactory {
    public GameObject createSmallPlane(Resources resources){
        return new SmallPlane(resources);
    }
    public GameObject createMiddlePlane(Resources resources){
        return new MiddlePlane(resources);
    }
    public GameObject createBigPlane(Resources resources){
        return new BigPlane(resources);
    }
    public GameObject createBossPlane(Resources resources){
        return new BossPlane(resources);
    }
    public GameObject createMyPlane(Resources resources){
        return new MyPlane(resources);
    }
    public GameObject createMyBullet(Resources resources){
        return new MyBullet(resources);
    }
    public GameObject createMyBullet2(Resources resources){
        return new MyBullet2(resources);
    }
    public GameObject createBossBullet(Resources resources){
        return new BossBullet(resources);
    }
    public GameObject createMissileGoods(Resources resources){
        return new MissileGoods(resources);
    }
    public GameObject createBulletGoods(Resources resources){
        return new BulletGoods(resources);
    }
}