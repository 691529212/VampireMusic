package vampire.com.baidumusic.aty.main.game.plane.object;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.Random;

import vampire.com.baidumusic.R;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public class BigPlane extends EnemyPlane {

    private static int currentCount = 0; // 当前
    public static int sumCount = 2; // 获取
    private Bitmap bigPlane; // 大个飞机

    public BigPlane(Resources resources) {
        super(resources);
        this.score = 3000;
    }

    // 大飞机参数
    @Override
    public void initial(int arg0, float arg1, float arg2) {
        isAlive = true;
        bloodVolume = 30;
        blood = bloodVolume;
        Random ran = new Random();
        speed = ran.nextInt(2) + 4 * arg0;
        object_x = ran.nextInt((int) (screen_width - object_width));
        object_y = -object_height * (currentCount * 2 + 1);
        currentCount++;
        if (currentCount >= sumCount) {
            currentCount = 0;
        }
    }

    // 大飞机图
    @Override
    protected void initBitmap() {
        bigPlane = BitmapFactory.decodeResource(resources, R.mipmap.big);
        object_width = bigPlane.getWidth();
        object_height = bigPlane.getHeight() / 5;
    }

    @Override
    public void drawSelf(Canvas canvas) {
        if (isAlive) {
            if (!isExplosion) {
                if (isVisible) {

                    canvas.save();
                    canvas.clipRect(object_x, object_y, object_x + object_width, object_y + object_height);
                    canvas.drawBitmap(bigPlane, object_x, object_y, paint);
                    canvas.restore();
                }
                logic();
            } else {
                int y = (int) (currentFrame * object_height);
                canvas.save();
                canvas.clipRect(object_x, object_y, object_x + object_width, object_y + object_height);
                canvas.drawBitmap(bigPlane, object_x, object_y = y, paint);
                canvas.restore();
                currentFrame++;
                if (currentFrame >= 5) {
                    currentFrame = 0;
                    isExplosion = false;
                    isAlive = false;
                }
            }
        }
    }

    @Override
    public void release() {
        if (!bigPlane.isRecycled()) {
            bigPlane.recycle();
        }
    }
}
