package vampire.com.baidumusic.aty.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import vampire.com.baidumusic.values.TitleValues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MainAdapter extends FragmentPagerAdapter{
    private static final String TAG = "Vampire_MainAdapter";

    private ArrayList<Fragment> fragments;

    public MainAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public int getCount() {
        return fragments!=null?fragments.size():0;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TitleValues.MAIN_TITLES[position];
    }
}
