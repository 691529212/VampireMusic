package vampire.com.baidumusic.aty.main.mvplayer;

import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.values.KeyValues;

/**
 * Created vampires by *Vampire* on 16/8/31.
 */
public class MVPlayerActivity extends BaseActivity implements MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener {
    private static final String TAG = "Vampire_MVPlayer";
    private CustomMediaController mMediaController;
    private VideoView mVideoView;
    private TextView downloadRateView;
    private TextView loadRateView;
    private ProgressBar pb;


    @Override
    protected int setLayout() {
        return R.layout.fragment_mv_play;
    }

    @Override
    protected void initView() {

        // 定义全屏参数
        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        // 获得当前窗体对象
        Window window = MVPlayerActivity.this.getWindow();
        // 设置当前窗体为全屏显示
        window.setFlags(flag, flag);
        // 必须写这个 初始化加载库文件
        Vitamio.initialize(this);
        // 设置视频解码监听
        if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this))
            return;
        Intent intent = getIntent();


        mVideoView = bindView(R.id.surface_view);
        mVideoView.setVideoPath(intent.getStringExtra(KeyValues.MV_URL));
        mMediaController = new CustomMediaController(this,mVideoView,this);//实例化控制器
        mMediaController.show(5000);//控制器显示5s后自动隐藏
        mVideoView.setMediaController(mMediaController);//绑定控制器
        mVideoView.setVideoQuality(MediaPlayer.VIDEOQUALITY_HIGH);//设置播放画质 高画质
        mVideoView.requestFocus();//取得焦点

        mVideoView.setOnInfoListener(this);
        mVideoView.setOnBufferingUpdateListener(this);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setPlaybackSpeed(1.0f);
            }
        });

    }


    @Override
    protected void initData() {
        downloadRateView = bindView(R.id.download_rate);
        loadRateView = bindView(R.id.load_rate);
        pb = bindView(R.id.probar);
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            // 开始加载
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                    pb.setVisibility(View.VISIBLE);
                    loadRateView.setText("");
                    downloadRateView.setVisibility(View.VISIBLE);
                    loadRateView.setVisibility(View.VISIBLE);
                }
                break;
            // 加载完成
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                mVideoView.start();
                pb.setVisibility(View.GONE);
                downloadRateView.setVisibility(View.GONE);
                loadRateView.setVisibility(View.GONE);
                break;
            // 加载中
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
                downloadRateView.setText("" + extra + "kb/s" + "  ");
                break;
        }
        return true;
    }

    // 回馈加载比例
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        loadRateView.setText(percent + "%");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //屏幕切换时，设置全屏
        if (mVideoView != null) {
            mVideoView.setVideoLayout(VideoView.VIDEO_LAYOUT_SCALE, 0);
        }
        super.onConfigurationChanged(newConfig);
    }
}
