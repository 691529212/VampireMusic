package vampire.com.baidumusic.aty.main.game.plane.object;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
abstract public class GameObject {
    protected int currentFrame; // 当前帧
    protected int speed; // 速度

    protected float object_x;
    protected float object_y;

    protected float object_width;
    protected float object_height;

    protected float screen_width; // 屏幕宽
    protected float screen_height; // 屏幕高

    protected boolean isAlive; // 是否存活;

    protected Paint paint; // 画笔;
    protected Resources resources; // 资源

    public GameObject(Resources resources) {
        this.resources = resources;
        this.paint = new Paint();
    }

    // 设置屏幕宽高
    public void setScreenWH(float screen_width, float screen_height) {
        this.screen_width = screen_width;
        this.screen_height = screen_height;
    }

    public void initial(int arg0, float arg1, float arg2) {
    }

    protected abstract void initBitmap();

    public abstract void drawSelf(Canvas canvas);

    public abstract void release();

    // 是否碰撞
    public boolean isCollide(GameObject obj) {
        return true;
    }

    public void logic() {
    }

    // 获取速度
    public int getSpeed() {
        return speed;
    }

    // 设置速度
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public float getObject_x(){
        return object_x;
    }

    public void setObject_x(float object_x) {
        this.object_x = object_x;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public float getObject_y() {
        return object_y;
    }

    public void setObject_y(float object_y) {
        this.object_y = object_y;
    }

    public float getObject_width() {
        return object_width;
    }

    public void setObject_width(float object_width) {
        this.object_width = object_width;
    }

    public float getObject_height() {
        return object_height;
    }

    public void setObject_height(float object_height) {
        this.object_height = object_height;
    }
}
