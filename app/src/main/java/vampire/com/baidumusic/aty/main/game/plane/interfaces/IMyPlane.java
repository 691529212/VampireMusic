package vampire.com.baidumusic.aty.main.game.plane.interfaces;

import android.graphics.Canvas;

import java.util.List;

import vampire.com.baidumusic.aty.main.game.plane.object.EnemyPlane;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public interface IMyPlane {
    public float getMiddle_x();
    public void setMiddle_x(float middle_x);
    public float getMiddle_y();
    public void setMiddle_y(float middle_y);
    public boolean isChangeBullet();
    public void setChangeBullet(boolean isChangeBullet);
    public void shoot(Canvas canvas, List<EnemyPlane> planes);
    public void initButtle();
    public void changeButtle();
}
