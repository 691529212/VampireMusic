package vampire.com.baidumusic.aty.main.signin;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.aty.main.logon.LogOnActivity;
import vampire.com.baidumusic.base.BaseActivity;
import vampire.com.baidumusic.tools.dbtools.DBMusicList;
import vampire.com.baidumusic.tools.dbtools.DBMyFavoriteList;
import vampire.com.baidumusic.tools.dbtools.DBMyMusicList;
import vampire.com.baidumusic.tools.dbtools.DBTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/28.
 */
public class SignInActivity extends BaseActivity {
    private static final String TAG = "Vampire_SingIn";

    @Override
    protected int setLayout() {
        return R.layout.fragment_sign_in;
    }

    @Override
    protected void initView() {
        final EditText user =bindView(R.id.et_user_sign);
        final EditText passWord = bindView(R.id.et_password_sign);
            Button signIn = bindView(R.id.btn_sign);
        TextView newUser = bindView(R.id.btn_log_on);
        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(SignInActivity.this,LogOnActivity.class);
                startActivityForResult(intent,101);
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.getText().length()>0&&passWord.getText().length()>0) {
                    final BmobUser bmobUser = new BmobUser();
                    bmobUser.setUsername(user.getText().toString());
                    bmobUser.setPassword(passWord.getText().toString());
                    bmobUser.login(new SaveListener<BmobUser>() {

                        @Override
                        public void done(BmobUser o, BmobException e) {
                            if (e == null) {
                                Toast.makeText(SignInActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                                try {
                                    DBTool.getInstance().cleanDBMusicList(DBMyMusicList.class);
                                    DBTool.getInstance().cleanDBMusicList(DBMyFavoriteList.class);
                                    DBTool.getInstance().cleanDBMusicList(DBMusicList.class);
                                    Thread.sleep(1000);
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                }
                                finish();
                            }else {
                                Toast.makeText(SignInActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else {
                    Toast.makeText(SignInActivity.this, "请输入账号/密码", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void initData() {

    }
}
