package vampire.com.baidumusic.aty.main.game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.view.WindowManager;

import vampire.com.baidumusic.aty.main.game.plane.ConstantUtil;
import vampire.com.baidumusic.aty.main.game.plane.sounds.GameSoundPool;
import vampire.com.baidumusic.aty.main.game.plane.view.EndView;
import vampire.com.baidumusic.aty.main.game.plane.view.MainView;
import vampire.com.baidumusic.aty.main.game.plane.view.ReadyView;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public class GameActivity extends Activity {
    private EndView endView;
    private MainView mainView;
    private ReadyView readyView;
    private GameSoundPool sounds;
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if(msg.what == ConstantUtil.TO_MAIN_VIEW){
                toMainView();
            }
            else  if(msg.what == ConstantUtil.TO_END_VIEW){
                toEndView(msg.arg1);
            }
            else  if(msg.what == ConstantUtil.END_GAME){
                endGame();
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sounds = new GameSoundPool(this);
        sounds.initGameSound();
        readyView = new ReadyView(this,sounds);

        setContentView(readyView);

    }
    public void toMainView(){
        if(mainView == null){
            mainView = new MainView(this,sounds);
        }
        setContentView(mainView);
        readyView = null;
        endView = null;
    }
    public void toEndView(int score){
        if(endView == null){
            endView = new EndView(this,sounds);
            endView.setScore(score);
        }
        setContentView(endView);
        mainView = null;
    }
    public void endGame(){
        if(readyView != null){
            readyView.setThreadFlag(false);
        }
        else if(mainView != null){
            mainView.setThreadFlag(false);
        }
        else if(endView != null){
            endView.setThreadFlag(false);
        }
        this.finish();
    }
    public Handler getHandler() {
        return handler;
    }
    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}