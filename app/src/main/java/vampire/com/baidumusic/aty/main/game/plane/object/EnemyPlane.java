package vampire.com.baidumusic.aty.main.game.plane.object;

import android.content.res.Resources;
import android.graphics.Canvas;

/**
 * Created vampires by *Vampire* on 16/9/2.
 */
public class EnemyPlane extends GameObject {

    protected int score; // 分数
    protected int blood; // 血量
    protected int bloodVolume; // 血总量
    protected boolean isExplosion; // 是否爆炸
    protected boolean isVisible; // 是否消失

    // 敌机
    public EnemyPlane(Resources resources) {
        super(resources);
        initBitmap();
    }

    @Override
    public void initial(int arg0, float arg1, float arg2) {
        super.initial(arg0, arg1, arg2);
    }

    @Override
    protected void initBitmap() {

    }

    @Override
    public void drawSelf(Canvas canvas) {

    }

    @Override
    public void release() {

    }

    // 逻辑
    @Override
    public void logic() {

        if (object_y < screen_height) { // 屏幕内
            object_y += speed;
        } else { // 屏幕边缘
            isAlive = false;
        }


        if (object_y + object_height > 0) { // 从屏幕中出现
            isVisible = true;
        } else {
            isVisible = false;
        }
    }

    // 伤害
    public void attacked(int harm) {
        blood -= harm;
        if (blood <= 0) {
            isExplosion = true;
        }
    }

    // 是否碰撞
    @Override
    public boolean isCollide(GameObject obj) {
        if (object_x <= obj.getObject_x() && object_x + object_width <= obj.getObject_x()) {
            return false;
        } else if (obj.getObject_x() <= object_x && obj.getObject_x() + obj.object_width <= object_x) {
            return false;
        } else if (object_y <= obj.getObject_y() && obj.getObject_y() + obj.getObject_height() <= object_y) {
            return false;
        }
        return true;
    }

    public boolean isCanCollide() {
        return isAlive && !isExplosion && isVisible;
    }

    public int getScore(){
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getBlood() {
        return blood;
    }

    public void setBlood(int bllod) {
        this.blood = bllod;
    }

    public int getBloodVolume() {
        return bloodVolume;
    }

    public void setBloodVolume(int bloodVolume) {
        this.bloodVolume = bloodVolume;
    }

    public boolean isExplosion() {
        return isExplosion;
    }

}
