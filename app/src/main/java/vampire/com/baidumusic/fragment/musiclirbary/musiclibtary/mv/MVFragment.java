package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.mv;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.aty.main.mvplayer.MVLoaderBean;
import vampire.com.baidumusic.aty.main.mvplayer.MVPlayerActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.event.PauseEvent;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MVFragment extends BaseFragment {
    private static final String TAG = "Vampire_MVFragment";
    private PullToRefreshGridView grapeGridView;
    private List<MvListBean.ResultBean.MvListBeen> mvListBeens;
    private int a = 1;

    @Override
    protected int setLayout() {
        return R.layout.fragment_music_libiry_fragment_mv_fragment;
    }

    @Override
    protected void initView() {
        grapeGridView = bindView(R.id.ggv_mv);
        final MvGGVAdapter adapter = new MvGGVAdapter(getContext());

        grapeGridView.setMode(PullToRefreshBase.Mode.BOTH);
        grapeGridView.setAdapter(adapter);
        mNetTool.getNetData(URLVlaues.MV_A + a + URLVlaues.MV_B, MvListBean.class, new NetTool.NetListener<MvListBean>() {
            @Override
            public void onSuccess(MvListBean mvListBean) {

                mvListBeens = mvListBean.getResult().getMv_list();

                adapter.setMvListBeens(mvListBeens);
            }

            @Override
            public void onError(String errorMsg) {
                Log.d(TAG, "网络问题");
            }
        });

        grapeGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
                mNetTool.getNetData(URLVlaues.MV_A + a + URLVlaues.MV_B, MvListBean.class, new NetTool.NetListener<MvListBean>() {
                    @Override
                    public void onSuccess(MvListBean mvListBean) {

                        mvListBeens = mvListBean.getResult().getMv_list();
                        adapter.setMvListBeens(mvListBeens);
                        grapeGridView.onRefreshComplete();
                    }

                    @Override
                    public void onError(String errorMsg) {
                        Log.d(TAG, "网络问题");
                    }
                });
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                a += 1;
                mNetTool.getNetData(URLVlaues.MV_A + a + URLVlaues.MV_B, MvListBean.class, new NetTool.NetListener<MvListBean>() {
                    @Override
                    public void onSuccess(MvListBean mvListBean) {

                        List<MvListBean.ResultBean.MvListBeen> mv_list = mvListBean.getResult().getMv_list();
                        adapter.addMvListBeens(mv_list);
                        mvListBeens.addAll(mv_list);
                        grapeGridView.onRefreshComplete();
                    }

                    @Override
                    public void onError(String errorMsg) {
                        Log.d(TAG, "网络问题");
                    }
                });
            }
        });
    }

    @Override
    protected void initData() {
        grapeGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mNetTool.getNetData(URLVlaues.MV_LOADER_A + mvListBeens.get(position).getMv_id() + URLVlaues.MV_LOADER_B, MVLoaderBean.class, new NetTool.NetListener<MVLoaderBean>() {
                    @Override
                    public void onSuccess(MVLoaderBean mvLoaderBean) {
                        if (mvLoaderBean != null &&
                                mvLoaderBean.getResult().getFiles().getValue31() != null &&
                                mvLoaderBean.getResult().getFiles().getValue31().getSource_path() != null &&
                                mvLoaderBean.getResult().getFiles().getValue31().getSource_path().indexOf("yinyuetai") >= 0) {

                            // 让音乐暂停
                            EventBus.getDefault().post(new PauseEvent());

                            Intent intent = new Intent(getActivity(), MVPlayerActivity.class);
                            intent.putExtra(KeyValues.MV_URL, mvLoaderBean.getResult().getFiles().getValue31().getSource_path());
                            startActivity(intent);
                        } else {
                            Toast.makeText(getContext(), "网址不对劲儿 , 点了也放不了", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(String errorMsg) {

                    }
                });

            }
        });
    }
}