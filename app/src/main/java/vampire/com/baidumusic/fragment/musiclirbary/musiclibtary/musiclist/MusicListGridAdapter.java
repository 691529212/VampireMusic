package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.musiclist;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class MusicListGridAdapter extends BaseAdapter {
    private static final String TAG = "Vampire_MusicListGridAdapter";
    private Context context;
    private List<MusicListBean.ContentBean> contentBeen;

    public void setContentBeen(List<MusicListBean.ContentBean> contentBeen) {
        this.contentBeen = contentBeen;
        notifyDataSetChanged();
    }

    public void addContentBeen(List<MusicListBean.ContentBean> contentBeenNew) {
        contentBeen.addAll(contentBeenNew);
        notifyDataSetChanged();
    }

    public MusicListGridAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contentBeen != null ? contentBeen.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return contentBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        MyViewHolder myViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_item_all_list, null);
            myViewHolder = new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        } else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }
        MusicListBean.ContentBean contentBean = contentBeen.get(position);
        myViewHolder.listener.setText(contentBean.getListenum());
        myViewHolder.listName.setText(contentBean.getTitle());
        myViewHolder.type.setText(contentBean.getTag());
        NetTool netTool = new NetTool();
        netTool.getImage(contentBean.getPic_300(), myViewHolder.pic);

        return convertView;
    }

    private class MyViewHolder {
        private TextView listener;
        private ImageView pic;
        private TextView listName;
        private TextView type;

        public MyViewHolder(View view) {
            listener = (TextView) view.findViewById(R.id.tv_list_listener);
            listName = (TextView) view.findViewById(R.id.tv_list_name);
            type = (TextView) view.findViewById(R.id.tv_list_type);
            pic = (ImageView) view.findViewById(R.id.iv_list_pic);
        }


    }
}
