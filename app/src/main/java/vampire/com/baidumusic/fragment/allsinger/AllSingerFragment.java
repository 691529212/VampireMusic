package vampire.com.baidumusic.fragment.allsinger;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import vampire.com.baidumusic.Interface.OnclickAllSingerPress;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.fragment.allsinger.singgermore.MoreHotSingerFragment;
import vampire.com.baidumusic.fragment.allsinger.singer.SingerFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class AllSingerFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Vampire_AllSingerActivity";
    private int[] layoutId;
    private AllSingerAdapter adapter;
    private ViewPager viewPager;

    private Bundle bundle;
    private MainActivity mainActivity;

    @Override
    protected int setLayout() {
        return R.layout.activity_all_singer;
    }

    @Override
    protected void initView() {

        adapter = new AllSingerAdapter(getContext());

        Button hotMore =bindView(R.id.btn_hot_singer_more);
            hotMore.setOnClickListener(this);
        ImageView back = bindView(R.id.iv_go_back);
        back.setOnClickListener(this);
        layoutId = new int[]{R.id.layout_chinese_man,R.id.layout_chinese_woman,R.id.layout_chinese_combo,
                    R.id.layout_europe_man,R.id.layout_europe_women,R.id.layout_europe_combo,
                    R.id.layout_bangzi_man,R.id.layout_bangzi_women,R.id.layout_bangzi_combo,
                    R.id.layout_japan_man,R.id.layout_japan_women,R.id.layout_japan_combo,
                    R.id.layout_else_singer};
        for (int i = 0; i < layoutId.length; i++) {
            RelativeLayout layout =bindView(layoutId[i]);
            layout.setOnClickListener(this);
        }
    }

    @Override
    protected void initData() {

        mNetTool.getNetData(URLVlaues.HOT_SINGER_A+"12"+URLVlaues.HOT_SINGER_B, AllSingerBean.class, new NetTool.NetListener<AllSingerBean>() {
            @Override
            public void onSuccess(AllSingerBean allSingerBean) {
                List<AllSingerBean.ArtistBean> artistBeen =  allSingerBean.getArtist();
                viewPager = bindView(R.id.view_pager_pop_singer);
                adapter.setArtistBeen(artistBeen);
                viewPager.setAdapter(adapter);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });

        //viewPager的点击事件
        adapter.setOnclickAllSingerPress(new OnclickAllSingerPress() {

            @Override
            public void onclickAllSingerPress(String id, String songsTotal, String albumsTotal, String avatarBig, String name) {

                mainActivity = (MainActivity) getActivity();
                DetailFragment detailFragment = new DetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(KeyValues.RANK_TYPE,id);
                bundle.putString(KeyValues.RANK_NAME, name);
                bundle.putString("songs_total", songsTotal);
                bundle.putString("albums_total", albumsTotal);
                bundle.putString(KeyValues.RANK_PIC, avatarBig);
                bundle.putInt(KeyValues.MY_TYPE_ID, 1);
                detailFragment.setArguments(bundle);
                mainActivity.updateView(detailFragment);
            }

        });

    }
    private static String[] strings ={"华语男歌手","华语女歌手","华语组合",
                        "欧美男歌手","欧美女歌手","欧美组合",
                        "韩国男歌手","韩国女歌手","韩国组合",
                        "日本男歌手","日本女歌手","日本组合","其他歌手"};

    @Override
    public void onClick(View v) {


       //貌似for循环更佳
        for (int i = 0; i <layoutId.length ; i++) {
            if (v.getId()==layoutId[i]){
                SingerFragment fragment =new SingerFragment();
                bundle = new Bundle();
                bundle.putString("url", URLVlaues.ALL_SINGER[i]);
                bundle.putString("title",strings[i]);
                mainActivity = (MainActivity) getActivity();
                fragment.setArguments(bundle);
                mainActivity.updateView(fragment);

            }
        }
        switch (v.getId()){
            case R.id.btn_hot_singer_more:
                MoreHotSingerFragment hotSingerFragment =new MoreHotSingerFragment();
                bundle =new Bundle();
                bundle.putInt(KeyValues.MORE_HOT_SINGER_A,90);
                bundle.putInt(KeyValues.MORE_HOT_SINGER_B,0);
                hotSingerFragment.setArguments(bundle);
                mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(hotSingerFragment);
                break;
            case R.id.iv_go_back:
                AllSingerFragment fragment =new AllSingerFragment();
                FragmentManager fragmentManager =getFragmentManager();
                fragmentManager.beginTransaction().remove(fragment);
                break;
        }

    }
}
