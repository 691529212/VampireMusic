package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.radio;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class RadioFragment extends BaseFragment {
    private static final String TAG = "Vampire_RadioFragment";

    @Override
    protected int setLayout() {
        return R.layout.fragment_music_libiry_radio;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
