package vampire.com.baidumusic.fragment.myfavorite;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.dbtools.DBMyMusicList;
import vampire.com.baidumusic.tools.dbtools.DBTool;
import vampire.com.baidumusic.tools.event.SongIdEvent;
import vampire.com.baidumusic.tools.event.SongIdEventBean;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/27.
 */
public class MyFavoriteFragment extends BaseFragment{
    private static final String TAG = "Vampire_MyFavorite";
    private ArrayList<DBMyMusicList> dbMyMusicLists;
    private List<String> author;
    private List<String> song;
    private List<String> songID;

    @Override
    protected int setLayout() {
        return R.layout.fragment_my_favorite;
    }

    @Override
    protected void initView() {
        ListView listView =bindView(R.id.lv_my_favorite);
        final MyFavoriteAdapter  adapter =new MyFavoriteAdapter(getContext());
        listView.setAdapter(adapter);
        author = new ArrayList<String>();
        song = new ArrayList<String>();
        songID = new ArrayList<String>();
        DBTool.getInstance().queryDBMyMusicList(new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {

                dbMyMusicLists = (ArrayList<DBMyMusicList>) list;
                adapter.setDbMyMusicLists(dbMyMusicLists);
                for (DBMyMusicList dbMyMusicList : dbMyMusicLists) {
                    author.add(dbMyMusicList.getAuthor());
                    song.add(dbMyMusicList.getSongName());
                    songID.add(dbMyMusicList.getSongId());

                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SongIdEvent songIdEvent =new SongIdEvent();
                SongIdEventBean songIdEventBean =new SongIdEventBean(author, position,songID,song);
                songIdEvent.setSongIdEventBean(songIdEventBean);
                EventBus.getDefault().post(songIdEvent);
            }
        });
    }

    @Override
    protected void initData() {

    }
}
