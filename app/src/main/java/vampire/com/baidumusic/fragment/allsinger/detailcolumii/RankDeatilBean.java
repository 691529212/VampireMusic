package vampire.com.baidumusic.fragment.allsinger.detailcolumii;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/19.
 */
public class RankDeatilBean {
    private static final String TAG = "Vampire_RankDeatilBean";

    /**
     * billboard_type : 1
     * billboard_no : 1926
     * update_date : 2016-08-19
     * billboard_songnum : 198
     * havemore : 1
     * name : 新歌榜
     * comment : 该榜单是根据百度音乐平台歌曲每日播放量自动生成的数据榜单，统计范围为近期发行的歌曲，每日更新一次
     * pic_s640 : http://c.hiphotos.baidu.com/ting/pic/item/f7246b600c33874495c4d089530fd9f9d62aa0c6.jpg
     * pic_s444 : http://d.hiphotos.baidu.com/ting/pic/item/78310a55b319ebc4845c84eb8026cffc1e17169f.jpg
     * pic_s260 : http://b.hiphotos.baidu.com/ting/pic/item/e850352ac65c1038cb0f3cb0b0119313b07e894b.jpg
     * pic_s210 : http://business.cdn.qianqian.com/qianqian/pic/bos_client_c49310115801d43d42a98fdc357f6057.jpg
     * web_url : http://music.baidu.com/top/new
     */

    private BillboardBean billboard;
    /**
     * song_list : [{"artist_id":"14947058","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/518827cd2888633ac49f1fafc7002913/268809938/268809938.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/07e059f82a74da54cdce398b5e422fe9/268809943/268809943.jpg","country":"内地","area":"0","publishtime":"2016-08-12","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/b2b1ba655f1a9056020bf3700291a79b/268810692/268810692.lrc","copy_type":"1","hot":"285413","all_artist_ting_uid":"10559334","resource_type":"0","is_new":"1","rank_change":"0","rank":"1","all_artist_id":"14947058","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"-1|-1\"}","title":"树读","song_id":"268810037","author":"王俊凯","havehigh":2,"album_title":"树读","ting_uid":"10559334","album_id":"268810041","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"1","resource_type_ext":"1","mv_provider":"0000000000","artist_name":"王俊凯"},{"artist_id":"242771356","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/08df0af1b5428ff1898a680beb03e0e7/267994979/267994979.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/3408393b1e24c7c732297d23f5dee40b/267994982/267994982.jpg","country":"内地","area":"0","publishtime":"2016-07-25","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/1d346c8c9d58d75df17e35e855dc7b0c/268177347/268177347.lrc","copy_type":"1","hot":"544084","all_artist_ting_uid":"209940274","resource_type":"0","is_new":"1","rank_change":"0","rank":"2","all_artist_id":"242771356","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"-1|-1\"}","title":"逍遥","song_id":"268000667","author":"王青","havehigh":2,"album_title":"逍遥","ting_uid":"209940274","album_id":"267994993","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"1","resource_type_ext":"1","mv_provider":"0000000000","artist_name":"王青"},{"artist_id":"240752964","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/4d47a8c4056eb8802670dee0fa2d29e6/268591168/268591168.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/2a356dbf08b7039ebb385a1399e54f04/268591171/268591171.jpg","country":"内地","area":"0","publishtime":"2016-08-09","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/d5da6ea9a8f508fb1707226d742a4cbd/268591314/268591314.lrc","copy_type":"1","hot":"182643","all_artist_ting_uid":"209670348","resource_type":"0","is_new":"1","rank_change":"1","rank":"3","all_artist_id":"240752964","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"微微一笑很倾城","song_id":"268591208","author":"杨洋","havehigh":2,"album_title":"微微一笑很倾城","ting_uid":"209670348","album_id":"268591211","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"杨洋"},{"artist_id":"15","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/ed06f769e043d98f87443036b2c5b909/267778306/267778306.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/8100fdceeaed71940819d0593185be7f/267778310/267778310.jpg","country":"港台","area":"1","publishtime":"2016-07-22","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/45b7a43e127b6e0fe034b07a2e7ec276/267900666/267900666.lrc","copy_type":"1","hot":"521649","all_artist_ting_uid":"45561","resource_type":"0","is_new":"1","rank_change":"-1","rank":"4","all_artist_id":"15","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"尘埃","song_id":"267778516","author":"王菲","havehigh":2,"album_title":"尘埃","ting_uid":"45561","album_id":"267778552","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"王菲"},{"artist_id":"123339593","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/013f5d44ff9444fbebdba3cc3e08a7fe/268925739/268925739.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/8d976cc8b9000e353ddc058b2feb1ee7/268925742/268925742.jpg","country":"内地","area":"0","publishtime":"2016-08-05","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/93a4fb3db0614e47ebee122b3f9c07b3/268927791/268927791.lrc","copy_type":"1","hot":"140232","all_artist_ting_uid":"163361619","resource_type":"0","is_new":"1","rank_change":"2","rank":"5","all_artist_id":"123339593","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"下一秒","song_id":"268925443","author":"张碧晨","havehigh":2,"album_title":"下一秒","ting_uid":"163361619","album_id":"268925841","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"张碧晨"},{"artist_id":"73356250","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/977443fae7ed77dfb537fc3fcddb55d6/268583421/268583421.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/9f30fc6f8150c1c508d799b63eb70aa1/268583428/268583428.jpg","country":"其他","area":"4","publishtime":"2016-08-07","album_no":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/55bf3dbac35151e56135aed2b696dda9/268583424/268583424.lrc","copy_type":"1","hot":"137205","all_artist_ting_uid":"0","resource_type":"0","is_new":"1","rank_change":"0","rank":"6","all_artist_id":"73356250","style":"","del_status":"0","relate_status":"1","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":239,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"《典狱司》老九门电视剧片尾曲","song_id":"74156258","author":"音频怪物","havehigh":2,"album_title":"","ting_uid":"110961690","album_id":"0","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"yyr","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"音频怪物"},{"artist_id":"14031878","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/389e8041df9af0f26877fd4fe018af71/268421428/268421428.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/059baf08398a2c026c7812619f977878/268421431/268421431.jpg","country":"内地","area":"0","publishtime":"2016-08-05","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/9a16caf81f6aa60cb965f649cc68ee7f/268817651/268817651.lrc","copy_type":"1","hot":"124219","all_artist_ting_uid":"92452450,210095279,210016633,239564167,232953329","resource_type":"0","is_new":"1","rank_change":"1","rank":"7","all_artist_id":"14031878,242964591,242874265,268423194,245875579","style":"影视原声,流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"一笑倾城（合唱版）","song_id":"268423356","author":"张赫,白宇,张彬彬,崔航,郑业成","havehigh":2,"album_title":"微微一笑很倾城 电视原声","ting_uid":"92452450","album_id":"268424279","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"张赫,白宇,张彬彬,崔航,郑业成"},{"artist_id":"120870001","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/0bc3bc8537072826f862a0c3dc8d2fd6/267915045/267915045.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/96fdc6afe99223f067a8f96433f6d0f4/267915048/267915048.jpg","country":"内地","area":"0","publishtime":"2016-07-22","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/e8acc62de62fe9f058b41ea268f661b1/267915181/267915181.lrc","copy_type":"1","hot":"315723","all_artist_ting_uid":"147308161","resource_type":"0","is_new":"1","rank_change":"1","rank":"8","all_artist_id":"120870001","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"-1|-1\"}","title":"从此以后","song_id":"267915054","author":"吴亦凡","havehigh":2,"album_title":"从此以后","ting_uid":"147308161","album_id":"267915082","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"1","resource_type_ext":"1","mv_provider":"0000000000","artist_name":"吴亦凡"},{"artist_id":"163","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/9154bcf4708f1951bc2fca50fa7e795b/268316436/268316436.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/143fd3eecbb2b908688e76cdc410ea89/268316439/268316439.jpg","country":"内地","area":"0","publishtime":"2016-08-04","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/4a4bea5bebd90b26cac42b807eff6401/268333981/268333981.lrc","copy_type":"1","hot":"111123","all_artist_ting_uid":"1117","resource_type":"0","is_new":"1","rank_change":"1","rank":"9","all_artist_id":"163","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"爱在星空下","song_id":"268316624","author":"齐秦","havehigh":2,"album_title":"爱在星空下 电影原声带","ting_uid":"1117","album_id":"268316577","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"齐秦"},{"artist_id":"245926382","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/290e530f01e87abb3bc4637186b74b9b/268428256/268428256.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/9ebd8a8fff7afad2c3a2931d789baa79/268428259/268428259.jpg","country":"内地","area":"0","publishtime":"2016-08-05","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/a2bab43c38fdf2670e35ba2a84664c87/268822250/268822250.lrc","copy_type":"1","hot":"97415","all_artist_ting_uid":"213290149","resource_type":"0","is_new":"1","rank_change":"2","rank":"10","all_artist_id":"245926382","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"悟空","song_id":"268428594","author":"杨宝心","havehigh":2,"album_title":"悟空","ting_uid":"213290149","album_id":"268428624","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"杨宝心"},{"artist_id":"134","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/0f6479eb3de3784466f3ef8df31671d3/267453692/267453692.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/727b65fd994a4581db8a1b1ee8c7f1ac/267453695/267453695.jpg","country":"内地","area":"0","publishtime":"2016-07-11","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/63a2c1fab63cca1ef9a717cf809adcef/267453847/267453847.lrc","copy_type":"1","hot":"432648","all_artist_ting_uid":"1103","resource_type":"0","is_new":"0","rank_change":"0","rank":"11","all_artist_id":"134","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"影视原声","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"还魂门","song_id":"267453821","author":"胡彦斌","havehigh":2,"album_title":"还魂门","ting_uid":"1103","album_id":"267453699","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"胡彦斌"},{"artist_id":"1814","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/e696d6c4da68bfb9ab61d1fc279c60e2/267472922/267472922.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/d25fb9f6c06a362b9f5f67810edec2ad/267472925/267472925.jpg","country":"港台","area":"1","publishtime":"2016-07-10","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/fb770e284785b7b4cf2e2965b9c9fb63/267473026/267473026.lrc","copy_type":"1","hot":"658640","all_artist_ting_uid":"7898","resource_type":"0","is_new":"0","rank_change":"-7","rank":"12","all_artist_id":"1814","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"夜空中最亮的星","song_id":"267473025","author":"G.E.M.邓紫棋","havehigh":2,"album_title":"夜空中最亮的星","ting_uid":"7898","album_id":"267472929","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"G.E.M.邓紫棋"},{"artist_id":"58071","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/46f3f02e8b9872ea20020b9b09256a7d/268028080/268028080.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/746e98137aa1d4774d5eedac27248f96/268028083/268028083.jpg","country":"内地","area":"0","publishtime":"2016-07-16","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/1927497a0c73899ee83358e5f6df1a53/268028112/268028112.lrc","copy_type":"1","hot":"355981","all_artist_ting_uid":"2915785","resource_type":"0","is_new":"0","rank_change":"0","rank":"13","all_artist_id":"58071","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"歌在飞","song_id":"268028104","author":"苏勒亚其其格","havehigh":2,"album_title":"歌在飞","ting_uid":"2915785","album_id":"268028103","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"苏勒亚其其格"},{"artist_id":"14","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/4a6ee37f34e69a34b8b5645a91517d5e/268791166/268791166.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/f8d47974c6db88551b9ae1dfe8d11804/268791169/268791169.jpg","country":"内地","area":"0","publishtime":"2016-08-12","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/11b3d04eb855b36cf425ed626c784c5e/268792351/268792351.lrc","copy_type":"1","hot":"71738","all_artist_ting_uid":"1035","resource_type":"0","is_new":"1","rank_change":"4","rank":"14","all_artist_id":"14","style":"电子,影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"Lost In The Stars","song_id":"268791350","author":"张杰","havehigh":2,"album_title":"Lost In The Stars","ting_uid":"1035","album_id":"268791363","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"张杰"},{"artist_id":"32","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/d4d5e4f02a466fb79c81ff4c90d324d0/268582386/268582386.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/5b16b337ec585cd86024423be31496aa/268582389/268582389.jpg","country":"内地","area":"0","publishtime":"2016-08-09","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/f3c4ad6c83571037563380fe4e82ea12/268590386/268590386.lrc","copy_type":"1","hot":"65565","all_artist_ting_uid":"1045","resource_type":"0","is_new":"1","rank_change":"4","rank":"15","all_artist_id":"32","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"烈火神盾","song_id":"268582461","author":"魏晨","havehigh":2,"album_title":"烈火神盾","ting_uid":"1045","album_id":"268582532","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"魏晨"},{"artist_id":"246386992","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/7505daa9340d8fe7f83590f1f1fc0869/268313799/268313799.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/d62a2ddc3f38ba2f5ed48ad5a96d20b2/268313802/268313802.jpg","country":"内地","area":"0","publishtime":"2016-08-03","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/6cc4ca941ff81595cbcd3120dd5d5d9a/268767952/268767952.lrc","copy_type":"1","hot":"68201","all_artist_ting_uid":"232953783","resource_type":"0","is_new":"1","rank_change":"-1","rank":"16","all_artist_id":"246386992","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"陪我一起做梦","song_id":"268295799","author":"黄致列","havehigh":2,"album_title":"陪我一起做梦","ting_uid":"232953783","album_id":"268295977","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"黄致列"},{"artist_id":"88","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","publishtime":"2016-07-15","album_no":"10","lrclink":"http://musicdata.baidu.com/data2/lrc/3c65817e138bb6989b7a27b54f23ce3a/265795148/265795148.lrc","copy_type":"1","hot":"316869","all_artist_ting_uid":"2517","resource_type":"0","is_new":"0","rank_change":"-3","rank":"17","all_artist_id":"88","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"下雨了","song_id":"242078465","author":"薛之谦","havehigh":2,"album_title":"初学者","ting_uid":"2517","album_id":"241838068","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"薛之谦"},{"artist_id":"892","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/cef2448ff479ccde4388d1de41a6cb3e/268006477/268006477.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/3cc9e59df58f202c3b727b2da60c2e6f/268006480/268006480.jpg","country":"内地","area":"0","publishtime":"2016-07-27","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/c1765435b38bfc483fa2dbbfe4b342cb/268178691/268178691.lrc","copy_type":"1","hot":"178752","all_artist_ting_uid":"8553,1107","resource_type":"0","is_new":"1","rank_change":"-2","rank":"18","all_artist_id":"892,141","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"Flow","song_id":"268006623","author":"方大同,王力宏","havehigh":2,"album_title":"Flow","ting_uid":"8553","album_id":"268006646","charge":0,"is_first_publish":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"1000000000","artist_name":"方大同,王力宏"},{"artist_id":"242632271","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/d6b8602382133cbbcb9d5c201e358069/267894327/267894327.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eb10023496de7687beb50f8334201662/267894332/267894332.jpg","country":"内地","area":"0","publishtime":"2016-07-22","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/f6a89c0cb67ee821a5b6717bb6908e3f/267996470/267996470.lrc","copy_type":"1","hot":"171124","all_artist_ting_uid":"209709782","resource_type":"0","is_new":"1","rank_change":"-2","rank":"19","all_artist_id":"242632271","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"小幸运","song_id":"267894474","author":"简弘亦","havehigh":2,"album_title":"小幸运","ting_uid":"209709782","album_id":"267894476","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"简弘亦"},{"artist_id":"549","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/81fba7d75067700dbb11139b6e281b47/268108042/268108042.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/5c9b2eb83a206489fd5457e6d4302f61/268108045/268108045.jpg","country":"内地","area":"0","publishtime":"2016-07-28","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/cb127b68312daf606187d611f3896e15/268641615/268641615.lrc","copy_type":"1","hot":"167861","all_artist_ting_uid":"1322","resource_type":"0","is_new":"1","rank_change":"0","rank":"20","all_artist_id":"549","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"一生所爱 (新编曲版)","song_id":"268108410","author":"韩庚","havehigh":2,"album_title":"一生所爱 (新编曲版)","ting_uid":"1322","album_id":"268108514","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"韩庚"},{"artist_id":"5734662","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/f091094c60b2d5c0aa56cec82ac1352f/268248617/268248617.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/59f7bd9df50ccf467343e22eae43a43e/268248620/268248620.jpg","country":"内地","area":"0","publishtime":"2016-08-01","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/f2bdc3badb0042db05140afab64d9373/268249256/268249256.lrc","copy_type":"1","hot":"55013","all_artist_ting_uid":"821050","resource_type":"0","is_new":"1","rank_change":"1","rank":"21","all_artist_id":"5734662","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"草原之夜","song_id":"268249255","author":"乌兰图雅","havehigh":2,"album_title":"草原之夜","ting_uid":"821050","album_id":"268249267","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"乌兰图雅"},{"artist_id":"64279851","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/1379487e249532a7a0608eb46cb8f7d7/268315600/268315600.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/76f6198c46abc8e923a420a2d83df500/268315603/268315603.jpg","country":"内地","area":"0","publishtime":"2016-08-03","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/f3d9f0b5a526c9c4b0449600f8639b4c/268801232/268801232.lrc","copy_type":"1","hot":"55414","all_artist_ting_uid":"92456597","resource_type":"0","is_new":"1","rank_change":"-1","rank":"22","all_artist_id":"64279851","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"-1|-1\"}","title":"To Be Free","song_id":"268315838","author":"华晨宇","havehigh":2,"album_title":"To Be Free","ting_uid":"92456597","album_id":"268315845","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"1","resource_type_ext":"1","mv_provider":"0000000000","artist_name":"华晨宇"},{"artist_id":"68","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/f09570f2e131e76df6e58d4148bcb3e2/268544465/268544465.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/daee619c49de62e8c563242f883ee12a/268544468/268544468.jpg","country":"内地","area":"0","publishtime":"2016-08-05","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/f43211d851dd3137e759cc14150f743e/268817428/268817428.lrc","copy_type":"1","hot":"48505","all_artist_ting_uid":"1062","resource_type":"0","is_new":"1","rank_change":"1","rank":"23","all_artist_id":"68","style":"摇滚","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"Blah Blah Blah","song_id":"268417213","author":"谭维维","havehigh":2,"album_title":"夏长","ting_uid":"1062","album_id":"268417286","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"谭维维"},{"artist_id":"16","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/b31e348a55ca6baf6313c2cdb8194399/267534308/267534308.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/a6b05fb08a92da97ece397ed31b13198/267534311/267534311.jpg","country":"内地","area":"0","publishtime":"2016-07-12","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/9085c8bf90bf2a4ccdb491e30a70d251/267707119/267707119.lrc","copy_type":"1","hot":"216846","all_artist_ting_uid":"1036","resource_type":"0","is_new":"0","rank_change":"-1","rank":"24","all_artist_id":"16","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":1,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"-1|-1\"}","title":"手花","song_id":"267534409","author":"马天宇","havehigh":2,"album_title":"手花","ting_uid":"1036","album_id":"267534315","charge":0,"is_first_publish":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"1","resource_type_ext":"1","mv_provider":"0100000000","artist_name":"马天宇"},{"artist_id":"73349119","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/33466ad24f895e99bd04dcc95054e811/268375986/268375986.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/463575b11cb694f14d5014c8dd8cf772/268375998/268375998.jpg","country":"其他","area":"4","publishtime":"2016-08-02","album_no":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/c3021468b36fa7bc36bde449b3374c4f/268375505/268375505.lrc","copy_type":"1","hot":"45518","all_artist_ting_uid":"0","resource_type":"0","is_new":"1","rank_change":"0","rank":"25","all_artist_id":"73349119","style":"","del_status":"0","relate_status":"1","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":263,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"《山脉》盗墓笔记电影宣传曲","song_id":"74155552","author":"小爱的妈","havehigh":2,"album_title":"","ting_uid":"110954559","album_id":"0","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"yyr","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"小爱的妈"},{"artist_id":"2033","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/52767c2603c4e24e91e5b94152869caf/268720619/268720619.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/a43df6a22c148900c8452b57da2c68c5/268720646/268720646.jpg","country":"内地","area":"0","publishtime":"2016-08-11","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/9f8a7ff580ca9bb56771b0882a118b2e/268725087/268725087.lrc","copy_type":"1","hot":"41160","all_artist_ting_uid":"10474","resource_type":"0","is_new":"1","rank_change":"2","rank":"26","all_artist_id":"2033","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"原来都是梦","song_id":"268721915","author":"李行亮","havehigh":2,"album_title":"原来都是梦","ting_uid":"10474","album_id":"268725116","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"李行亮"},{"artist_id":"113566457","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/3b4c3515bd517775b57d5f51bb10e803/268590063/268590063.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/6db177363f58726d63a3a62f81e35be0/268590066/268590066.jpg","country":"内地","area":"0","publishtime":"2016-08-09","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/82df283fa4f7b7f845ccab2bb63192c6/268590363/268590363.lrc","copy_type":"1","hot":"39401","all_artist_ting_uid":"210120107","resource_type":"0","is_new":"1","rank_change":"-1","rank":"27","all_artist_id":"113566457","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"孩子","song_id":"268590165","author":"金玟岐","havehigh":2,"album_title":"孩子","ting_uid":"210120107","album_id":"268590365","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"金玟岐"},{"artist_id":"1656","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/37d1037444f102526b1807b91524d719/267603990/267603990.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/6b4fe1f0fcf5ac669f831ed924c0af32/267603993/267603993.jpg","country":"内地","area":"0","publishtime":"2016-07-14","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/1a30e6a26276f00b6a681a55bc2b9ef6/267604446/267604446.lrc","copy_type":"1","hot":"166919","all_artist_ting_uid":"1581","resource_type":"0","is_new":"0","rank_change":"-1","rank":"28","all_artist_id":"1656","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"影视原声","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"-1|-1\"}","title":"时光正好","song_id":"267604147","author":"郁可唯","havehigh":2,"album_title":"时光正好","ting_uid":"1581","album_id":"267604049","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"1","resource_type_ext":"1","mv_provider":"0000000000","artist_name":"郁可唯"},{"artist_id":"170","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/7b6706908aad6d0f1ac48fcf99053891/269079849/269079849.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/d543d6372fb5f2d4b1058734ec595eaa/269079852/269079852.jpg","country":"港台","area":"1","publishtime":"2016-08-07","album_no":"7","lrclink":"http://musicdata.baidu.com/data2/lrc/af1e34a8c962844164b9f6e9315fc2d2/269082030/269082030.lrc","copy_type":"3","hot":"31067","all_artist_ting_uid":"1121","resource_type":"0","is_new":"1","rank_change":"4","rank":"29","all_artist_id":"170","style":"摇滚","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"贼","song_id":"268459930","author":"戴佩妮","havehigh":2,"album_title":"贼","ting_uid":"1121","album_id":"268462289","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"戴佩妮"},{"artist_id":"267706478","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/28e1f2c7a4c5da8273b399ec672370ba/268690228/268690228.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/72d3db88e54caab6697584ba7119e4d6/268690231/268690231.jpg","country":"内地","area":"0","publishtime":"2016-08-11","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/23913c1237324fec9c0543b5e43c7b90/268987103/268987103.lrc","copy_type":"1","hot":"32019","all_artist_ting_uid":"239563535","resource_type":"0","is_new":"1","rank_change":"6","rank":"30","all_artist_id":"267706478","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"影视原声","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"Fighting Day","song_id":"268690250","author":"SNH48鞠婧祎","havehigh":2,"album_title":"Fighting Day","ting_uid":"239563535","album_id":"268690251","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"SNH48鞠婧祎"},{"artist_id":"10490649","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/8a790a5bc226b4b60ec5b606cbe8f325/267767591/267767591.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/05b698be27b55e5c862828d5d69862e0/267767593/267767593.jpg","country":"港台","area":"1","publishtime":"2016-07-18","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/0e3c214fda956f7c9d602b1c5e58d5a3/267772174/267772174.lrc","copy_type":"1","hot":"160796","all_artist_ting_uid":"687850","resource_type":"0","is_new":"0","rank_change":"-2","rank":"31","all_artist_id":"10490649","style":"","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"129|-1\",\"1\":\"-1|-1\"}","title":"最好的安排","song_id":"267767595","author":"曲婉婷","havehigh":2,"album_title":"最好的安排","ting_uid":"687850","album_id":"267767583","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"曲婉婷"},{"artist_id":"88","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","publishtime":"2016-07-15","album_no":"9","lrclink":"http://musicdata.baidu.com/data2/lrc/ea1b87e9112aaa7bfbba86b8bc954adb/267710713/267710713.lrc","copy_type":"1","hot":"154276","all_artist_ting_uid":"2517","resource_type":"0","is_new":"0","rank_change":"-2","rank":"32","all_artist_id":"88","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"花儿和少年","song_id":"267709069","author":"薛之谦","havehigh":2,"album_title":"初学者","ting_uid":"2517","album_id":"241838068","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"薛之谦"},{"artist_id":"2101","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/8485f763f2579f97815b68af8f165869/268419581/268419581.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/f9778c311beb02ce6d0a2b32731d79ad/268419584/268419584.jpg","country":"内地","area":"0","publishtime":"2016-08-02","album_no":"9","lrclink":"http://musicdata.baidu.com/data2/lrc/06a3b29d16bf05dda8ed3d4f7b3fdf18/268253084/268253084.lrc","copy_type":"1","hot":"32205","all_artist_ting_uid":"1684,5977","resource_type":"0","is_new":"1","rank_change":"-2","rank":"33","all_artist_id":"2101,1394","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":1,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"青春不散","song_id":"268251962","author":"崔恕,王馨平","havehigh":2,"album_title":"老好人","ting_uid":"1684","album_id":"268253100","charge":0,"is_first_publish":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0100000000","artist_name":"崔恕,王馨平"},{"artist_id":"73342624","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/ae413bb186486dccd34b08c4c0ee621e/268129650/268129650.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/1966ad235c18a50838914ece8865d727/268129646/268129646.jpg","country":"其他","area":"4","publishtime":"2016-07-27","album_no":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/b55a1d362e738446dd03d252e283326e/268129638/268129638.lrc","copy_type":"1","hot":"83627","all_artist_ting_uid":"0","resource_type":"0","is_new":"1","rank_change":"0","rank":"34","all_artist_id":"73342624","style":"","del_status":"0","relate_status":"1","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":262,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"不一样的我们","song_id":"74154329","author":"想想","havehigh":2,"album_title":"","ting_uid":"110948064","album_id":"0","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"yyr","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"想想"},{"artist_id":"5734662","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/c6c8cfad994c873eaa89fdf1968b94dc/267973475/267973475.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/5be50292728380cdfa12ce9211125dd8/267973478/267973478.jpg","country":"内地","area":"0","publishtime":"2016-07-23","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/20d3c9d175ece33b053f5a1d9ba021d8/268023399/268023399.lrc","copy_type":"1","hot":"75039","all_artist_ting_uid":"821050","resource_type":"0","is_new":"1","rank_change":"3","rank":"35","all_artist_id":"5734662","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"玛纳斯之恋","song_id":"267973488","author":"乌兰图雅","havehigh":2,"album_title":"玛纳斯之恋","ting_uid":"821050","album_id":"267973487","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"乌兰图雅"},{"artist_id":"1512","language":"英语","pic_big":"http://musicdata.baidu.com/data2/pic/c581f5d8e3cde04f707c9f3bd145472d/267905932/267905932.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/a57aa612d1fd303c4a1860d02ac9f855/267905934/267905934.jpg","country":"欧美","area":"2","publishtime":"2016-07-22","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/43f8071f6877ca3fadc50e03a6887244/268172228/268172228.lrc","copy_type":"1","hot":"74650","all_artist_ting_uid":"2716","resource_type":"0","is_new":"1","rank_change":"1","rank":"36","all_artist_id":"1512","style":"","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"129|-1\",\"1\":\"-1|-1\"}","title":"Rise","song_id":"267905936","author":"Katy Perry","havehigh":2,"album_title":"Rise","ting_uid":"2716","album_id":"267905926","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"Katy Perry"},{"artist_id":"1778","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/819da8289b0b89167ef9973e333d4367/268236406/268236406.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/bf4b92284fec7b0014460cd59f4d5036/268236409/268236409.jpg","country":"内地","area":"0","publishtime":"2016-08-01","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/ccb72e9ca07131d69cc6f90ba83c514d/268240865/268240865.lrc","copy_type":"1","hot":"26076","all_artist_ting_uid":"1607","resource_type":"0","is_new":"1","rank_change":"2","rank":"37","all_artist_id":"1778","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"天使在我身边","song_id":"268236531","author":"MIC男团","havehigh":2,"album_title":"天使在我身边","ting_uid":"1607","album_id":"268236539","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"MIC男团"},{"artist_id":"73334036","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/df3048ea1ca3d2ad9af0dd4d1805ed38/268570998/268570998.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/dc10bd96563a80acf9f050a319171c42/268571010/268571010.jpg","country":"其他","area":"4","publishtime":"2016-08-02","album_no":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/3be0f70cc0b0a8e3637ef98620c07f44/268547286/268547286.lrc","copy_type":"1","hot":"24225","all_artist_ting_uid":"0","resource_type":"0","is_new":"1","rank_change":"3","rank":"38","all_artist_id":"73334036","style":"","del_status":"0","relate_status":"1","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":277,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"树叶的光","song_id":"74156199","author":"徐菲","havehigh":2,"album_title":"","ting_uid":"110939476","album_id":"0","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"yyr","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"徐菲"},{"artist_id":"146","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/6a782411df6078968a89dd109e072ea3/267534149/267534149.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/72421a798241d09ad379f867b5db52e2/267534153/267534153.jpg","country":"内地","area":"0","publishtime":"2016-07-12","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/2b29b613d527c6086d276b51ce9cc10e/267534251/267534251.lrc","copy_type":"1","hot":"120582","all_artist_ting_uid":"1108,232949040","resource_type":"0","is_new":"0","rank_change":"-4","rank":"39","all_artist_id":"146,131781029","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"影视原声","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"明明白白我的心2016","song_id":"267534176","author":"成龙,魏允熙","havehigh":2,"album_title":"明明白白我的心2016","ting_uid":"1108","album_id":"267534159","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"成龙,魏允熙"},{"artist_id":"12381018","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/d8af39ca3ade01ea34289400a63bccab/268684781/268684781.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/8dd8858c02d1b21c04aed5476ed6923d/268684784/268684784.jpg","country":"内地","area":"0","publishtime":"2016-08-08","album_no":"2","lrclink":"http://musicdata.baidu.com/data2/lrc/dfbd93456ce2adce9e5624b06ac18261/268685131/268685131.lrc","copy_type":"1","hot":"18982","all_artist_ting_uid":"1224778","resource_type":"0","is_new":"1","rank_change":"18","rank":"40","all_artist_id":"12381018","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"不失不忘","song_id":"268681798","author":"崔子格","havehigh":2,"album_title":"不失不忘","ting_uid":"1224778","album_id":"268609859","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"崔子格"},{"artist_id":"88","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","publishtime":"2016-06-23","album_no":"3","lrclink":"http://musicdata.baidu.com/data2/lrc/f63a5782603c444ad7c66b2d67bd534f/267708381/267708381.lrc","copy_type":"1","hot":"0","all_artist_ting_uid":"2517","resource_type":"0","is_new":"0","rank_change":"-1","rank":"41","all_artist_id":"88","style":"影视原声","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"影视原声","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"我好像在哪见过你","song_id":"266942077","author":"薛之谦","havehigh":2,"album_title":"初学者","ting_uid":"2517","album_id":"241838068","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"薛之谦"},{"artist_id":"57297","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/aa1239d0e00543d967a138198f6ab19c/268081973/268081973.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/f8db23d478057cc4480455bd29e65d6d/268081976/268081976.jpg","country":"内地","area":"0","publishtime":"2016-07-28","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/4d2ff88b76b959480356d7a75ac09146/268117359/268117359.lrc","copy_type":"1","hot":"68705","all_artist_ting_uid":"245815","resource_type":"0","is_new":"1","rank_change":"0","rank":"42","all_artist_id":"57297","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"惦记","song_id":"268081984","author":"祁隆","havehigh":2,"album_title":"惦记","ting_uid":"245815","album_id":"268081987","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"祁隆"},{"artist_id":"73330300","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/98580be7ffea5b0ded73e00f8b453e14/268562742/268562742.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/14394a6944cab8909687f2207a5eb227/268562749/268562749.jpg","country":"其他","area":"4","publishtime":"2016-08-02","album_no":"0","lrclink":"","copy_type":"1","hot":"20885","all_artist_ting_uid":"0","resource_type":"0","is_new":"1","rank_change":"4","rank":"43","all_artist_id":"73330300","style":"","del_status":"0","relate_status":"1","toneid":"0","all_rate":"64,128","sound_effect":"0","file_duration":238,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"林中鸟","song_id":"74155852","author":"魏佳艺","havehigh":0,"album_title":"","ting_uid":"110935740","album_id":"0","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"yyr","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"魏佳艺"},{"artist_id":"905","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/dc40e09303a64ce2bfbd77a2d7b8774d/268926683/268926683.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/4554712fcc9e0f4cc7175c30e177ce98/268926685/268926685.jpg","country":"内地","area":"0","publishtime":"2016-08-15","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/1684c3470860e45a67b8e6fa9adb88be/268948068/268948068.lrc","copy_type":"1","hot":"19978","all_artist_ting_uid":"10560","resource_type":"0","is_new":"1","rank_change":"10","rank":"44","all_artist_id":"905","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"129|-1\",\"1\":\"-1|-1\"}","title":"悬之又悬","song_id":"268926687","author":"萨顶顶","havehigh":2,"album_title":"悬之又悬","ting_uid":"10560","album_id":"268926676","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"萨顶顶"},{"artist_id":"73386783","language":"粤语","pic_big":"http://musicdata.baidu.com/data2/pic/d0e4732987f6f98166e18ae7fe80823b/268708230/268708230.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/7f304fda27cb3f350f5a27c60a0d9bb7/268708241/268708241.jpg","country":"其他","area":"4","publishtime":"2016-08-11","album_no":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/0d56d4a3859c129490519a01bf4deb16/268708223/268708223.lrc","copy_type":"1","hot":"19766","all_artist_ting_uid":"0","resource_type":"0","is_new":"1","rank_change":"10","rank":"45","all_artist_id":"73386783","style":"","del_status":"0","relate_status":"1","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":197,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"才不枉过年少吧","song_id":"74156730","author":"张璐诗Lucie","havehigh":2,"album_title":"","ting_uid":"110992223","album_id":"0","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"yyr","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"张璐诗Lucie"},{"artist_id":"10490649","language":"英语","pic_big":"http://musicdata.baidu.com/data2/pic/c4b2a4acef5cd6b9e762e394cbf71126/267915333/267915333.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/8d3849000cffab7be528e240bbccb4c0/267915335/267915335.jpg","country":"内地","area":"0","publishtime":"2016-07-22","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/0c9b01f0ef97f6adef4b6e4a69be80bd/267915719/267915719.lrc","copy_type":"1","hot":"59171","all_artist_ting_uid":"687850","resource_type":"0","is_new":"1","rank_change":"-3","rank":"46","all_artist_id":"10490649","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"129|-1\",\"1\":\"-1|-1\"}","title":"Your Girl","song_id":"267915337","author":"曲婉婷","havehigh":2,"album_title":"Your Girl","ting_uid":"687850","album_id":"267915327","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"曲婉婷"},{"artist_id":"903","language":"英语","pic_big":"http://musicdata.baidu.com/data2/pic/377ec9d2bb4f4d517da6a1cc81275cf8/268664406/268664406.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/4f728c64e784b4de36b7d0e386fd2853/268664409/268664409.jpg","country":"内地","area":"0","publishtime":"2016-08-10","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/a195a03cbf341c280f6bdd2d39588fd0/268668510/268668510.lrc","copy_type":"1","hot":"19110","all_artist_ting_uid":"1418","resource_type":"0","is_new":"1","rank_change":"2","rank":"47","all_artist_id":"903","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"Because of you (Guitar Ver.)","song_id":"268665871","author":"By2","havehigh":2,"album_title":"亲爱的,公主病 OST.Part.1","ting_uid":"1418","album_id":"268665875","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"By2"},{"artist_id":"68","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/f09570f2e131e76df6e58d4148bcb3e2/268544465/268544465.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/daee619c49de62e8c563242f883ee12a/268544468/268544468.jpg","country":"内地","area":"0","publishtime":"2016-08-05","album_no":"2","lrclink":"http://musicdata.baidu.com/data2/lrc/da6e509b6071e8e5fe615daaf8c2574b/268817476/268817476.lrc","copy_type":"1","hot":"19430","all_artist_ting_uid":"1062","resource_type":"0","is_new":"1","rank_change":"0","rank":"48","all_artist_id":"68","style":"摇滚","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"城市病人","song_id":"268470191","author":"谭维维","havehigh":2,"album_title":"夏长","ting_uid":"1062","album_id":"268417286","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"谭维维"},{"artist_id":"268171195","language":"英语","pic_big":"http://musicdata.baidu.com/data2/pic/fe3d34c110dca61f460fd1af75056246/268168702/268168702.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/7159dccdd835fd6c2040401c25019df3/268168705/268168705.jpg","country":"内地","area":"0","publishtime":"2016-08-04","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/16909a591c20abf143ee7a77b54632e6/268353507/268353507.lrc","copy_type":"1","hot":"19738","all_artist_ting_uid":"239564022","resource_type":"0","is_new":"1","rank_change":"-3","rank":"49","all_artist_id":"268171195","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":0,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"You You","song_id":"268175134","author":"Freia","havehigh":2,"album_title":"You You","ting_uid":"239564022","album_id":"268175140","charge":0,"is_first_publish":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0000000000","artist_name":"Freia"},{"artist_id":"123339593","language":"国语","pic_big":"http://musicdata.baidu.com/data2/pic/4827be12e023a8bfffd487fefd443114/266867928/266867928.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/30f2119dee46fd8f629ded6e6a11be36/266867931/266867931.jpg","country":"内地","area":"0","publishtime":"2016-06-22","album_no":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/491f9bb238d224444d896a31ca9f4457/266879166/266879166.lrc","copy_type":"1","hot":"252449","all_artist_ting_uid":"163361619","resource_type":"0","is_new":"0","rank_change":"-6","rank":"50","all_artist_id":"123339593","style":"流行","del_status":"0","relate_status":"0","toneid":"0","all_rate":"64,128,256,320,flac","sound_effect":"0","file_duration":0,"has_mv_mobile":1,"versions":"","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","title":"渡红尘","song_id":"266867984","author":"张碧晨","havehigh":2,"album_title":"渡红尘","ting_uid":"163361619","album_id":"266867952","charge":0,"is_first_publish":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","resource_type_ext":"0","mv_provider":"0100000000","artist_name":"张碧晨"}]
     * billboard : {"billboard_type":"1","billboard_no":"1926","update_date":"2016-08-19","billboard_songnum":"198","havemore":1,"name":"新歌榜","comment":"该榜单是根据百度音乐平台歌曲每日播放量自动生成的数据榜单，统计范围为近期发行的歌曲，每日更新一次","pic_s640":"http://c.hiphotos.baidu.com/ting/pic/item/f7246b600c33874495c4d089530fd9f9d62aa0c6.jpg","pic_s444":"http://d.hiphotos.baidu.com/ting/pic/item/78310a55b319ebc4845c84eb8026cffc1e17169f.jpg","pic_s260":"http://b.hiphotos.baidu.com/ting/pic/item/e850352ac65c1038cb0f3cb0b0119313b07e894b.jpg","pic_s210":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_c49310115801d43d42a98fdc357f6057.jpg","web_url":"http://music.baidu.com/top/new"}
     * error_code : 22000
     */

    private int error_code;
    /**
     * artist_id : 14947058
     * language : 国语
     * pic_big : http://musicdata.baidu.com/data2/pic/518827cd2888633ac49f1fafc7002913/268809938/268809938.jpg
     * pic_small : http://musicdata.baidu.com/data2/pic/07e059f82a74da54cdce398b5e422fe9/268809943/268809943.jpg
     * country : 内地
     * area : 0
     * publishtime : 2016-08-12
     * album_no : 1
     * lrclink : http://musicdata.baidu.com/data2/lrc/b2b1ba655f1a9056020bf3700291a79b/268810692/268810692.lrc
     * copy_type : 1
     * hot : 285413
     * all_artist_ting_uid : 10559334
     * resource_type : 0
     * is_new : 1
     * rank_change : 0
     * rank : 1
     * all_artist_id : 14947058
     * style : 流行
     * del_status : 0
     * relate_status : 0
     * toneid : 0
     * all_rate : 64,128,256,320,flac
     * sound_effect : 0
     * file_duration : 0
     * has_mv_mobile : 0
     * versions :
     * bitrate_fee : {"0":"0|0","1":"-1|-1"}
     * title : 树读
     * song_id : 268810037
     * author : 王俊凯
     * havehigh : 2
     * album_title : 树读
     * ting_uid : 10559334
     * album_id : 268810041
     * charge : 0
     * is_first_publish : 0
     * has_mv : 0
     * learn : 0
     * song_source : web
     * piao_id : 0
     * korean_bb_song : 1
     * resource_type_ext : 1
     * mv_provider : 0000000000
     * artist_name : 王俊凯
     */

    private List<SongListBean> song_list;

    public BillboardBean getBillboard() {
        return billboard;
    }

    public void setBillboard(BillboardBean billboard) {
        this.billboard = billboard;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public List<SongListBean> getSong_list() {
        return song_list;
    }

    public void setSong_list(List<SongListBean> song_list) {
        this.song_list = song_list;
    }

    public static class BillboardBean {
        private String billboard_type;
        private String billboard_no;
        private String update_date;
        private String billboard_songnum;
        private int havemore;
        private String name;
        private String comment;
        private String pic_s640;
        private String pic_s444;
        private String pic_s260;
        private String pic_s210;
        private String web_url;

        public String getBillboard_type() {
            return billboard_type;
        }

        public void setBillboard_type(String billboard_type) {
            this.billboard_type = billboard_type;
        }

        public String getBillboard_no() {
            return billboard_no;
        }

        public void setBillboard_no(String billboard_no) {
            this.billboard_no = billboard_no;
        }

        public String getUpdate_date() {
            return update_date;
        }

        public void setUpdate_date(String update_date) {
            this.update_date = update_date;
        }

        public String getBillboard_songnum() {
            return billboard_songnum;
        }

        public void setBillboard_songnum(String billboard_songnum) {
            this.billboard_songnum = billboard_songnum;
        }

        public int getHavemore() {
            return havemore;
        }

        public void setHavemore(int havemore) {
            this.havemore = havemore;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getPic_s640() {
            return pic_s640;
        }

        public void setPic_s640(String pic_s640) {
            this.pic_s640 = pic_s640;
        }

        public String getPic_s444() {
            return pic_s444;
        }

        public void setPic_s444(String pic_s444) {
            this.pic_s444 = pic_s444;
        }

        public String getPic_s260() {
            return pic_s260;
        }

        public void setPic_s260(String pic_s260) {
            this.pic_s260 = pic_s260;
        }

        public String getPic_s210() {
            return pic_s210;
        }

        public void setPic_s210(String pic_s210) {
            this.pic_s210 = pic_s210;
        }

        public String getWeb_url() {
            return web_url;
        }

        public void setWeb_url(String web_url) {
            this.web_url = web_url;
        }
    }

    public static class SongListBean {
        private String artist_id;
        private String language;
        private String pic_big;
        private String pic_small;
        private String country;
        private String area;
        private String publishtime;
        private String album_no;
        private String lrclink;
        private String copy_type;
        private String hot;
        private String all_artist_ting_uid;
        private String resource_type;
        private String is_new;
        private String rank_change;
        private String rank;
        private String all_artist_id;
        private String style;
        private String del_status;
        private String relate_status;
        private String toneid;
        private String all_rate;
        private String sound_effect;
        private int file_duration;
        private int has_mv_mobile;
        private String versions;
        private String bitrate_fee;
        private String title;
        private String song_id;
        private String author;
        private int havehigh;
        private String album_title;
        private String ting_uid;
        private String album_id;
        private int charge;
        private int is_first_publish;
        private int has_mv;
        private int learn;
        private String song_source;
        private String piao_id;
        private String korean_bb_song;
        private String resource_type_ext;
        private String mv_provider;
        private String artist_name;

        public String getArtist_id() {
            return artist_id;
        }

        public void setArtist_id(String artist_id) {
            this.artist_id = artist_id;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getPic_big() {
            return pic_big;
        }

        public void setPic_big(String pic_big) {
            this.pic_big = pic_big;
        }

        public String getPic_small() {
            return pic_small;
        }

        public void setPic_small(String pic_small) {
            this.pic_small = pic_small;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getPublishtime() {
            return publishtime;
        }

        public void setPublishtime(String publishtime) {
            this.publishtime = publishtime;
        }

        public String getAlbum_no() {
            return album_no;
        }

        public void setAlbum_no(String album_no) {
            this.album_no = album_no;
        }

        public String getLrclink() {
            return lrclink;
        }

        public void setLrclink(String lrclink) {
            this.lrclink = lrclink;
        }

        public String getCopy_type() {
            return copy_type;
        }

        public void setCopy_type(String copy_type) {
            this.copy_type = copy_type;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getAll_artist_ting_uid() {
            return all_artist_ting_uid;
        }

        public void setAll_artist_ting_uid(String all_artist_ting_uid) {
            this.all_artist_ting_uid = all_artist_ting_uid;
        }

        public String getResource_type() {
            return resource_type;
        }

        public void setResource_type(String resource_type) {
            this.resource_type = resource_type;
        }

        public String getIs_new() {
            return is_new;
        }

        public void setIs_new(String is_new) {
            this.is_new = is_new;
        }

        public String getRank_change() {
            return rank_change;
        }

        public void setRank_change(String rank_change) {
            this.rank_change = rank_change;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getAll_artist_id() {
            return all_artist_id;
        }

        public void setAll_artist_id(String all_artist_id) {
            this.all_artist_id = all_artist_id;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String getDel_status() {
            return del_status;
        }

        public void setDel_status(String del_status) {
            this.del_status = del_status;
        }

        public String getRelate_status() {
            return relate_status;
        }

        public void setRelate_status(String relate_status) {
            this.relate_status = relate_status;
        }

        public String getToneid() {
            return toneid;
        }

        public void setToneid(String toneid) {
            this.toneid = toneid;
        }

        public String getAll_rate() {
            return all_rate;
        }

        public void setAll_rate(String all_rate) {
            this.all_rate = all_rate;
        }

        public String getSound_effect() {
            return sound_effect;
        }

        public void setSound_effect(String sound_effect) {
            this.sound_effect = sound_effect;
        }

        public int getFile_duration() {
            return file_duration;
        }

        public void setFile_duration(int file_duration) {
            this.file_duration = file_duration;
        }

        public int getHas_mv_mobile() {
            return has_mv_mobile;
        }

        public void setHas_mv_mobile(int has_mv_mobile) {
            this.has_mv_mobile = has_mv_mobile;
        }

        public String getVersions() {
            return versions;
        }

        public void setVersions(String versions) {
            this.versions = versions;
        }

        public String getBitrate_fee() {
            return bitrate_fee;
        }

        public void setBitrate_fee(String bitrate_fee) {
            this.bitrate_fee = bitrate_fee;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSong_id() {
            return song_id;
        }

        public void setSong_id(String song_id) {
            this.song_id = song_id;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public int getHavehigh() {
            return havehigh;
        }

        public void setHavehigh(int havehigh) {
            this.havehigh = havehigh;
        }

        public String getAlbum_title() {
            return album_title;
        }

        public void setAlbum_title(String album_title) {
            this.album_title = album_title;
        }

        public String getTing_uid() {
            return ting_uid;
        }

        public void setTing_uid(String ting_uid) {
            this.ting_uid = ting_uid;
        }

        public String getAlbum_id() {
            return album_id;
        }

        public void setAlbum_id(String album_id) {
            this.album_id = album_id;
        }

        public int getCharge() {
            return charge;
        }

        public void setCharge(int charge) {
            this.charge = charge;
        }

        public int getIs_first_publish() {
            return is_first_publish;
        }

        public void setIs_first_publish(int is_first_publish) {
            this.is_first_publish = is_first_publish;
        }

        public int getHas_mv() {
            return has_mv;
        }

        public void setHas_mv(int has_mv) {
            this.has_mv = has_mv;
        }

        public int getLearn() {
            return learn;
        }

        public void setLearn(int learn) {
            this.learn = learn;
        }

        public String getSong_source() {
            return song_source;
        }

        public void setSong_source(String song_source) {
            this.song_source = song_source;
        }

        public String getPiao_id() {
            return piao_id;
        }

        public void setPiao_id(String piao_id) {
            this.piao_id = piao_id;
        }

        public String getKorean_bb_song() {
            return korean_bb_song;
        }

        public void setKorean_bb_song(String korean_bb_song) {
            this.korean_bb_song = korean_bb_song;
        }

        public String getResource_type_ext() {
            return resource_type_ext;
        }

        public void setResource_type_ext(String resource_type_ext) {
            this.resource_type_ext = resource_type_ext;
        }

        public String getMv_provider() {
            return mv_provider;
        }

        public void setMv_provider(String mv_provider) {
            this.mv_provider = mv_provider;
        }

        public String getArtist_name() {
            return artist_name;
        }

        public void setArtist_name(String artist_name) {
            this.artist_name = artist_name;
        }
    }
}
