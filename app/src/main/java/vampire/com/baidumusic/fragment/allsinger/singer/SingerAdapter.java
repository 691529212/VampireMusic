package vampire.com.baidumusic.fragment.allsinger.singer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class SingerAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_SingerAdapter";
    private Context context;
    private List<SingerBean.ArtistBean> artistBeen;

    public void setArtistBeen(List<SingerBean.ArtistBean> artistBeen) {
        this.artistBeen = artistBeen;
        notifyDataSetChanged();
    }

    public SingerAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return artistBeen!=null?artistBeen.size():0;
    }

    @Override
    public Object getItem(int position) {
        return artistBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =LayoutInflater.from(context);
        MyViewHolder myViewHolder;
        if (convertView==null){
            convertView =inflater.inflate(R.layout.item_lv_singer,null);
            myViewHolder=new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        }else {
            myViewHolder= (MyViewHolder) convertView.getTag();
        }
        NetTool netTool =new NetTool();
        netTool.getImage(artistBeen.get(position).getAvatar_big(),myViewHolder.pic);
        myViewHolder.name.setText(artistBeen.get(position).getName());

        return convertView;
    }
    private class MyViewHolder{
        private ImageView pic ;
        private TextView name;

        public MyViewHolder(View view) {
            pic= (ImageView) view.findViewById(R.id.iv_singer_item_pic);
            name= (TextView) view.findViewById(R.id.tv_singer_name);
        }
    }
}
