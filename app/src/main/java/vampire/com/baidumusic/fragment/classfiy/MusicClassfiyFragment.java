package vampire.com.baidumusic.fragment.classfiy;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class MusicClassfiyFragment extends BaseFragment {
    private static final String TAG = "Vampire_MusicClassfiyActivity";

    @Override
    protected int setLayout() {
        return R.layout.activity_classfiy_music;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
