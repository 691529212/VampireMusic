package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.mv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created vampires by *Vampire* on 16/8/31.
 */
public class MvGGVAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_MvGGVAdapter";
    private Context context;
    private List<MvListBean.ResultBean.MvListBeen> mvListBeens;

    public void setMvListBeens(List<MvListBean.ResultBean.MvListBeen> mvListBeens) {
        this.mvListBeens = mvListBeens;
        notifyDataSetChanged();
    }

    public MvGGVAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    public void addMvListBeens(List<MvListBean.ResultBean.MvListBeen> mvListBeens){
        this.mvListBeens.addAll(mvListBeens);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mvListBeens!=null?mvListBeens.size():0;
    }

    @Override
    public Object getItem(int position) {
        return mvListBeens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder ;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.ggv_mv,null);
            myViewHolder = new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        }else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }
        myViewHolder.author.setText(mvListBeens.get(position).getArtist());
        myViewHolder.title.setText(mvListBeens.get(position).getTitle());
        NetTool netTool =new NetTool();
        netTool.getImage(mvListBeens.get(position).getThumbnail2(),myViewHolder.pic);

        return convertView;
    }

    private class MyViewHolder {
        private ImageView pic;
        private TextView title;
        private TextView author;


        public MyViewHolder(View view) {

            pic = (ImageView) view.findViewById(R.id.iv_mv);
            title = (TextView) view.findViewById(R.id.tv_mv_title);
            author = (TextView) view.findViewById(R.id.tv_mv_author);
        }
    }
}
