package vampire.com.baidumusic.fragment.player;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class PlayerAdapter extends FragmentPagerAdapter {
    private static final String TAG = "Vampire_PlayerAdapter";
    ArrayList <Fragment> fragments;

    public void setFragments(ArrayList<Fragment> fragments) {
        this.fragments = fragments;
        notifyDataSetChanged();
    }

    public PlayerAdapter(FragmentManager fm) {
        super(fm);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        int count =fragments!=null?fragments.size():0;
        return fragments!=null?fragments.size():0;
    }


}
