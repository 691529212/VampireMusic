package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.ranking;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class RankFragment extends BaseFragment {
    private static final String TAG = "Vampire_RankFragment";
    private RankFragmentAdapter rankFragmentAdapter;
    private ArrayList<RankFragmentBean.ContentBean> contentBeen;
    private ListView listView;

    @Override
    protected int setLayout() {
        return R.layout.fragment_music_libiry_rank;
    }

    @Override
    protected void initView() {
        listView = bindView(R.id.lv_music_ranking);
        rankFragmentAdapter = new RankFragmentAdapter(contentBeen,getContext());
        listView.setAdapter(rankFragmentAdapter);

    }

    @Override
    protected void initData() {
        mNetTool.getNetData(URLVlaues.MUSICSTORE_TOP, RankFragmentBean.class, new NetTool.NetListener<RankFragmentBean>() {
            @Override
            public void onSuccess(RankFragmentBean rankFragmentBean) {
                contentBeen = (ArrayList<RankFragmentBean.ContentBean>) rankFragmentBean.getContents();
                rankFragmentAdapter.setContentBeen(contentBeen);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RankFragmentBean.ContentBean listBean =contentBeen.get(position);
                Bundle bundle =new Bundle();
                bundle.putString(KeyValues.RANK_PIC,listBean.getPic_s210());
                bundle.putString(KeyValues.RANK_TYPE,""+listBean.getType());
                bundle.putString(KeyValues.RANK_NAME,listBean.getName());
                bundle.putInt(KeyValues.MY_TYPE_ID,3);
                DetailFragment fragment=new DetailFragment();
                fragment.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(fragment);



            }
        });

    }
}
