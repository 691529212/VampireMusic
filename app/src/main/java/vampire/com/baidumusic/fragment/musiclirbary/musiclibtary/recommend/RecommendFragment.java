package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.recommend;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import com.jude.rollviewpager.RollPagerView;

import java.util.ArrayList;
import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.tools.myview.GrapeGridView;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.fragment.allsinger.AllSingerFragment;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class RecommendFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Vampire_RecommendFragment";
    private ArrayList<RecommendBean.PicBean> picBeens;
    private RecommendAdapter recommendAdapter;
    private RollPagerView viewPager;

    private GrapeGridView gridViewList;
    //private FragmentTransaction transaction;
    private FragmentManager manager;
    private List<ListRecommendBean.ContentBean.ListBean> contentBeens;

    @Override
    protected int setLayout() {
        return R.layout.fragment_music_libiry_recommend;
    }

    @Override
    protected void initView() {
        manager = getFragmentManager();

        viewPager = bindView(R.id.view_pager_recommend);

        picBeens = new ArrayList<>();
        Button clsaafiy = bindView(R.id.btn_recommend_classfiy);
        clsaafiy.setOnClickListener(this);
        Button singer = bindView(R.id.btn_recommend_singer);
        singer.setOnClickListener(this);
        Button todayRecommend = bindView(R.id.btn_recommend_today);
        todayRecommend.setOnClickListener(this);
        Button listMore = bindView(R.id.btn_recommend_list_more);// 歌单推荐-更多;
        listMore.setOnClickListener(this);

        // 歌单推荐
        gridViewList = bindView(R.id.grid_view_list_recommend);

    }

    @Override
    protected void initData() {

        // 轮播图数据获取
        mNetTool.getNetData(URLVlaues.RECOMMAND_CAROUSE, RecommendBean.class, new NetTool.NetListener<RecommendBean>() {
            @Override
            public void onSuccess(RecommendBean recommendBean) {
                recommendAdapter = new RecommendAdapter(viewPager,getContext());
                picBeens = (ArrayList<RecommendBean.PicBean>) recommendBean.getPic();
                recommendAdapter.setPicBeens(picBeens);
                viewPager.setAdapter(recommendAdapter);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });


        // 歌单推荐
        mNetTool.getNetData(URLVlaues.RECOMMAND_HOT, ListRecommendBean.class, new NetTool.NetListener<ListRecommendBean>() {
            @Override
            public void onSuccess(ListRecommendBean listRecommendBean) {
                contentBeens = listRecommendBean.getContent().getList();
                GridViewAdapter gridViewAdapter = new GridViewAdapter(getContext(), contentBeens);
                gridViewList.setAdapter(gridViewAdapter);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });

        gridViewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListRecommendBean.ContentBean.ListBean listBean = contentBeens.get(position);
                Bundle bundle = new Bundle();
                bundle.putString(KeyValues.RANK_PIC, listBean.getPic());
                bundle.putString(KeyValues.RANK_TYPE, listBean.getListid());
                bundle.putString(KeyValues.RANK_NAME, listBean.getTitle());
                bundle.putInt(KeyValues.MY_TYPE_ID, 2);
                DetailFragment fragment = new DetailFragment();
                fragment.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(fragment);

            }
        });

    }



    @Override
    public void onPause() {
        super.onPause();

    }



    // 点击事件
    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = manager.beginTransaction();
        switch (v.getId()) {
            case R.id.btn_recommend_list_more:
                // 歌单推荐-更多 (跳转至歌单)done
                Intent broadcast = new Intent();
                broadcast.setAction("vampire.com.baidumusic.fragment.musicLirbaryFragment.fragmenstInMusicLibtary.recommendFragment.MoveToList");
                getActivity().sendBroadcast(broadcast);
                break;
            case R.id.btn_recommend_classfiy:
                // 歌曲分类

                // transaction.add(R.id.main_frame_layout,new MusicClassfiyFragment()).commit();
                break;
            case R.id.btn_recommend_singer:
                // 全部歌手
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(new AllSingerFragment());
                // transaction.add(R.id.main_frame_layout,new AllSingerFragment()).commit();
                break;
            case R.id.btn_recommend_today:
                // 今日推荐歌曲
                transaction.add(R.id.main_frame_layout, new RecommendFragment()).commit();
                break;
        }
    }
}
