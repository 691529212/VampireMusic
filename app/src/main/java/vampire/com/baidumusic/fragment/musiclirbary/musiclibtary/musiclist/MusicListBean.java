package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.musiclist;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class MusicListBean {
    private static final String TAG = "Vampire_MusicListBean";
    /**
     * error_code : 22000
     * total : 6329
     * havemore : 1
     * content : [{"listid":"7013","listenum":"51061","collectnum":"398","title":"有些歌我们一起唱会更美","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_304cab51951525021bf7d9d1a31d77f6.jpg","tag":"KTV,约会,经典","desc":"用歌声表达心中最恳切的情愫","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_45075b779fd5d6d079d8781c0ba3ca6a.jpg","width":"872","height":"871","songIds":["7318424","7319923","5837564","2131293","124685863","8564144","7332494","7328274","313985","7318602","280015","280015","466135","58801103","7330118"]},{"listid":"7034","listenum":"15123","collectnum":"538","title":"神秘风情，世界音乐","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_a51809c97d8a8b86817d1b35f95a5914.jpg","tag":"世界音乐,小语种,地铁","desc":"或热烈或低沉，总有惊喜的声音，带给你无限想象","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_5dcca589f93a670b9a39bb124bfc973f.jpg","width":"473","height":"474","songIds":["115050798","115050817","115050825","1735171","1751809","1414241","7651274","2725418","2187000","1574647","2655498","2655530","2655556","2656003","10223428"]},{"listid":"7017","listenum":"12190","collectnum":"537","title":"KTV提升B格必备","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_d6390fbb8c112556995969e694d7d3df.jpg","tag":"欧美,KTV,流行","desc":"耳熟能详的英文金曲，全场合唱，嗨翻全场","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1591dec5cb0d102049401783fb6c1121.jpg","width":"872","height":"871","songIds":["14824603","2158779","92279724","14919656","7478534","13685350","119189680","8584419","14907195","8532318","120102174","109714819","331822","356051","998473"]},{"listid":"7049","listenum":"13471","collectnum":"372","title":"我们正青春，校园歌曲精选","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_6e50c5c02ead16264c1849f0b80a18bb.jpg","tag":"校园,KTV,散步","desc":"回味校园生活，再尝酸甜苦辣","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_603e66317596731539c6af121c6052bd.jpg","width":"684","height":"684","songIds":["963292","2496515","8248175","304804","222655","1331688","306065","5919772","1122495","289693","1988832","7317572","7328681","7348218","233111"]},{"listid":"7009","listenum":"6298","collectnum":"505","title":"文青的怀旧音乐之旅，老布鲁斯","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_76c32c51b50d76e3b71c7beec028a49f.jpg","tag":"布鲁斯,酒吧,咖啡厅","desc":"古老的音乐节奏，老旧的忧郁情怀","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_94cbcee11dc9f389bbd540278acb4ac7.jpg","width":"1023","height":"1024","songIds":["2933341","402247","7536316","8565870","8045597","7924862","402247","5505152","10236530","8258585","7926532","10236549","10236526","7536422","7581686"]},{"listid":"7024","listenum":"8414","collectnum":"486","title":"心脏共鸣，灵魂共感","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_9d30c759de64dc126b6fdf2b7f6b6743.jpg","tag":"工作,纯音乐,放松","desc":"上班时的效率胶囊","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_5cc4738823b931b5c60ae0e71646e0c2.jpg","width":"872","height":"871","songIds":["73819000","8199430","8199432","8199435","8199436","7926593","1051479","7379310","7926595","1162834","8035133","7318399","7322462","7322478","7322492"]},{"listid":"7026","listenum":"9430","collectnum":"478","title":"酷派爵士\u2014\u2014冷峻一如某个夜晚","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_6d97ecd10cee4daa90e52268e9f549e4.jpg","tag":"爵士,酒吧,咖啡厅","desc":"Cool Jazz，正如夜晚触动诗人的心灵和情人的眼睛","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_a78242e33d58bea97a4f9f9fe8e3d347.jpg","width":"667","height":"667","songIds":["7478297","7521820","7478595","7521821","8094021","1623550","343058","7384894","7567721","8688985","8712223","7543103","8536477","536021","7543121","7659410","7903294","8566155","8371928","7552999"]},{"listid":"7048","listenum":"20922","collectnum":"407","title":"午后的悠扬小调，暖心功力max","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_d050674124a875b7b67bb6ade5fe8d3c.jpg","tag":"华语,韩语,午后","desc":"不同的语言与音乐调和一同陪你看路边花开","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_e21b284fdef004d0a2bd612a7e2a976f.jpg","width":"872","height":"871","songIds":["946215","14848895","349453","13902465","438173","275880","7326146","14554166","263954613","124738347","56563538","265029966","127428254","91782560","246346053"]},{"listid":"7028","listenum":"23154","collectnum":"542","title":"小语种精选\u2014\u2014永不凋零的浪漫","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_af769ce09e39b7763b204263cdc57567.jpg","tag":"小语种,翻唱,经典","desc":"听外语演绎本土与异地的纯净之声","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_fc30ae6a3e0020708e6c536a79695835.jpg","width":"872","height":"871","songIds":["7653137","132682972","65626244","7346341","1340093","2097624","18290591","18312346","245609052","1417405","2640101","1256993","1256962","64350493","8575639"]},{"listid":"7020","listenum":"4422","collectnum":"501","title":"汇聚时代流光，新世纪音乐合集","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_664028d541dc058aff67c3e83ec75849.jpg","tag":"新世纪,清晨,散步","desc":"美好音乐与你一路同行","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_00319058fcc19c023cbe64efa186b96d.jpg","width":"1080","height":"1080","songIds":["10284008","22811473","1973021","10284012","8578866","11268853","357168","7384880","2270756","10284009","8035130","458698","1973021","114624393","10218019"]},{"listid":"7007","listenum":"8891","collectnum":"360","title":"魅力拉丁，热情似火浪漫如诗","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_152865030d6665f010260e7b24599448.jpg","tag":"拉丁,午后,性感","desc":"风情万种，节奏曼妙，拉丁音乐的迷人魅力","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_499769a652109463a266c620d4b52aeb.jpg","width":"964","height":"964","songIds":["8264417","131717890","1148976","1485653","1830594","8191741","7377920","9893172","8565990","1599730","1594795","8016140","1420357","8236299","8278058"]},{"listid":"7029","listenum":"5961","collectnum":"486","title":"匪帮说唱\u2014\u2014街头的声音喷薄而出","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_a38e0a786df22399278967e467ba1f49.jpg","tag":"说唱,工作,夜店","desc":"匪帮说唱\u2014\u2014Gangsta rap，起源于20世纪80年代的说唱分支，诞生了一批最伟大的说唱歌手","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_9bba71664c11c8fcf247f56f8b2e348b.jpg","width":"555","height":"555","songIds":["7386319","10265709","119802203","2543331","2543069","257078017","116747399","30722716","14871628","14871626","8053412","8053411","8053423","2540969","2695273","261435944","261435644","252015466"]},{"listid":"7045","listenum":"14696","collectnum":"434","title":"秋冬暖心必备\u2014\u2014快来暖暖你的心","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_95f3fdb112e3e0f2781862b1ff096f1a.jpg","tag":"华语,咖啡厅,流行","desc":"首首情歌将暖流注入心田","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_a11177a2e5c99c62e68b75653f952eba.jpg","width":"872","height":"871","songIds":["276237","2496515","5440927","255704757","1329512","772146","7144261","260545309","7332912","2496530","492246","13902446","7317568","267767595","261931230"]},{"listid":"7041","listenum":"18543","collectnum":"447","title":"节奏入耳，旋律入心","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_768b395ed185b75138a50ff9e467123b.jpg","tag":"放松,r&b/soul,咖啡厅","desc":"中英r&b演绎别样风情","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_2a401480142b71b369fa54b4c50bccd3.jpg","width":"872","height":"871","songIds":["264130473","8011011","10047273","10236500","2704474","8192438","337759","46714205","53461469","2113203","261500","7318373","14699366","73819000","73819000"]},{"listid":"7047","listenum":"5828","collectnum":"386","title":"中式雷鬼\u2014\u2014一起飞去牙买加","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_ad33753109369bb1a108f2d6630eb6b4.jpg","tag":"雷鬼,地铁,酒吧","desc":"听雷鬼，在中国","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_9a69dd4a5076c388897eb7744c1f921e.jpg","width":"499","height":"499","songIds":["74101957","73829544","73896649","74101959","74101960","74101969","73829026","73829027","73896641","73896675","115269053","73829758","24059530","7307357","35122015","73990172","2091714","821102","2117515","73829085"]},{"listid":"7027","listenum":"8902","collectnum":"391","title":"爱的路上不言结局","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_e9fa7a190e52e2071ce6aa969771f41a.jpg","tag":"婚礼,清晨,乡村","desc":"一次婚礼许你一世情话","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_b092d9302088156a05fac2ed1d25bad0.jpg","width":"872","height":"871","songIds":["541244","7322462","8273019","8273070","8273075","1433392","7524228","7650848","7650847","8591742","7184772","105655495","123850421","7926593","8199432"]},{"listid":"7036","listenum":"10650","collectnum":"470","title":"年轻的节奏就要与众不同","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_9213328edae4db561d88b482c93dc398.jpg","tag":"DJ,性感,电子","desc":"跟随节奏跳起来，你就是全场的女王","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_3fabcb5e4b4bf737ac4c88c6ad44bf6c.jpg","width":"799","height":"800","songIds":["8572454","5442951","32996745","18177957","1622632","32996785","18156560","10279737","8572461","31863235","1770723","5547051","8572459","13058379","13849745"]},{"listid":"7035","listenum":"6621","collectnum":"479","title":"美好时光，与歌声和吉他为伴","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_356ab579245c495340ca11412183249d.jpg","tag":"乡村,流行,清晨","desc":"清新脱俗，你不能错过的好音乐","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_17a5859bf0052395d2e4f353e7e79448.jpg","width":"869","height":"869","songIds":["7532440","7391230","440614","8540304","2178522","13685350","7996037","1194796","135791631","1359529","1188633","1183712","446310","239012076","122269837"]},{"listid":"7010","listenum":"52808","collectnum":"533","title":"张口就来的华语金曲","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_555fd0ff82ec34354a31ac8dbf8b1ff9.jpg","tag":"华语,KTV,约会","desc":"去KTV嗨歌全体合唱的不二选择","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_300d6201e057549a1932e1820ef59191.jpg","width":"872","height":"871","songIds":["339744","7316463","1000860","1233757","302996","290008","620023","1175705","32145005","7316502","7317702","7329455","296750","242078437","64772983"]},{"listid":"7016","listenum":"19641","collectnum":"567","title":"私家珍藏，经典R&B","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_8cc77d68eaeb11eb20e2dffa86051999.jpg","tag":"R&B/Soul,说唱,工作","desc":"中毒节奏布鲁斯，耳边吹仙气","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_436c2ba8fbf785b1813f80405d629a6a.jpg","width":"500","height":"500","songIds":["1790591","7380346","1134889","7388724","98834442","7389236","31214306","496237","13685422","1764427","8564751","8281346","7574402","8053544","8185209"]},{"listid":"7008","listenum":"16994","collectnum":"476","title":"别样温柔，治愈我心的阳光","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_832a715042611983e9c6c941c3fbb6db.jpg","tag":"日语,治愈,清晨","desc":"男声与女声同样可以治愈心中的低落","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_66696e781d3d8d218320ab29ec20a19c.jpg","width":"872","height":"871","songIds":["52997836","296304","1315429","13780452","17722754","56046714","514068","19172749","867264","55955446","873801","5583840","13774425","116971503","14423477"]},{"listid":"7015","listenum":"8267","collectnum":"499","title":"抖腿星宿间，和星星一起嗨！","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_b1ebf8cf34bac4f99a22a02f323321c3.jpg","tag":"电子,DJ,夜店","desc":"电音和鬼步舞的完美结合","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_983a75dacd3b7a39fe7a7a41b3cb42af.jpg","width":"872","height":"871","songIds":["14841270","118787217","122790804","10999019","13742706","666792","13742708","8969666","31288696","2588176","31214048","13918905","8969678","8284398","119241250"]},{"listid":"7003","listenum":"8962","collectnum":"514","title":"追寻旅行的意义","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_39225add42839efefe6cda172ebbba9e.jpg","tag":"快乐,旅行,欧美","desc":"伴着耳边的曲调，转换心情，享受眼前的光景","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_4e76bcead5fdbe25f730835819bfdfaa.jpg","width":"872","height":"871","songIds":["8517152","8517160","10263288","10263290","10059245","10558909","1635459","1790591","12337876","448491","8530474","858762","1412567","130130697","409372"]},{"listid":"7006","listenum":"16940","collectnum":"567","title":"行车时的风舞心驰","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_fede4b3c22d72c0c05861de78410eaa2.jpg","tag":"驾驶,清晨,快乐","desc":"最佳的行车伴侣，让耳朵尽情享受","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_c63ba1f3d28d36dbe8a2b4cb5294822e.jpg","width":"872","height":"871","songIds":["2175784","7548128","7564546","8530474","1228205","8525932","7532440","2583867","882441","116464670","752574","2197551","2623086","135791969","262574763"]},{"listid":"7037","listenum":"27220","collectnum":"473","title":"用光所有的运气遇见你","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1fbba80a0c205d1bed0740e00df2db4f.jpg","tag":"悲伤,华语,午后","desc":"命运赐予我们遇见，却不赠予我们永远","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_72bdb56177f76149d743f090d9dd6808.jpg","width":"872","height":"871","songIds":["13949885","13684609","14868550","7327230","233212","1795257","1347200","521408","206818","7316459","130244095","32933484","452199","5837140","930068"]},{"listid":"7000","listenum":"18026","collectnum":"471","title":"甜美声线，想一口吃掉的声音","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_3c928c7b39e13edc829ffbab624738e1.jpg","tag":"日语,ACG,学习","desc":"日本歌姬的声音，一个赛一个甜","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_3aa8383dec49013e17b76db78b123109.jpg","width":"740","height":"741","songIds":["26079686","59209248","18622574","5865670","17719559","1585048","1010104","1439662","557626","857698","589903","66607309","515883","70435116","31211067"]},{"listid":"6984","listenum":"4523","collectnum":"508","title":"大西洋东岸异域的抒情家","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_cd67222b2d9d0c83b8d31752bac84a16.jpg","tag":"小语种,午后,咖啡厅","desc":"在我们生活大陆的另一端，在大西洋的东岸，人们深邃的眼眶中盛着无法用言语诉说的情感。当你和他们对视，就走入他们暗潮涌动的世界......","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_9c299f777988332a5fe442c1a0cb9842.jpg","width":"540","height":"540","songIds":["1485653","2178049","257752197","119240561","71782239","8030830","257777411","692990","1235129","267435437","257778512","259471604","245171276","10557505","257381680"]},{"listid":"6985","listenum":"8726","collectnum":"390","title":"复古爵士布鲁斯，要的就是情怀","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_30a52983980fbb332e5fd6660bb6b971.jpg","tag":"爵士,布鲁斯,R&B/Soul","desc":"音乐里藏着岁月的痕迹","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_e54705f7523a61f85e9656e755f4a461.jpg","width":"539","height":"539","songIds":["1664910","1664902","612853","10226025","8565861","8341430","118691340","2270756","2196834","8257826","7543216","130137794","38276705","8244563","316821"]},{"listid":"6990","listenum":"21532","collectnum":"410","title":"无聊就听歌，好听R&B","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_fc446002bb8f13d548166eaa55bea604.jpg","tag":"R&B/Soul,地铁,午后","desc":"时间的轨道里，我们只是一班孤寂的地铁","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_268996c5613431478de6eefdeca1b6ed.jpg","width":"1200","height":"1200","songIds":["7572329","1790591","2196832","8518868","8052473","8059246","7386676","1483361","31876318","7389880","12484150","13733115","1462497","360408","8532316","651776"]},{"listid":"6991","listenum":"6372","collectnum":"325","title":"酌酒与憩，与神狂欢","pic_300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_35c1a5eaca0c3f0fa5497d3fcb97a403.jpg","tag":"酒吧,性感,欧美","desc":"酒神狄奥尼索斯手中醇美的酒是这些音符最恰当不过的容器","pic_w300":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_f16718b1cecf9c701b372488735d0613.jpg","width":"852","height":"852","songIds":["1414574","1163733","13739441","262112580","7541364","247486084","33777043","245847836","264540397","245847870","257810319","257501512","7567988","243051854","245847897"]}]
     */

    private int error_code;
    private int total;
    private int havemore;
    /**
     * listid : 7013
     * listenum : 51061
     * collectnum : 398
     * title : 有些歌我们一起唱会更美
     * pic_300 : http://business.cdn.qianqian.com/qianqian/pic/bos_client_304cab51951525021bf7d9d1a31d77f6.jpg
     * tag : KTV,约会,经典
     * desc : 用歌声表达心中最恳切的情愫
     * pic_w300 : http://business.cdn.qianqian.com/qianqian/pic/bos_client_45075b779fd5d6d079d8781c0ba3ca6a.jpg
     * width : 872
     * height : 871
     * songIds : ["7318424","7319923","5837564","2131293","124685863","8564144","7332494","7328274","313985","7318602","280015","280015","466135","58801103","7330118"]
     */

    private List<ContentBean> content;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getHavemore() {
        return havemore;
    }

    public void setHavemore(int havemore) {
        this.havemore = havemore;
    }

    public List<ContentBean> getContent() {
        return content;
    }

    public void setContent(List<ContentBean> content) {
        this.content = content;
    }

    public static class ContentBean {
        private String listid;
        private String listenum;
        private String collectnum;
        private String title;
        private String pic_300;
        private String tag;
        private String desc;
        private String pic_w300;
        private String width;
        private String height;
        private List<String> songIds;

        public String getListid() {
            return listid;
        }

        public void setListid(String listid) {
            this.listid = listid;
        }

        public String getListenum() {
            return listenum;
        }

        public void setListenum(String listenum) {
            this.listenum = listenum;
        }

        public String getCollectnum() {
            return collectnum;
        }

        public void setCollectnum(String collectnum) {
            this.collectnum = collectnum;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPic_300() {
            return pic_300;
        }

        public void setPic_300(String pic_300) {
            this.pic_300 = pic_300;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getPic_w300() {
            return pic_w300;
        }

        public void setPic_w300(String pic_w300) {
            this.pic_w300 = pic_w300;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public List<String> getSongIds() {
            return songIds;
        }

        public void setSongIds(List<String> songIds) {
            this.songIds = songIds;
        }
    }
}
