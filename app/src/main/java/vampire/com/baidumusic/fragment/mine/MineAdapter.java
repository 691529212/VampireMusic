package vampire.com.baidumusic.fragment.mine;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.dbtools.DBMyFavoriteList;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/29.
 */
public class MineAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_MineAdapter";

    private Context context;
    private ArrayList<DBMyFavoriteList> dbMyFavoriteLists;

    public void setDbMyFavoriteLists(ArrayList<DBMyFavoriteList> dbMyFavoriteLists) {
        this.dbMyFavoriteLists = dbMyFavoriteLists;
        notifyDataSetChanged();
    }

    public MineAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dbMyFavoriteLists!=null?dbMyFavoriteLists.size():0;
    }

    @Override
    public Object getItem(int position) {
        return dbMyFavoriteLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder;
        if (convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.lv_in_mine,null);
            myViewHolder = new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);

        }else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }

        myViewHolder.name.setText(dbMyFavoriteLists.get(position).getListName());
        NetTool netTool =new NetTool();
        netTool.getImage(dbMyFavoriteLists.get(position).getPic(),myViewHolder.pic);
        return convertView;
    }

    private class MyViewHolder {
        private TextView name;
        private ImageView pic;
        public MyViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.iv_my_favorite_list_name);
             pic = (ImageView) view.findViewById(R.id.iv_my_favorite_list_pic);
        }
    }
}
