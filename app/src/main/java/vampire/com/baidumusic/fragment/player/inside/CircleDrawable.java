package vampire.com.baidumusic.fragment.player.inside;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

/**
 * Created DrawableDemo by *Vampire* on 16/8/29.
 *
 * 自定义圆形图片
 */
/* drawable 与自定义view的区别
 * drawable只能决定画在屏幕上的东西是个什么样子
 * 是抽象的 , 不能再自定义drawable里处理view 事件 逻辑
 *
 * drawable 的好处  不需要改动原始的组件
 */
public class CircleDrawable extends Drawable{
    private static final String TAG = "Vampire_CircleDrawable";

    private Bitmap bitmap ; // 要处理的源图片
    private Paint paint; // 画笔
    private int r ; // 圆形的半径

    public CircleDrawable(Bitmap bitmap) {
        this.bitmap = bitmap;
        // 半径为宽高最小值除2
        r = Math.min(bitmap.getWidth(),bitmap.getHeight())/3;
        paint =new Paint();
        paint.setAntiAlias(true); // 设置抗锯齿

        // 将图片处理成shard , 之后设置给画笔
        // 三个参数  第一个参数 是图片
        // 第二,三参数是 图片的重复属性
        BitmapShader bitmapShader =new BitmapShader(bitmap,
                Shader.TileMode.CLAMP
                , Shader.TileMode.CLAMP);
        // 设置画笔的shard属性 , 可以理解为画笔的花纹 , 用这个画笔画出的东西就是图片.
        // 所以直接画圆 就是圆形图片
        paint.setShader(bitmapShader);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(r,r,r,paint);
    }

    @Override
    public void setAlpha(int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        paint.setColorFilter(colorFilter);
    }

    // 来确定该drawable 具体范围
    @Override
    public int getIntrinsicWidth() {
        return r*2;
    }

    @Override
    public int getIntrinsicHeight() {
        return r*2;
    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
