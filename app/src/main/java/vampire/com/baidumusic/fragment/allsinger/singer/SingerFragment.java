package vampire.com.baidumusic.fragment.allsinger.singer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.values.KeyValues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class SingerFragment extends BaseFragment{
    private static final String TAG = "Vampire_SingerActivity";
    private String url;
    private SingerAdapter adapter;
    private ListView listView;
    private List<SingerBean.ArtistBean> artistBeen;
    private FragmentTransaction transaction;

    @Override
    protected int setLayout() {
        return R.layout.activity_singer;
    }

    @Override
    protected void initView() {
        FragmentManager manager =getFragmentManager();
        transaction = manager.beginTransaction();
        Bundle bundle =getArguments();
        String getTitle =bundle.getString("title");
        url = bundle.getString("url");
        TextView title =bindView(R.id.title_singer);
        title.setText(getTitle);

        listView = bindView(R.id.lv_singer);
        adapter = new SingerAdapter(getContext());
        listView.setAdapter(adapter);
    }

    @Override
    protected void initData() {

        mNetTool.getNetData(url, SingerBean.class, new NetTool.NetListener<SingerBean>() {

            @Override
            public void onSuccess(SingerBean singerBean) {
                artistBeen = singerBean.getArtist();
                adapter.setArtistBeen(artistBeen);
            }

            @Override
            public void onError(String errorMsg) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DetailFragment detailFragment =new DetailFragment();
                Bundle bundle =new Bundle();
                bundle.putString(KeyValues.RANK_TYPE,artistBeen.get(position).getTing_uid());
                bundle.putString(KeyValues.RANK_NAME,artistBeen.get(position).getName());
                bundle.putString("songs_total",artistBeen.get(position).getSongs_total());
                bundle.putString("albums_total",artistBeen.get(position).getAlbums_total());
                bundle.putString(KeyValues.RANK_PIC,artistBeen.get(position).getAvatar_big());
                bundle.putInt(KeyValues.MY_TYPE_ID, 1);
                detailFragment.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(detailFragment);


            }
        });

    }

}
