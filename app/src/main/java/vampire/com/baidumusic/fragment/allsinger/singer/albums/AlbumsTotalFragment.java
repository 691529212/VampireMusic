package vampire.com.baidumusic.fragment.allsinger.singer.albums;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class AlbumsTotalFragment extends BaseFragment {
    private static final String TAG = "Vampire_AlbumsTotalFragment";

    @Override
    protected int setLayout() {
        return R.layout.fragment_total_albums;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
