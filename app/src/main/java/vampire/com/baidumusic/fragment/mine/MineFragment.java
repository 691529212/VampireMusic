package vampire.com.baidumusic.fragment.mine;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.aty.main.game.GameActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.fragment.mine.history.PlayerHistoryFragment;
import vampire.com.baidumusic.fragment.myfavorite.MyFavoriteFragment;
import vampire.com.baidumusic.tools.dbtools.DBHistoryPlay;
import vampire.com.baidumusic.tools.dbtools.DBMyFavoriteList;
import vampire.com.baidumusic.tools.dbtools.DBTool;
import vampire.com.baidumusic.tools.myview.ListViewForScrollView;
import vampire.com.baidumusic.values.KeyValues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MineFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Vampire_MineFragment";
    private LinearLayout local;
    private LinearLayout history;
    private LinearLayout download;
    private LinearLayout sing;
    private LinearLayout myFavorite;
    private LinearLayout createList;
    private int size;
    private TextView myFavoriteNum;
    private MainActivity mainActivity;
    private ArrayList<DBHistoryPlay> dbHistoryPlays;
    private ArrayList<DBMyFavoriteList> dbMyFavoriteLists;
    private MineAdapter mineAdapter;
    private TextView hitoryTv;

    @Override
    protected int setLayout() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView() {

        // 本地音乐
        local = bindView(R.id.mine_local_layout);
        // 最近播放
        history = bindView(R.id.layout_history_player);
        // 下载管理
        download = bindView(R.id.layout_history_download);
        // 我的K歌
        sing = bindView(R.id.layout_history_sing);
        // 我最喜欢的单曲
        myFavorite = bindView(R.id.layout_my_favorite);
        myFavoriteNum = bindView(R.id.tv_my_favorite);

        // 新建歌单
        createList = bindView(R.id.layout_my_create);

        hitoryTv = bindView(R.id.tv_mine_history);

        local.setOnClickListener(this);
        history.setOnClickListener(this);
        download.setOnClickListener(this);
        sing.setOnClickListener(this);
        myFavorite.setOnClickListener(this);
        createList.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
        Log.d(TAG, "do");

    }


    @Override
    protected void initData() {
        DBTool.getInstance().queryDBMyMusicList(new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {
                size = list.size();
                myFavoriteNum.setText("共" + size + "首");
            }
        });

        DBTool.getInstance().queryHistory(new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {
                hitoryTv.setText("共" + list.size() + "首");
                dbHistoryPlays = (ArrayList<DBHistoryPlay>) list;

            }
        });


        ListViewForScrollView listView = bindView(R.id.lv_in_mine);
        mineAdapter = new MineAdapter(getContext());
        listView.setAdapter(mineAdapter);
        final TextView textView = bindView(R.id.tv_no_use);

        DBTool.getInstance().queryDBMfavorite(new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {

                dbMyFavoriteLists = (ArrayList<DBMyFavoriteList>) list;
                if (dbMyFavoriteLists.size() > 0) {
                    textView.setVisibility(View.GONE);
                }else {
                    textView.setVisibility(View.VISIBLE);
                }
                mineAdapter.setDbMyFavoriteLists(dbMyFavoriteLists);


            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(KeyValues.RANK_PIC, dbMyFavoriteLists.get(position).getPic());
                bundle.putString(KeyValues.RANK_TYPE, dbMyFavoriteLists.get(position).getUrl());
                bundle.putString(KeyValues.RANK_NAME, dbMyFavoriteLists.get(position).getListName());
                bundle.putInt(KeyValues.MY_TYPE_ID, dbMyFavoriteLists.get(position).getTypeId());
                DetailFragment fragment = new DetailFragment();
                fragment.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(fragment);
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mine_local_layout:
                PointAnimator(local);

                break;
            case R.id.layout_history_player:
                PointAnimator(history);
                mainActivity = (MainActivity) getActivity();
                PlayerHistoryFragment playerHistoryFragment = new PlayerHistoryFragment();
                mainActivity.updateView(playerHistoryFragment);
                break;
            case R.id.layout_history_download:
                PointAnimator(download);

                break;
            case R.id.layout_history_sing:
                PointAnimator(sing);
                Intent intent = new Intent(getActivity(), GameActivity.class);
                startActivity(intent);

                break;
            case R.id.layout_my_favorite: // 我喜欢的Music;
                PointAnimator(myFavorite);
                mainActivity = (MainActivity) getActivity();
                MyFavoriteFragment myFavoriteFragment = new MyFavoriteFragment();
                mainActivity.updateView(myFavoriteFragment);
                break;
            case R.id.layout_my_create:
                PointAnimator(createList);

                break;
        }

    }

    // 点击动画
    private void PointAnimator(LinearLayout linearLayout) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(linearLayout, "alpha", 1.0f, 0.7f, 0.4f, 0.7f, 1.0f);
        animator.setDuration(800);
        animator.start();
    }
}
