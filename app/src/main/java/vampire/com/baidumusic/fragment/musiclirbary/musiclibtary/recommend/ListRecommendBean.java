package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.recommend;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/16.
 */
public class ListRecommendBean {
    private static final String TAG = "Vampire_ListRecommendBean";
    /**
     * error_code : 22000
     * content : {"title":"热门歌单","list":[{"listid":"7081","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_a9b4fe28152ed0fca952fdca8d611161.jpg","listenum":"5471","collectnum":"470","title":"我的人生中，幸甚有你","tag":"婚礼,清晨,浪漫","type":"gedan"},{"listid":"7012","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_210f981ee6fc3b968a8b278bae6d823b.jpg","listenum":"3273","collectnum":"469","title":"英伦小调，暖暖的阳光和浓郁咖啡香","tag":"英伦,午后,地铁","type":"gedan"},{"listid":"7060","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_f3a08155d59ba922155a163cdbf5989e.jpg","listenum":"2530","collectnum":"331","title":"请享用一杯孤独","tag":"华语,悲伤,午后","type":"gedan"},{"listid":"7031","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_fc75d1691cd1e0ee7acf66df73f509c1.jpg","listenum":"958","collectnum":"411","title":"东海岸说唱集锦","tag":"说唱,经典,地铁","type":"gedan"}]}
     */

    private int error_code;
    /**
     * title : 热门歌单
     * list : [{"listid":"7081","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_a9b4fe28152ed0fca952fdca8d611161.jpg","listenum":"5471","collectnum":"470","title":"我的人生中，幸甚有你","tag":"婚礼,清晨,浪漫","type":"gedan"},{"listid":"7012","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_210f981ee6fc3b968a8b278bae6d823b.jpg","listenum":"3273","collectnum":"469","title":"英伦小调，暖暖的阳光和浓郁咖啡香","tag":"英伦,午后,地铁","type":"gedan"},{"listid":"7060","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_f3a08155d59ba922155a163cdbf5989e.jpg","listenum":"2530","collectnum":"331","title":"请享用一杯孤独","tag":"华语,悲伤,午后","type":"gedan"},{"listid":"7031","pic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_fc75d1691cd1e0ee7acf66df73f509c1.jpg","listenum":"958","collectnum":"411","title":"东海岸说唱集锦","tag":"说唱,经典,地铁","type":"gedan"}]
     */

    private ContentBean content;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }

    public static class ContentBean {
        private String title;
        /**
         * listid : 7081
         * pic : http://business.cdn.qianqian.com/qianqian/pic/bos_client_a9b4fe28152ed0fca952fdca8d611161.jpg
         * listenum : 5471
         * collectnum : 470
         * title : 我的人生中，幸甚有你
         * tag : 婚礼,清晨,浪漫
         * type : gedan
         */

        private List<ListBean> list;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            private String listid;
            private String pic;
            private String listenum;
            private String collectnum;
            private String title;
            private String tag;
            private String type;

            public String getListid() {
                return listid;
            }

            public void setListid(String listid) {
                this.listid = listid;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getListenum() {
                return listenum;
            }

            public void setListenum(String listenum) {
                this.listenum = listenum;
            }

            public String getCollectnum() {
                return collectnum;
            }

            public void setCollectnum(String collectnum) {
                this.collectnum = collectnum;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }
}
