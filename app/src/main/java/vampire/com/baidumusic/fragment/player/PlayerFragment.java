package vampire.com.baidumusic.fragment.player;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.fragment.player.lyirc.LyricFragment;
import vampire.com.baidumusic.fragment.player.inside.PICFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.aty.main.PopBean;
import vampire.com.baidumusic.notification.PlayerNotification;
import vampire.com.baidumusic.tools.TimeTurn;
import vampire.com.baidumusic.tools.dbtools.DBMusicList;
import vampire.com.baidumusic.tools.dbtools.DBTool;
import vampire.com.baidumusic.tools.event.BottomInformationEvent;
import vampire.com.baidumusic.tools.event.PlayerStateEvent;
import vampire.com.baidumusic.tools.event.ProgressEvent;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/20.
 */
public class PlayerFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Vampire_player";
    private SeekBar progressBar;
    private TextView songName;
    private TextView author;
    private MainActivity mainActivity;
    private PopBean popBean;
    private PopupWindow popupWindow;

    private TextView timePlayer;
    private TextView totalTime;

    private boolean page; // 歌词/图片判断
    private boolean playState; // 播放状态
    private int playType ; // 播放模式 (默认顺序) 顺序 / 随机/ 单曲
    private TextView pageState;
    private ViewPager viewPager;
    private ImageView playerMode;
    private int mProgress;
    private CheckBox state;
    private Button btnList;
    private ImageView back;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        EventBus.getDefault().register(this);

        PlayerStateEvent playerStateEvent = new PlayerStateEvent();
        EventBus.getDefault().post(playerStateEvent);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getProgress(ProgressEvent progressEvent) {
        state.setChecked(progressEvent.getProgressBean().getState());
        progressBar.setProgress(progressEvent.getProgressBean().getProgress());
        int time = progressEvent.getProgressBean().getTime();
        int allTime = progressEvent.getProgressBean().getTotalTime();
        timePlayer.setText(TimeTurn.TimeTurn(time));
        totalTime.setText(TimeTurn.TimeTurn(allTime));

        if (!progressEvent.getProgressBean().getSongName().equals(songName.getText())) {
            songName.setText(progressEvent.getProgressBean().getSongName());
            author.setText(progressEvent.getProgressBean().getAuthour());
        }
        if (playType != progressEvent.getProgressBean().getMode()) {
            playType = progressEvent.getProgressBean().getMode();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mainActivity = (MainActivity) getActivity();
        mainActivity.getLinearLayout().setVisibility(View.VISIBLE);
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_player;
    }

    @Override
    protected void initView() {

        progressBar = bindView(R.id.pb_player);
        progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {

                    mProgress = progress;
                    seekBar.setTag(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                EventBus.getDefault().unregister(PlayerFragment.this);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                EventBus.getDefault().register(PlayerFragment.this);
                mProgress = (int) seekBar.getTag();
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.playerSeekTo(mProgress);
            }
        });
//
        timePlayer = bindView(R.id.tv_player_time);
        totalTime = bindView(R.id.tv_player_time_total);
        back = bindView(R.id.back_play);
        back.setOnClickListener(this);

        Button goBack = bindView(R.id.btn_go_back);
        goBack.setOnClickListener(this);
        Button goNext = bindView(R.id.btn_go_next);
        goNext.setOnClickListener(this);
        state = bindView(R.id.btn_player_state);
        btnList = bindView(R.id.btn_player_list);
        btnList.setOnClickListener(this);

//        DBTool.getInstance().querDBMusicListByBoolean(new DBTool.QueryListener() {
//            @Override
//            public <T> void onQueryListener(List<T> list) {
//                List<DBMusicList> dbMusicListlist = (List<DBMusicList>) list;
//                author.setText(dbMusicListlist.get(0).getAuthor());
//                songName.setText(dbMusicListlist.get(0).getSong());
//            }
//        });

        state.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) v;
                boolean isChecked = checkBox.isChecked();
                if (isChecked) {
                    mainActivity = (MainActivity) getActivity();
                    mainActivity.getPlayerBinder().playState(isChecked);
                    playState = false;
                }
                if (!isChecked) {
                    playState = true;
                    mainActivity = (MainActivity) getActivity();
                    mainActivity.getPlayerBinder().playState(false);
                }


            }
        });

        songName = bindView(R.id.tv_palyer_songs_name);
        author = bindView(R.id.tv_player_author);
        pageState = bindView(R.id.tv_lyrc);// 右上角词/图
        pageState.setOnClickListener(this);

        playerMode = bindView(R.id.btn_player_mode);// 播放状态
        playerMode.setOnClickListener(this);

        mainActivity = (MainActivity) getActivity();
        PICFragment picFragment = mainActivity.getPicFragment();
        LyricFragment lyricFragment = mainActivity.getLyricFragment();

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(picFragment);
        fragments.add(lyricFragment);

        viewPager = bindView(R.id.vp_player);
        PlayerAdapter adapter = new PlayerAdapter(getChildFragmentManager());
        adapter.setFragments(fragments);
        viewPager.setAdapter(adapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    page = true;
                    pageState.setText("词");
                } else {
                    page = false;
                    pageState.setText("图");
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    @Override
    protected void initData() {
        setModeBtn(mainActivity.getPlayerBinder().getPlayMode());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        mainActivity = (MainActivity) getActivity();
//        mainActivity.getPlayer().setChecked(!playState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_go_back://上一曲
                mainActivity = (MainActivity) getActivity();
                mainActivity.getPlayerBinder().playerBack();
                break;
            case R.id.btn_go_next: // 下一曲
                mainActivity = (MainActivity) getActivity();
                mainActivity.getPlayerBinder().playerNext();
                break;
            case R.id.tv_lyrc://词图
                if (page == true) {
                    page = false;
                    viewPager.setCurrentItem(1);
                } else {
                    page = true;
                    viewPager.setCurrentItem(0);
                }
                break;
            case R.id.btn_player_mode:
                mainActivity = (MainActivity) getActivity();
                int mode = mainActivity.getPlayerBinder().changeMode();
                setModeBtn(mode);
                break;
            case R.id.back_play:
                // 回退

            case R.id.btn_player_list:

                break;

        }
    }

    private void setModeBtn(int mode){
        switch (mode) {
            case 0: // normal
                playerMode.setImageResource(R.mipmap.normal);
                break;
            case 1: // 单曲
                playerMode.setImageResource(R.mipmap.one);
                break;
            case 2://随机
                playerMode.setImageResource(R.mipmap.random);
                break;
        }
    }


}
