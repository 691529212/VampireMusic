package vampire.com.baidumusic.fragment.allsinger.singgermore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.AllSingerBean;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/18.
 */
public class HotSingerAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_HotSingerAdapter";
    private Context context;
    private List<AllSingerBean.ArtistBean> artistBeen;

    public void setArtistBeen(List<AllSingerBean.ArtistBean> artistBeen) {
        this.artistBeen = artistBeen;
        notifyDataSetChanged();
    }
    public void addArtistBeen(List<AllSingerBean.ArtistBean> artistBeenNew) {
        this.artistBeen.addAll(artistBeenNew);
        notifyDataSetChanged();
    }


    public HotSingerAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return artistBeen!=null?artistBeen.size():0;
    }

    @Override
    public Object getItem(int position) {
        return artistBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        MyViewHolder myViewHolder;
        if (convertView == null){
            convertView =inflater.inflate(R.layout.item_ggv_hot_singer,null);
            myViewHolder =new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        }else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }
        AllSingerBean.ArtistBean artistBean =artistBeen.get(position);
        NetTool netTool =new NetTool();
        netTool.getImage(artistBean.getAvatar_big(),myViewHolder.pic);
        myViewHolder.name.setText(artistBean.getName());

        return convertView;
    }

    private class MyViewHolder {
        private ImageView pic;
        private TextView name;
        public MyViewHolder(View view) {
            pic= (ImageView) view.findViewById(R.id.iv_item_hot_singer_pic);
            name = (TextView) view.findViewById(R.id.tv_item_hot_singer_name);

        }
    }
}
