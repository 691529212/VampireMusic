package vampire.com.baidumusic.fragment.player.lyirc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import android.util.Xml.Encoding;
import android.widget.SlidingDrawer;

import vampire.com.baidumusic.base.MyApp;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class LrcProgress {
    private static final String TAG = "Vampire_LrcProgress";
    private List<LrcContent> lrcList; //List集合存放歌词内容对象
    private LrcContent mLrcContent;     //声明一个歌词内容对象
    private FileWriter fileWriter;

    /**
     * 无参构造函数用来实例化对象
     */
    public LrcProgress() {
        mLrcContent = new LrcContent();
        lrcList = new ArrayList<LrcContent>();
    }

    /**
     * 读取歌词
     * @param path
     * @return
     */
    public String readLRC(String path) {
        //定义一个StringBuilder对象，用来存放歌词内容
        Log.d("Sysout", "read");
        StringBuilder stringBuilder = new StringBuilder();
        File lrcFile = new File(MyApp.getContext().getCacheDir() + "temp.lrc");
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(lrcFile);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }


//        File f = new File(path.replace(".mp3", ".lrc"));

        try {
            URL url = new URL(path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = connection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

//            //创建一个文件输入流对象
//            FileInputStream fis = new FileInputStream(f);
//            InputStreamReader isr = new InputStreamReader(fis, "utf-8");
//            BufferedReader br = new BufferedReader(isr);
            String s = "";
                Log.d("Sysout", "loc");
            while ((s = bufferedReader.readLine()) != null) {
                bufferedWriter.write(s);
                Log.d("Sysout", s);
//                Log.d(TAG, s);
                //替换字符
                s = s.replace("[", "");
                s = s.replace("]", "@");
                stringBuilder.append(s);

                //分离“@”字符
                String splitLrcData[] = s.split("@");
                if (splitLrcData.length > 1) {
                    mLrcContent.setLrcStr(splitLrcData[1]);

                    //处理歌词取得歌曲的时间
                    int lrcTime = time2Str(splitLrcData[0]);

                    mLrcContent.setLrcTime(lrcTime);

                    //添加进列表数组
                    lrcList.add(mLrcContent);

                    //新创建歌词内容对象
                    mLrcContent = new LrcContent();
                }
            }
        }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            stringBuilder.append("木有歌词文件，赶紧去下载！...");
        } catch (IOException e) {
            e.printStackTrace();
            stringBuilder.append("木有读取到歌词哦！");
        }
        try {
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Sysout", "aaaa");
        return MyApp.getContext().getCacheDir() + "temp.lrc";
    }
    /**
     * 解析歌词时间
     * 歌词内容格式如下：
     * [00:02.32]陈奕迅
     * [00:03.43]好久不见
     * [00:05.22]歌词制作  王涛
     * @param timeStr
     * @return
     */
    public int time2Str(String timeStr) {
        timeStr = timeStr.replace(":", ".");
        timeStr = timeStr.replace(".", "@");

        String timeData[] = timeStr.split("@"); //将时间分隔成字符串数组

        //分离出分、秒并转换为整型
        int minute = Integer.parseInt(timeData[0]);
        int second = Integer.parseInt(timeData[1]);
        int millisecond = Integer.parseInt(timeData[2]);

        //计算上一行与下一行的时间转换为毫秒数
        int currentTime = (minute * 60 + second) * 1000 + millisecond * 10;
        return currentTime;
    }
    public List<LrcContent> getLrcList() {
        return lrcList;
    }
}
