package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.recommend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/16.
 */
public class GridViewAdapter extends BaseAdapter {
    private static final String TAG = "Vampire_GridViewAdapter";
    private List<ListRecommendBean.ContentBean.ListBean> listBeen;
    private Context context;

    public GridViewAdapter(Context context, List<ListRecommendBean.ContentBean.ListBean> listBeen) {
        this.context = context;
        this.listBeen = listBeen;
    }

    @Override
    public int getCount() {
        int count =3;
        if (listBeen.size()>=6){
            count =listBeen.size();
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return listBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_item_list,null);
        ImageView pic = (ImageView) view.findViewById(R.id.layout_grid_item_list);
        TextView listenum = (TextView) view.findViewById(R.id.tv_listenum_grid_list_item);
        TextView title = (TextView) view.findViewById(R.id.tv_title_grid_list_item);
        ListRecommendBean.ContentBean.ListBean contentBean =listBeen.get(position);
        NetTool netTool =new NetTool();
        netTool.getImage(contentBean.getPic(),pic);
        listenum.setText(contentBean.getListenum());
        title.setText(contentBean.getTitle());
        return view;
    }
}
