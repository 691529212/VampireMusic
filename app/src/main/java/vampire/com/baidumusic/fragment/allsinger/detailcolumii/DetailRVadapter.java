package vampire.com.baidumusic.fragment.allsinger.detailcolumii;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.Interface.OnClickDetailItem;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.singer.songs.SongsTitalBean;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/19.
 */
public class DetailRVadapter extends RecyclerView.Adapter{
    private static final String TAG = "Vampire_DetailRVadapter";

    private List<SongsTitalBean.SonglistBean> songlistBeen;
    private List<SongListDeatilBean.ContentBean> contentBeen;
    private List<RankDeatilBean.SongListBean> listBeen;
    private Context context;
    private int typeID;
    private RankViewHolder rankViewHolder;
    private OnClickDetailItem onClickDetailItem;

    public void setOnClickDetailItem(OnClickDetailItem onClickDetailItem) {
        this.onClickDetailItem = onClickDetailItem;
        notifyDataSetChanged();
    }

    public void setListBeen(List<RankDeatilBean.SongListBean> listBeen) {
        this.listBeen = listBeen;
        notifyDataSetChanged();
    }

    public void setContentBeen(List<SongListDeatilBean.ContentBean> contentBeen) {
        this.contentBeen = contentBeen;
        notifyDataSetChanged();
    }

    public DetailRVadapter(Context context, int typeID) {
        this.context = context;
        this.typeID =typeID;
        notifyDataSetChanged();
    }

    public void setSonglistBeen(List<SongsTitalBean.SonglistBean> songlistBeen) {
        this.songlistBeen = songlistBeen;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SingerViewHolder singerViewHolder;
        switch (typeID){
            case 1:
                View singerView = LayoutInflater.from(context).inflate(R.layout.lv_item_songs_total,parent,false);
                singerViewHolder =new SingerViewHolder(singerView);
                return singerViewHolder;
            case 2:
                View listView = LayoutInflater.from(context).inflate(R.layout.lv_item_songs_total,parent,false);
                singerViewHolder =new SingerViewHolder(listView);
                return singerViewHolder;
            case 3:
                View rankView =LayoutInflater.from(context).inflate(R.layout.lv_item_rank_total,parent,false);
                rankViewHolder = new RankViewHolder(rankView);
                return rankViewHolder;
        }

        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (typeID){
            case 1:
                SingerViewHolder singerViewHolder = (SingerViewHolder) holder;
                final SongsTitalBean.SonglistBean songlistBean =songlistBeen.get(position);
                singerViewHolder.songs.setText(songlistBean.getTitle());
                singerViewHolder.albums.setText("《"+songlistBean.getAlbum_title()+"》");
                singerViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickDetailItem.OnClickDetailItem(position,songlistBean.getSong_id());
                    }
                });

                break;
            case 2:
                SingerViewHolder listViewHolder = (SingerViewHolder) holder;
                final SongListDeatilBean.ContentBean contentBean =contentBeen.get(position);
                listViewHolder.songs.setText(contentBean.getTitle());
                listViewHolder.albums.setText(contentBean.getAuthor());
                listViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickDetailItem.OnClickDetailItem(position,contentBean.getSong_id());
                    }
                });
                break;
            case 3:
                RankViewHolder rankViewHolder = (RankViewHolder) holder;
                final RankDeatilBean.SongListBean listBean =listBeen.get(position);
                NetTool netTool =new NetTool();
                netTool.getImage(listBean.getPic_big(),rankViewHolder.pic);
                rankViewHolder.songName.setText(listBean.getTitle());
                rankViewHolder.singer.setText(listBean.getAuthor());
                rankViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickDetailItem.OnClickDetailItem(position,listBean.getSong_id());
                    }
                });
                break;

        }

    }

    @Override
    public int getItemCount() {
        int itemCount  = 0;
        switch (typeID){
            case 1:// 歌手
                itemCount =songlistBeen!=null?songlistBeen.size():0;
                break;
            case 2:
                itemCount =contentBeen !=null?contentBeen.size():0;
                break;
            case 3:
                itemCount =listBeen != null?listBeen.size():0;
                break;

        }
        return itemCount;
    }

    private class SingerViewHolder extends RecyclerView.ViewHolder{
        private TextView songs;
        private TextView albums;
        private LinearLayout relativeLayout;
        public SingerViewHolder(View view) {
            super(view);
            songs= (TextView) view.findViewById(R.id.tv_songs_name);
            albums = (TextView) view.findViewById(R.id.tv_albums_name);
            relativeLayout = (LinearLayout) view.findViewById(R.id.layout_song_total);
        }
    }
    public class RankViewHolder extends RecyclerView.ViewHolder{
        private ImageView pic;
        private TextView songName;
        private TextView singer;
        private LinearLayout linearLayout;

        public RankViewHolder(View itemView) {
            super(itemView);
            pic = (ImageView) itemView.findViewById(R.id.iv_item_rank);
            songName = (TextView) itemView.findViewById(R.id.tv_songs_name);
            singer= (TextView) itemView.findViewById(R.id.tv_singer_name_rank);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.layout_song_detial);
        }
    }
}
