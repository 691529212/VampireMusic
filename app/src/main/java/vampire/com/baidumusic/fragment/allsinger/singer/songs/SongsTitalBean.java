package vampire.com.baidumusic.fragment.allsinger.singer.songs;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class SongsTitalBean {
    private static final String TAG = "Vampire_DetailBean";

    /**
     * songlist : [{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2015-05-20","album_no":"4","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/ac66a881bd5cb97ad351936606c37495/266097259/266097259.lrc","hot":"1017199","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,192,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"242078437","title":"演员","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"166816"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-10-08","album_no":"1","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/239771339/239771339.lrc","hot":"527197","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320","has_mv_mobile":1,"toneid":"600907000002830320","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"87967607","title":"丑八怪","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"87093"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2016-07-15","album_no":"10","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/3c65817e138bb6989b7a27b54f23ce3a/265795148/265795148.lrc","hot":"323315","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"242078465","title":"下雨了","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"50751"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2015-05-20","album_no":"5","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/241841385/241841385.lrc","hot":"262012","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,192,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"241838066","title":"绅士","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"41268"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-11-11","album_no":"3","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/238665983/238665983.lrc","hot":"248817","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,160","has_mv_mobile":1,"toneid":"600907000002830308","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"100575177","title":"你还要我怎样","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":0,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"40081"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2016-07-15","album_no":"9","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/ea1b87e9112aaa7bfbba86b8bc954adb/267710713/267710713.lrc","hot":"159591","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"267709069","title":"花儿和少年","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"25056"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2006-06-16","album_no":"2","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/8ba6e582d435cd7239fcb16cdc6d79e6/115360913/115360913.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/7ad1971cbd61fbf46243d9555027c68f/115360939/115360939.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/240397514/240397514.lrc","hot":"107237","file_duration":"261","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"7356091","title":"认真的雪","ting_uid":"2517","author":"薛之谦","album_id":"7327990","album_title":"薛之谦同名专辑","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"17137"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2016-07-15","album_no":"8","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/1b1029b373a062dc2ebb447de0de038c/267710736/267710736.lrc","hot":"74480","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"267709057","title":"Stay Here","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"11207"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2015-10-19","album_no":"1","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/68e7e43292d0ee87c9b33e7ad17a85ef/255982718/255982718.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/c5d6866502b792ce5c77d221eecec273/255982722/255982722.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/256011809/256011809.lrc","hot":"63388","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"255982860","title":"一半","ting_uid":"2517","author":"薛之谦","album_id":"255983039","album_title":"一半","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"9455"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2012-12-27","album_no":"9","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/100612039/100612039.lrc","hot":"60734","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"60058623084","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"33341939","title":"方圆几里","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"9965"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-10-24","album_no":"2","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/127509684/127509684.lrc","hot":"47210","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320","has_mv_mobile":1,"toneid":"600907000002830296","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"92447139","title":"意外","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"8067"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2012-07-20","album_no":"1","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115643258/115643258.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115643283/115643283.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/23366802/23366802.lrc","hot":"43859","file_duration":"0","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"15368282","title":"我知道你都知道","ting_uid":"2517","author":"薛之谦","album_id":"14921267","album_title":"几个薛之谦","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"6934"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2008-12-31","album_no":"1","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115994676/115994676.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115994703/115994703.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/27856231/27856231.lrc","hot":"33083","file_duration":"270","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"129|-1\",\"1\":\"-1|-1\"}","song_id":"264792","title":"传说","ting_uid":"2517","author":"薛之谦","album_id":"7311438","album_title":"深深爱过你","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"5148"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2012-06-25","album_no":"2","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115643258/115643258.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115643283/115643283.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/14921328/14921328.lrc","hot":"31668","file_duration":"0","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"14921266","title":"几个你","ting_uid":"2517","author":"薛之谦","album_id":"14921267","album_title":"几个薛之谦","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"5191"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-11-11","album_no":"8","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/238666006/238666006.lrc","hot":"29552","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,160","has_mv_mobile":1,"toneid":"600907000002830304","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"100575294","title":"其实","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":0,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"4956"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2006-06-16","album_no":"7","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/8ba6e582d435cd7239fcb16cdc6d79e6/115360913/115360913.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/7ad1971cbd61fbf46243d9555027c68f/115360939/115360939.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/240400373/240400373.lrc","hot":"25904","file_duration":"254","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"7349737","title":"黄色枫叶","ting_uid":"2517","author":"薛之谦","album_id":"7327990","album_title":"薛之谦同名专辑","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"4241"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2008-12-31","album_no":"8","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115994676/115994676.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115994703/115994703.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/246647434/246647434.lrc","hot":"22922","file_duration":"257","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"1405184","title":"深深爱过你(今生)","ting_uid":"2517","author":"薛之谦","album_id":"7311438","album_title":"深深爱过你","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"3677"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-11-11","album_no":"6","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/100610581/100610581.lrc","hot":"22759","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,160","has_mv_mobile":0,"toneid":"600907000002830316","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"100575264","title":"等我回家","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":0,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"3849"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2015-07-15","album_no":"7","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/4d869384306d5fca84266010e616eaf0/267710451/267710451.lrc","hot":"20890","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"267709056","title":"小孩","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"3461"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2006-06-16","album_no":"8","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/8ba6e582d435cd7239fcb16cdc6d79e6/115360913/115360913.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/7ad1971cbd61fbf46243d9555027c68f/115360939/115360939.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/240400253/240400253.lrc","hot":"20771","file_duration":"298","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"7352697","title":"钗头凤","ting_uid":"2517","author":"薛之谦","album_id":"7327990","album_title":"薛之谦同名专辑","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"3510"},{"artist_id":"88","all_artist_ting_uid":"2517,210115854","all_artist_id":"88,85209914","language":"国语","publishtime":"2016-06-09","album_no":"1","versions":"现场","pic_big":"http://musicdata.baidu.com/data2/pic/947ea98d14c88e06d3ab922d15bfe826/266415073/266415073.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/8eeff8da3bbcc26402e490efc04a1b2e/266415076/266415076.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/9636f723a23dbba35d7f3422600e16fe/268383774/268383774.lrc","hot":"18383","file_duration":"0","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,256,320","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"266415113","title":"为了遇见你","ting_uid":"2517","author":"薛之谦,李好","album_id":"266415188","album_title":"端午金曲捞","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"2858"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2015-07-15","album_no":"6","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/4af5a75c47239204b8eb22a5ea78c1a1/267710745/267710745.lrc","hot":"16918","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,256,320,flac","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"267709049","title":"一半","ting_uid":"2517","author":"薛之谦","album_id":"241838068","album_title":"初学者","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"2733"},{"artist_id":"88","all_artist_ting_uid":"2517,10561","all_artist_id":"88,1709","language":"国语","publishtime":"2011-10-10","album_no":"7","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115643258/115643258.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115643283/115643283.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/13975041/13975041.lrc","hot":"14574","file_duration":"262","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"600902000009332902","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"10542288","title":"我们爱过就好","ting_uid":"2517","author":"薛之谦,黄龄","album_id":"14921267","album_title":"几个薛之谦","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":1,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"2594"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-11-11","album_no":"4","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/100609719/100609719.lrc","hot":"13526","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,160","has_mv_mobile":1,"toneid":"600907000002830292","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"100575185","title":"有没有","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":0,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"2315"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2008-12-31","album_no":"5","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115994676/115994676.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115994703/115994703.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/219377/219377.lrc","hot":"12864","file_duration":"254","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"600902000006266933","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"219375","title":"我们的世界","ting_uid":"2517","author":"薛之谦","album_id":"7311438","album_title":"深深爱过你","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"1975"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2013-11-11","album_no":"7","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/116000869/116000869.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/116000880/116000880.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/d4b22ca62af0fc1a1c16b37d2e055f37/266645781/266645781.lrc","hot":"10800","file_duration":"0","del_status":"0","resource_type":"0","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,160","has_mv_mobile":0,"toneid":"60058623087","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"100575276","title":"我想起你了","ting_uid":"2517","author":"薛之谦","album_id":"93104033","album_title":"意外","is_first_publish":0,"havehigh":0,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"1826"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2012-08-15","album_no":"5","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/115643258/115643258.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/115643283/115643283.jpg","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/246651613/246651613.lrc","hot":"10442","file_duration":"0","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"64,128,192,237,256,320,flac","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"5963603","title":"我终于成了别人的女人","ting_uid":"2517","author":"薛之谦","album_id":"14921267","album_title":"几个薛之谦","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"1100000000","listen_total":"1571"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2014-01-30","album_no":"2","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/117339196/117339196.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/117339232/117339232.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/14973132/14973132.lrc","hot":"9535","file_duration":"285","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"249963","title":"你过得好吗","ting_uid":"2517","author":"薛之谦","album_id":"7313787","album_title":"你过得好吗","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"1426"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2015-05-20","album_no":"0","versions":"","pic_big":"","pic_small":"","country":"内地","area":"0","lrclink":"http://musicdata.baidu.com/data2/lrc/246975469/246975469.lrc","hot":"7234","file_duration":"0","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"1","all_rate":"24,64,128,192,256,320","has_mv_mobile":0,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"241830295","title":"绅士-薛之谦","ting_uid":"2517","author":"薛之谦","album_id":"0","album_title":"","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":0,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0000000000","listen_total":"1330"},{"artist_id":"88","all_artist_ting_uid":"2517","all_artist_id":"88","language":"国语","publishtime":"2006-06-16","album_no":"1","versions":"","pic_big":"http://musicdata.baidu.com/data2/pic/8ba6e582d435cd7239fcb16cdc6d79e6/115360913/115360913.jpg","pic_small":"http://musicdata.baidu.com/data2/pic/7ad1971cbd61fbf46243d9555027c68f/115360939/115360939.jpg","country":"港台","area":"1","lrclink":"http://musicdata.baidu.com/data2/lrc/240400230/240400230.lrc","hot":"6924","file_duration":"290","del_status":"0","resource_type":"2","resource_type_ext":"0","copy_type":"1","relate_status":"0","all_rate":"24,64,128,192,256,320,flac","has_mv_mobile":1,"toneid":"0","bitrate_fee":"{\"0\":\"0|0\",\"1\":\"0|0\"}","song_id":"7353524","title":"王子归来","ting_uid":"2517","author":"薛之谦","album_id":"7327990","album_title":"薛之谦同名专辑","is_first_publish":0,"havehigh":2,"charge":0,"has_mv":1,"learn":0,"song_source":"web","piao_id":"0","korean_bb_song":"0","mv_provider":"0100000000","listen_total":"1149"}]
     * songnums : 87
     * havemore : 1
     * error_code : 22000
     */

    private String songnums;
    private int havemore;
    private int error_code;
    /**
     * artist_id : 88
     * all_artist_ting_uid : 2517
     * all_artist_id : 88
     * language : 国语
     * publishtime : 2015-05-20
     * album_no : 4
     * versions :
     * pic_big : http://musicdata.baidu.com/data2/pic/76dc8dc35a361ef018c2c52befabfb03/267709259/267709259.jpg
     * pic_small : http://musicdata.baidu.com/data2/pic/eede55e93e4f0353b1eea0a7627e7be1/267709262/267709262.jpg
     * country : 内地
     * area : 0
     * lrclink : http://musicdata.baidu.com/data2/lrc/ac66a881bd5cb97ad351936606c37495/266097259/266097259.lrc
     * hot : 1017199
     * file_duration : 0
     * del_status : 0
     * resource_type : 0
     * resource_type_ext : 0
     * copy_type : 1
     * relate_status : 0
     * all_rate : 64,128,192,256,320,flac
     * has_mv_mobile : 0
     * toneid : 0
     * bitrate_fee : {"0":"0|0","1":"0|0"}
     * song_id : 242078437
     * title : 演员
     * ting_uid : 2517
     * author : 薛之谦
     * album_id : 241838068
     * album_title : 初学者
     * is_first_publish : 0
     * havehigh : 2
     * charge : 0
     * has_mv : 0
     * learn : 1
     * song_source : web
     * piao_id : 0
     * korean_bb_song : 0
     * mv_provider : 0000000000
     * listen_total : 166816
     */

    private List<SonglistBean> songlist;

    public String getSongnums() {
        return songnums;
    }

    public void setSongnums(String songnums) {
        this.songnums = songnums;
    }

    public int getHavemore() {
        return havemore;
    }

    public void setHavemore(int havemore) {
        this.havemore = havemore;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public List<SonglistBean> getSonglist() {
        return songlist;
    }

    public void setSonglist(List<SonglistBean> songlist) {
        this.songlist = songlist;
    }

    public static class SonglistBean {
        private String artist_id;
        private String all_artist_ting_uid;
        private String all_artist_id;
        private String language;
        private String publishtime;
        private String album_no;
        private String versions;
        private String pic_big;
        private String pic_small;
        private String country;
        private String area;
        private String lrclink;
        private String hot;
        private String file_duration;
        private String del_status;
        private String resource_type;
        private String resource_type_ext;
        private String copy_type;
        private String relate_status;
        private String all_rate;
        private int has_mv_mobile;
        private String toneid;
        private String bitrate_fee;
        private String song_id;
        private String title;
        private String ting_uid;
        private String author;
        private String album_id;
        private String album_title;
        private int is_first_publish;
        private int havehigh;
        private int charge;
        private int has_mv;
        private int learn;
        private String song_source;
        private String piao_id;
        private String korean_bb_song;
        private String mv_provider;
        private String listen_total;

        public String getArtist_id() {
            return artist_id;
        }

        public void setArtist_id(String artist_id) {
            this.artist_id = artist_id;
        }

        public String getAll_artist_ting_uid() {
            return all_artist_ting_uid;
        }

        public void setAll_artist_ting_uid(String all_artist_ting_uid) {
            this.all_artist_ting_uid = all_artist_ting_uid;
        }

        public String getAll_artist_id() {
            return all_artist_id;
        }

        public void setAll_artist_id(String all_artist_id) {
            this.all_artist_id = all_artist_id;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getPublishtime() {
            return publishtime;
        }

        public void setPublishtime(String publishtime) {
            this.publishtime = publishtime;
        }

        public String getAlbum_no() {
            return album_no;
        }

        public void setAlbum_no(String album_no) {
            this.album_no = album_no;
        }

        public String getVersions() {
            return versions;
        }

        public void setVersions(String versions) {
            this.versions = versions;
        }

        public String getPic_big() {
            return pic_big;
        }

        public void setPic_big(String pic_big) {
            this.pic_big = pic_big;
        }

        public String getPic_small() {
            return pic_small;
        }

        public void setPic_small(String pic_small) {
            this.pic_small = pic_small;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getLrclink() {
            return lrclink;
        }

        public void setLrclink(String lrclink) {
            this.lrclink = lrclink;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getFile_duration() {
            return file_duration;
        }

        public void setFile_duration(String file_duration) {
            this.file_duration = file_duration;
        }

        public String getDel_status() {
            return del_status;
        }

        public void setDel_status(String del_status) {
            this.del_status = del_status;
        }

        public String getResource_type() {
            return resource_type;
        }

        public void setResource_type(String resource_type) {
            this.resource_type = resource_type;
        }

        public String getResource_type_ext() {
            return resource_type_ext;
        }

        public void setResource_type_ext(String resource_type_ext) {
            this.resource_type_ext = resource_type_ext;
        }

        public String getCopy_type() {
            return copy_type;
        }

        public void setCopy_type(String copy_type) {
            this.copy_type = copy_type;
        }

        public String getRelate_status() {
            return relate_status;
        }

        public void setRelate_status(String relate_status) {
            this.relate_status = relate_status;
        }

        public String getAll_rate() {
            return all_rate;
        }

        public void setAll_rate(String all_rate) {
            this.all_rate = all_rate;
        }

        public int getHas_mv_mobile() {
            return has_mv_mobile;
        }

        public void setHas_mv_mobile(int has_mv_mobile) {
            this.has_mv_mobile = has_mv_mobile;
        }

        public String getToneid() {
            return toneid;
        }

        public void setToneid(String toneid) {
            this.toneid = toneid;
        }

        public String getBitrate_fee() {
            return bitrate_fee;
        }

        public void setBitrate_fee(String bitrate_fee) {
            this.bitrate_fee = bitrate_fee;
        }

        public String getSong_id() {
            return song_id;
        }

        public void setSong_id(String song_id) {
            this.song_id = song_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTing_uid() {
            return ting_uid;
        }

        public void setTing_uid(String ting_uid) {
            this.ting_uid = ting_uid;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getAlbum_id() {
            return album_id;
        }

        public void setAlbum_id(String album_id) {
            this.album_id = album_id;
        }

        public String getAlbum_title() {
            return album_title;
        }

        public void setAlbum_title(String album_title) {
            this.album_title = album_title;
        }

        public int getIs_first_publish() {
            return is_first_publish;
        }

        public void setIs_first_publish(int is_first_publish) {
            this.is_first_publish = is_first_publish;
        }

        public int getHavehigh() {
            return havehigh;
        }

        public void setHavehigh(int havehigh) {
            this.havehigh = havehigh;
        }

        public int getCharge() {
            return charge;
        }

        public void setCharge(int charge) {
            this.charge = charge;
        }

        public int getHas_mv() {
            return has_mv;
        }

        public void setHas_mv(int has_mv) {
            this.has_mv = has_mv;
        }

        public int getLearn() {
            return learn;
        }

        public void setLearn(int learn) {
            this.learn = learn;
        }

        public String getSong_source() {
            return song_source;
        }

        public void setSong_source(String song_source) {
            this.song_source = song_source;
        }

        public String getPiao_id() {
            return piao_id;
        }

        public void setPiao_id(String piao_id) {
            this.piao_id = piao_id;
        }

        public String getKorean_bb_song() {
            return korean_bb_song;
        }

        public void setKorean_bb_song(String korean_bb_song) {
            this.korean_bb_song = korean_bb_song;
        }

        public String getMv_provider() {
            return mv_provider;
        }

        public void setMv_provider(String mv_provider) {
            this.mv_provider = mv_provider;
        }

        public String getListen_total() {
            return listen_total;
        }

        public void setListen_total(String listen_total) {
            this.listen_total = listen_total;
        }
    }
}
