package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.ranking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import vampire.com.baidumusic.R;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/16.
 */
public class LvInLvAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_LvInLvAdapter";
    private Context context;
    private ArrayList<RankFragmentBean.ContentBean.ContentBeans> contentBeanses;

    public LvInLvAdapter(ArrayList<RankFragmentBean.ContentBean.ContentBeans> contentBeanses, Context context) {
        this.contentBeanses = contentBeanses;
        this.context = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return contentBeanses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =LayoutInflater.from(context);
        MyViewHolder myViewHolder;
        if (convertView==null){
            convertView=inflater.inflate(R.layout.lv_in_lv_item,null);
            myViewHolder=new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        }else {
            myViewHolder= (MyViewHolder) convertView.getTag();
        }
        RankFragmentBean.ContentBean.ContentBeans beans =contentBeanses.get(position);
        myViewHolder.firstName.setText(beans.getAuthor());
        myViewHolder.firstMusic.setText(beans.getTitle());
        // 三甲颜色渐变
        int[] color ={0xffec0f1e,0xfff37a25,0xfff3e545};
        // 三甲数字变化
        String[] num ={"1","2","3"};
        myViewHolder.rank.setText(num[position]);
        myViewHolder.rank.setTextColor(color[position]);
        return convertView;
    }

    private class MyViewHolder {
        private TextView firstName;
        private TextView firstMusic;
        private TextView rank;
        private MyViewHolder(View view){
            firstName = (TextView) view.findViewById(R.id.tv_rank_author_first);
            firstMusic= (TextView) view.findViewById(R.id.tv_rank_title_first);
            rank= (TextView) view.findViewById(R.id.tv_in_lv_rank);
        }

    }
}
