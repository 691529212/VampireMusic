package vampire.com.baidumusic.fragment.musiclirbary;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import vampire.com.baidumusic.values.TitleValues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MusicLibraryAdapter extends FragmentPagerAdapter {
    private static final String TAG = "Vampire_MusicLirbiryAdapter";
    private ArrayList <Fragment> fragments;

    public MusicLibraryAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments!=null?fragments.size():0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TitleValues.MUSIC_LIBRARY_TITLES[position];
    }
}
