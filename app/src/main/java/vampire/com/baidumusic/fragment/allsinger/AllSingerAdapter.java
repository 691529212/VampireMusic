package vampire.com.baidumusic.fragment.allsinger;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.Interface.OnclickAllSingerPress;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class AllSingerAdapter  extends PagerAdapter {
    private static final String TAG = "Vampire_AllSingerAdapter";
    private Context context;
    private List<AllSingerBean.ArtistBean> artistBeen;
    private OnclickAllSingerPress onclickAllSingerPress;
    private ImageView pic1;
    private ImageView pic2;
    private ImageView pic3;


    public void setArtistBeen(List<AllSingerBean.ArtistBean> artistBeen) {
        this.artistBeen = artistBeen;
        notifyDataSetChanged();
    }

    public void setOnclickAllSingerPress(OnclickAllSingerPress onclickAllSingerPress) {
        this.onclickAllSingerPress = onclickAllSingerPress;
    }

    public AllSingerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_vp_hot_singer,null);
        pic1 = (ImageView) view.findViewById(R.id.iv_hot_pic_one);
        pic2 = (ImageView) view.findViewById(R.id.iv_hot_pic_two);
        pic3 = (ImageView) view.findViewById(R.id.iv_hot_pic_three);

        TextView name1 = (TextView) view.findViewById(R.id.tv_hot_name_one);
        TextView name2 = (TextView) view.findViewById(R.id.tv_hot_name_two);
        TextView name3 = (TextView) view.findViewById(R.id.tv_hot_name_three);

        int i =position;
        if (i!=0){
            i=position*3;
        }
        NetTool netTool =new NetTool();
        netTool.getImage(artistBeen.get(i).getAvatar_big(), pic1);
        netTool.getImage(artistBeen.get(i+1).getAvatar_big(), pic2);
        netTool.getImage(artistBeen.get(i+2).getAvatar_big(), pic3);

        name1.setText(artistBeen.get(i).getName());
        name2.setText(artistBeen.get(i+1).getName());
        name3.setText(artistBeen.get(i+2).getName());

        final int finalI = i;
        pic1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onclickAllSingerPress!=null){
                    AllSingerBean.ArtistBean artistBean = artistBeen.get(finalI);
                    onclickAllSingerPress.onclickAllSingerPress(
                            artistBean.getTing_uid(),
                            artistBean.getSongs_total(),
                            artistBean.getAlbums_total(),
                            artistBean.getAvatar_big(),
                            artistBean.getName());
                }
            }
        });
        pic2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onclickAllSingerPress!=null){
                    AllSingerBean.ArtistBean artistBean = artistBeen.get(finalI+1);
                    onclickAllSingerPress.onclickAllSingerPress(artistBean.getTing_uid(),
                            artistBean.getSongs_total(),
                            artistBean.getAlbums_total(),
                            artistBean.getAvatar_big(),
                            artistBean.getName());
                }
            }
        });
        pic3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onclickAllSingerPress!=null){
                    AllSingerBean.ArtistBean artistBean = artistBeen.get(finalI+2);
                    onclickAllSingerPress.onclickAllSingerPress(artistBean.getTing_uid(),
                            artistBean.getSongs_total(),
                            artistBean.getAlbums_total(),
                            artistBean.getAvatar_big(),
                            artistBean.getName());
                }
            }
        });

        container.addView(view);
        return view;
    }






    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(container);
    }
}
