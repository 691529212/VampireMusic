package vampire.com.baidumusic.fragment.allsinger.detailcolumii;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/18.
 */
public class DetailVPAdapter extends FragmentPagerAdapter{
    private static final String TAG = "Vampire_DetailVPAdapter";
    private ArrayList<Fragment> fragments;
    private ArrayList<String> title;

    public void setTitle(ArrayList<String> title) {
        this.title = title;
        notifyDataSetChanged();
    }

    public void setFragments(ArrayList<Fragment> fragments) {
        this.fragments = fragments;
        notifyDataSetChanged();
    }

    public DetailVPAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments!=null?fragments.size():0;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return title.get(position);
    }
}
