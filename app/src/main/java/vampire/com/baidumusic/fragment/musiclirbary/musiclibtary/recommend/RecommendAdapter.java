package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.recommend;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.LoopPagerAdapter;

import java.util.ArrayList;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class RecommendAdapter extends LoopPagerAdapter {
    private static final String TAG = "Vampire_RecommendAdapter";
    private ArrayList<RecommendBean.PicBean> picBeens;
    private Context mContext;

    public RecommendAdapter(RollPagerView viewPager, Context mContext) {
        super(viewPager);
        this.mContext = mContext;
    }

    public RecommendAdapter(RollPagerView viewPager) {
        super(viewPager);
    }

    public void setPicBeens(ArrayList<RecommendBean.PicBean> picBeens) {
        this.picBeens = picBeens;
        notifyDataSetChanged();
    }




    @Override
    public int getRealCount() {
        return picBeens !=null?picBeens.size():0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view == object;
    }


    @Override
    public View getView(ViewGroup container, int position) {
        ImageView view = new ImageView(container.getContext());
        NetTool netTool =new NetTool();

        netTool.getImage(String.valueOf(picBeens.get(position).getRandpic()),view);
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(container);
    }
}
