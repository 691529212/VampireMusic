package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.mv;

import java.util.List;

/**
 * Created vampires by *Vampire* on 16/8/31.
 */
public class MvListBean {

    /**
     * error_code : 22000
     * result : {"total":967989,"havemore":"1","mv_list":[{"mv_id":"134667557","all_artist_id":"18097268","title":"EXO MV20合1完整版合集","aliastitle":"EXO MV20合1完整版合集","subtitle":"EXO MV20合1完整版合集","play_nums":"4","publishtime":"2015-02-06","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/2f9210da185fd8f97ad0d9362bf28b18/269396578/269396578.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/b87f6c40e9978722d26ae6c4233ce3ab/269396579/269396579.jpg","artist":"EXO","provider":"11"},{"mv_id":"242437750","all_artist_id":"115035175","title":"BANG BANG BANG Cover版","aliastitle":"BANG BANG BANG Cover版","subtitle":"Sandy&Mandy - BANG BANG BANG Cover版","play_nums":"0","publishtime":"2015-06-29","del_status":"0","artist_id":"115035175","thumbnail":"http://qukufile2.qianqian.com/data2/pic/245afcdc9f57e2810ce6a88bfe9242bc/269435536/269435536.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/17721778d4e8112573286a64439e7b4e/269435537/269435537.jpg","artist":"Sandy&Mandy","provider":"11"},{"mv_id":"238565981","all_artist_id":"20723846,55635","title":"快乐新春","aliastitle":"快乐新春","subtitle":"张艺兴&柳岩 - 快乐新春 央视网络春晚 现场版","play_nums":"0","publishtime":"2015-03-06","del_status":"0","artist_id":"20723846","thumbnail":"http://qukufile2.qianqian.com/data2/pic/3b987df633aa139933bd994950d1025c/269394988/269394988.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/c27c1b825e1b57cae5ba806a0321069b/269394990/269394990.jpg","artist":"张艺兴","provider":"11"},{"mv_id":"124868519","all_artist_id":"313607","title":"2014韩流梦想演唱会 完整版 14/10/12","aliastitle":"","subtitle":"2014韩流梦想演唱会 完整版 14/10/12","play_nums":"0","publishtime":"2014-12-05","del_status":"0","artist_id":"313607","thumbnail":"http://qukufile2.qianqian.com/data2/pic/8dd18ee279fa511df66c6f347bca5557/269400229/269400229.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/f7ee9c6b830dd2656f7a55cd266abc95/269400230/269400230.jpg","artist":"群星","provider":"11"},{"mv_id":"124865500","all_artist_id":"313607","title":"SBS 2014梦想演唱会 完整版 14/06/15","aliastitle":"","subtitle":"SBS 2014梦想演唱会 完整版 14/06/15","play_nums":"0","publishtime":"2014-12-05","del_status":"0","artist_id":"313607","thumbnail":"http://qukufile2.qianqian.com/data2/pic/659829febdca2f51f92b832b122d82c1/269396547/269396547.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/2475aa6621e449a1bed4c75d719d955a/269396548/269396548.jpg","artist":"群星","provider":"11"},{"mv_id":"242469443","all_artist_id":"772,121515348,238759127,14,157,6137","title":"真心英雄","aliastitle":"真心英雄","subtitle":"杨坤 & 佟大为 & 郑元畅 & 张杰 & 朱亚文 & 陈学冬 - 真心英雄 真人秀真心英雄主题曲","play_nums":"0","publishtime":"2015-06-30","del_status":"0","artist_id":"772","thumbnail":"http://qukufile2.qianqian.com/data2/pic/4f79d2ba37b402836c513d47c8ba8713/269374022/269374022.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/a84f0a6b7c6dcbcf5d235091624148f1/269374023/269374023.jpg","artist":"杨坤","provider":"11"},{"mv_id":"107668813","all_artist_id":"434","title":"I Wanna Hold You","aliastitle":"I Wanna Hold You","subtitle":"东方神起 - I Wanna Hold You 演唱会现场版","play_nums":"6","publishtime":"2013-12-23","del_status":"0","artist_id":"434","thumbnail":"http://qukufile2.qianqian.com/data2/pic/dfc3b92d97aced47bb10ff5fb7b36734/269393697/269393697.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/6a483e57cf9fb57eefbacc86c3e8baf7/269393698/269393698.jpg","artist":"东方神起","provider":"11"},{"mv_id":"133330653","all_artist_id":"18097268","title":"月光+上瘾 - 4th Gaon Chart K-Pop Awards","aliastitle":"月光+上瘾","subtitle":"EXO - 月光+上瘾 - 4th Gaon Chart K-Pop Awards 现场版 15/01/28","play_nums":"0","publishtime":"2015-01-29","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/620c61e8b919960c73497627581762e6/269393360/269393360.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/d32b81b361d848092488425e78bb46be/269393361/269393361.jpg","artist":"EXO","provider":"11"},{"mv_id":"247139979","all_artist_id":"1702","title":"战士","aliastitle":"","subtitle":"陈伟霆 - 战士 英皇盛世十周年巨星演唱会现场版","play_nums":"0","publishtime":"2015-09-15","del_status":"0","artist_id":"1702","thumbnail":"http://qukufile2.qianqian.com/data2/pic/c84d75192515cbe7dd0019146a94d379/269393356/269393356.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/7278506e8c01f8cd6ceb6e1e4cf10344/269393357/269393357.jpg","artist":"陈伟霆","provider":"11"},{"mv_id":"242461879","all_artist_id":"104","title":"光辉岁月","aliastitle":"光辉岁月","subtitle":"黄家驹 - 光辉岁月","play_nums":"1","publishtime":"2015-06-30","del_status":"0","artist_id":"104","thumbnail":"http://qukufile2.qianqian.com/data2/pic/b356492625ef7bb424795bfbdeffa3f0/269373869/269373869.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/bb8fad7ef7be742c18268fca83b7343e/269373870/269373870.jpg","artist":"黄家驹","provider":"11"},{"mv_id":"108181560","all_artist_id":"3571248,194","title":"Bad Girls +","aliastitle":"Bad Girls +","subtitle":"李孝利 & CL - Bad Girls + 坏丫头 2013SBS歌谣大战 现场版 13/12/29","play_nums":"0","publishtime":"2013-12-30","del_status":"0","artist_id":"3571248","thumbnail":"http://qukufile2.qianqian.com/data2/pic/ab131c7a7c65a16efc1d19481f8b309f/269372615/269372615.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/11c1b815961b7ac9acedcc22fe175447/269372616/269372616.jpg","artist":"CL","provider":"11"},{"mv_id":"246383524","all_artist_id":"1999","title":"给我的快乐","aliastitle":"","subtitle":"胡夏 - 给我的快乐 电视剧神犬小七主题曲","play_nums":"6","publishtime":"2015-08-06","del_status":"0","artist_id":"1999","thumbnail":"http://qukufile2.qianqian.com/data2/pic/9702b112da49302673363d3bfa2a8ed9/269371967/269371967.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/60d2bcd76364739336caba0e042aee5c/269371968/269371968.jpg","artist":"胡夏","provider":"11"},{"mv_id":"239019447","all_artist_id":"20821946","title":"PRAY预告","aliastitle":"PRAY预告","subtitle":"FTISLAND - PRAY预告","play_nums":"0","publishtime":"2015-03-20","del_status":"0","artist_id":"20821946","thumbnail":"http://qukufile2.qianqian.com/data2/pic/399996c361962a45fe57d9d7871582ef/269371960/269371960.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/d17f9da58e08a0d3da9020680317f82b/269371961/269371961.jpg","artist":"ft island","provider":"11"},{"mv_id":"109014022","all_artist_id":"88","title":"有没有","aliastitle":"有没有","subtitle":"薛之谦 - 有没有","play_nums":"10022","publishtime":"2014-01-13","del_status":"0","artist_id":"88","thumbnail":"http://qukufile2.qianqian.com/data2/pic/6d2963ef4d7559bbee0185214ad5b5a6/269364711/269364711.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/d564b474ce4d81ff99ff29256bdb9ca7/269364712/269364712.jpg","artist":"薛之谦","provider":"11"},{"mv_id":"246840646","all_artist_id":"14357526","title":"因为红","aliastitle":"","subtitle":"泫雅 - 因为红 舞蹈练习","play_nums":"0","publishtime":"2015-08-28","del_status":"0","artist_id":"14357526","thumbnail":"http://qukufile2.qianqian.com/data2/pic/8f53a8dc43e11c3673af9745d3c3e6db/269353118/269353118.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/ddc7387dd27f84bb21c9ccbb0ac1afe5/269353119/269353119.jpg","artist":"泫雅","provider":"11"},{"mv_id":"116534792","all_artist_id":"18097268","title":"一起吃苦的幸福","aliastitle":"","subtitle":"EXO - 一起吃苦的幸福 - 不朽之名曲 现场版 2014/03/15","play_nums":"45318","publishtime":"2014-03-17","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/e449cf069138dd7daa0bd14aedba56c1/269363879/269363879.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/27454fc5cc6907772e2a44c592421623/269363880/269363880.jpg","artist":"EXO","provider":"11"},{"mv_id":"247598114","all_artist_id":"23330032","title":"心电感应","aliastitle":"","subtitle":"Simon D - 心电感应 蒙面歌王 现场版 15/09/20","play_nums":"0","publishtime":"2015-09-22","del_status":"0","artist_id":"23330032","thumbnail":"http://qukufile2.qianqian.com/data2/pic/8a233bd1d183ab9f19769ac15f73b9ed/269363874/269363874.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/1383c39cb48c315aa99f0b2351a623be/269363875/269363875.jpg","artist":"Simon D","provider":"11"},{"mv_id":"245950501","all_artist_id":"1786","title":"IF YOU","aliastitle":"","subtitle":"BIGBANG - IF YOU 新加坡演唱会 现场版","play_nums":"0","publishtime":"2015-07-27","del_status":"0","artist_id":"1786","thumbnail":"http://qukufile2.qianqian.com/data2/pic/3b67b5a56f4356663441a2527489d75d/269363867/269363867.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/105dbe1f1bf42f3392eda993bd60bc4e/269363868/269363868.jpg","artist":"BigBang","provider":"11"},{"mv_id":"118177477","all_artist_id":"18097268","title":"Alright Alright","aliastitle":"","subtitle":"EXO - Alright Alright  现场版 14/04/15","play_nums":"1","publishtime":"2014-04-17","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/578abf02fd131b826c68ac6fa6107c47/269353664/269353664.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/5f8ec53a337e6d870f9c299094c99a78/269353665/269353665.jpg","artist":"EXO","provider":"11"},{"mv_id":"115100249","all_artist_id":"25815210,831","title":"呼吸","aliastitle":"呼吸","subtitle":"Chen & 张力尹 - 呼吸 中文字幕版","play_nums":"0","publishtime":"2014-02-27","del_status":"0","artist_id":"25815210","thumbnail":"http://qukufile2.qianqian.com/data2/pic/5173a61a67666e35e180387c210aed27/269316015/269316015.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/b9d9185e2351253c93a1cca3b07d4a32/269316016/269316016.jpg","artist":"CHEN","provider":"11"}]}
     */

    private int error_code;
    /**
     * total : 967989
     * havemore : 1
     * mv_list : [{"mv_id":"134667557","all_artist_id":"18097268","title":"EXO MV20合1完整版合集","aliastitle":"EXO MV20合1完整版合集","subtitle":"EXO MV20合1完整版合集","play_nums":"4","publishtime":"2015-02-06","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/2f9210da185fd8f97ad0d9362bf28b18/269396578/269396578.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/b87f6c40e9978722d26ae6c4233ce3ab/269396579/269396579.jpg","artist":"EXO","provider":"11"},{"mv_id":"242437750","all_artist_id":"115035175","title":"BANG BANG BANG Cover版","aliastitle":"BANG BANG BANG Cover版","subtitle":"Sandy&Mandy - BANG BANG BANG Cover版","play_nums":"0","publishtime":"2015-06-29","del_status":"0","artist_id":"115035175","thumbnail":"http://qukufile2.qianqian.com/data2/pic/245afcdc9f57e2810ce6a88bfe9242bc/269435536/269435536.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/17721778d4e8112573286a64439e7b4e/269435537/269435537.jpg","artist":"Sandy&Mandy","provider":"11"},{"mv_id":"238565981","all_artist_id":"20723846,55635","title":"快乐新春","aliastitle":"快乐新春","subtitle":"张艺兴&柳岩 - 快乐新春 央视网络春晚 现场版","play_nums":"0","publishtime":"2015-03-06","del_status":"0","artist_id":"20723846","thumbnail":"http://qukufile2.qianqian.com/data2/pic/3b987df633aa139933bd994950d1025c/269394988/269394988.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/c27c1b825e1b57cae5ba806a0321069b/269394990/269394990.jpg","artist":"张艺兴","provider":"11"},{"mv_id":"124868519","all_artist_id":"313607","title":"2014韩流梦想演唱会 完整版 14/10/12","aliastitle":"","subtitle":"2014韩流梦想演唱会 完整版 14/10/12","play_nums":"0","publishtime":"2014-12-05","del_status":"0","artist_id":"313607","thumbnail":"http://qukufile2.qianqian.com/data2/pic/8dd18ee279fa511df66c6f347bca5557/269400229/269400229.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/f7ee9c6b830dd2656f7a55cd266abc95/269400230/269400230.jpg","artist":"群星","provider":"11"},{"mv_id":"124865500","all_artist_id":"313607","title":"SBS 2014梦想演唱会 完整版 14/06/15","aliastitle":"","subtitle":"SBS 2014梦想演唱会 完整版 14/06/15","play_nums":"0","publishtime":"2014-12-05","del_status":"0","artist_id":"313607","thumbnail":"http://qukufile2.qianqian.com/data2/pic/659829febdca2f51f92b832b122d82c1/269396547/269396547.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/2475aa6621e449a1bed4c75d719d955a/269396548/269396548.jpg","artist":"群星","provider":"11"},{"mv_id":"242469443","all_artist_id":"772,121515348,238759127,14,157,6137","title":"真心英雄","aliastitle":"真心英雄","subtitle":"杨坤 & 佟大为 & 郑元畅 & 张杰 & 朱亚文 & 陈学冬 - 真心英雄 真人秀真心英雄主题曲","play_nums":"0","publishtime":"2015-06-30","del_status":"0","artist_id":"772","thumbnail":"http://qukufile2.qianqian.com/data2/pic/4f79d2ba37b402836c513d47c8ba8713/269374022/269374022.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/a84f0a6b7c6dcbcf5d235091624148f1/269374023/269374023.jpg","artist":"杨坤","provider":"11"},{"mv_id":"107668813","all_artist_id":"434","title":"I Wanna Hold You","aliastitle":"I Wanna Hold You","subtitle":"东方神起 - I Wanna Hold You 演唱会现场版","play_nums":"6","publishtime":"2013-12-23","del_status":"0","artist_id":"434","thumbnail":"http://qukufile2.qianqian.com/data2/pic/dfc3b92d97aced47bb10ff5fb7b36734/269393697/269393697.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/6a483e57cf9fb57eefbacc86c3e8baf7/269393698/269393698.jpg","artist":"东方神起","provider":"11"},{"mv_id":"133330653","all_artist_id":"18097268","title":"月光+上瘾 - 4th Gaon Chart K-Pop Awards","aliastitle":"月光+上瘾","subtitle":"EXO - 月光+上瘾 - 4th Gaon Chart K-Pop Awards 现场版 15/01/28","play_nums":"0","publishtime":"2015-01-29","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/620c61e8b919960c73497627581762e6/269393360/269393360.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/d32b81b361d848092488425e78bb46be/269393361/269393361.jpg","artist":"EXO","provider":"11"},{"mv_id":"247139979","all_artist_id":"1702","title":"战士","aliastitle":"","subtitle":"陈伟霆 - 战士 英皇盛世十周年巨星演唱会现场版","play_nums":"0","publishtime":"2015-09-15","del_status":"0","artist_id":"1702","thumbnail":"http://qukufile2.qianqian.com/data2/pic/c84d75192515cbe7dd0019146a94d379/269393356/269393356.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/7278506e8c01f8cd6ceb6e1e4cf10344/269393357/269393357.jpg","artist":"陈伟霆","provider":"11"},{"mv_id":"242461879","all_artist_id":"104","title":"光辉岁月","aliastitle":"光辉岁月","subtitle":"黄家驹 - 光辉岁月","play_nums":"1","publishtime":"2015-06-30","del_status":"0","artist_id":"104","thumbnail":"http://qukufile2.qianqian.com/data2/pic/b356492625ef7bb424795bfbdeffa3f0/269373869/269373869.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/bb8fad7ef7be742c18268fca83b7343e/269373870/269373870.jpg","artist":"黄家驹","provider":"11"},{"mv_id":"108181560","all_artist_id":"3571248,194","title":"Bad Girls +","aliastitle":"Bad Girls +","subtitle":"李孝利 & CL - Bad Girls + 坏丫头 2013SBS歌谣大战 现场版 13/12/29","play_nums":"0","publishtime":"2013-12-30","del_status":"0","artist_id":"3571248","thumbnail":"http://qukufile2.qianqian.com/data2/pic/ab131c7a7c65a16efc1d19481f8b309f/269372615/269372615.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/11c1b815961b7ac9acedcc22fe175447/269372616/269372616.jpg","artist":"CL","provider":"11"},{"mv_id":"246383524","all_artist_id":"1999","title":"给我的快乐","aliastitle":"","subtitle":"胡夏 - 给我的快乐 电视剧神犬小七主题曲","play_nums":"6","publishtime":"2015-08-06","del_status":"0","artist_id":"1999","thumbnail":"http://qukufile2.qianqian.com/data2/pic/9702b112da49302673363d3bfa2a8ed9/269371967/269371967.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/60d2bcd76364739336caba0e042aee5c/269371968/269371968.jpg","artist":"胡夏","provider":"11"},{"mv_id":"239019447","all_artist_id":"20821946","title":"PRAY预告","aliastitle":"PRAY预告","subtitle":"FTISLAND - PRAY预告","play_nums":"0","publishtime":"2015-03-20","del_status":"0","artist_id":"20821946","thumbnail":"http://qukufile2.qianqian.com/data2/pic/399996c361962a45fe57d9d7871582ef/269371960/269371960.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/d17f9da58e08a0d3da9020680317f82b/269371961/269371961.jpg","artist":"ft island","provider":"11"},{"mv_id":"109014022","all_artist_id":"88","title":"有没有","aliastitle":"有没有","subtitle":"薛之谦 - 有没有","play_nums":"10022","publishtime":"2014-01-13","del_status":"0","artist_id":"88","thumbnail":"http://qukufile2.qianqian.com/data2/pic/6d2963ef4d7559bbee0185214ad5b5a6/269364711/269364711.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/d564b474ce4d81ff99ff29256bdb9ca7/269364712/269364712.jpg","artist":"薛之谦","provider":"11"},{"mv_id":"246840646","all_artist_id":"14357526","title":"因为红","aliastitle":"","subtitle":"泫雅 - 因为红 舞蹈练习","play_nums":"0","publishtime":"2015-08-28","del_status":"0","artist_id":"14357526","thumbnail":"http://qukufile2.qianqian.com/data2/pic/8f53a8dc43e11c3673af9745d3c3e6db/269353118/269353118.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/ddc7387dd27f84bb21c9ccbb0ac1afe5/269353119/269353119.jpg","artist":"泫雅","provider":"11"},{"mv_id":"116534792","all_artist_id":"18097268","title":"一起吃苦的幸福","aliastitle":"","subtitle":"EXO - 一起吃苦的幸福 - 不朽之名曲 现场版 2014/03/15","play_nums":"45318","publishtime":"2014-03-17","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/e449cf069138dd7daa0bd14aedba56c1/269363879/269363879.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/27454fc5cc6907772e2a44c592421623/269363880/269363880.jpg","artist":"EXO","provider":"11"},{"mv_id":"247598114","all_artist_id":"23330032","title":"心电感应","aliastitle":"","subtitle":"Simon D - 心电感应 蒙面歌王 现场版 15/09/20","play_nums":"0","publishtime":"2015-09-22","del_status":"0","artist_id":"23330032","thumbnail":"http://qukufile2.qianqian.com/data2/pic/8a233bd1d183ab9f19769ac15f73b9ed/269363874/269363874.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/1383c39cb48c315aa99f0b2351a623be/269363875/269363875.jpg","artist":"Simon D","provider":"11"},{"mv_id":"245950501","all_artist_id":"1786","title":"IF YOU","aliastitle":"","subtitle":"BIGBANG - IF YOU 新加坡演唱会 现场版","play_nums":"0","publishtime":"2015-07-27","del_status":"0","artist_id":"1786","thumbnail":"http://qukufile2.qianqian.com/data2/pic/3b67b5a56f4356663441a2527489d75d/269363867/269363867.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/105dbe1f1bf42f3392eda993bd60bc4e/269363868/269363868.jpg","artist":"BigBang","provider":"11"},{"mv_id":"118177477","all_artist_id":"18097268","title":"Alright Alright","aliastitle":"","subtitle":"EXO - Alright Alright  现场版 14/04/15","play_nums":"1","publishtime":"2014-04-17","del_status":"0","artist_id":"18097268","thumbnail":"http://qukufile2.qianqian.com/data2/pic/578abf02fd131b826c68ac6fa6107c47/269353664/269353664.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/5f8ec53a337e6d870f9c299094c99a78/269353665/269353665.jpg","artist":"EXO","provider":"11"},{"mv_id":"115100249","all_artist_id":"25815210,831","title":"呼吸","aliastitle":"呼吸","subtitle":"Chen & 张力尹 - 呼吸 中文字幕版","play_nums":"0","publishtime":"2014-02-27","del_status":"0","artist_id":"25815210","thumbnail":"http://qukufile2.qianqian.com/data2/pic/5173a61a67666e35e180387c210aed27/269316015/269316015.jpg","thumbnail2":"http://qukufile2.qianqian.com/data2/pic/b9d9185e2351253c93a1cca3b07d4a32/269316016/269316016.jpg","artist":"CHEN","provider":"11"}]
     */

    private ResultBean result;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private int total;
        private String havemore;
        /**
         * mv_id : 134667557
         * all_artist_id : 18097268
         * title : EXO MV20合1完整版合集
         * aliastitle : EXO MV20合1完整版合集
         * subtitle : EXO MV20合1完整版合集
         * play_nums : 4
         * publishtime : 2015-02-06
         * del_status : 0
         * artist_id : 18097268
         * thumbnail : http://qukufile2.qianqian.com/data2/pic/2f9210da185fd8f97ad0d9362bf28b18/269396578/269396578.jpg
         * thumbnail2 : http://qukufile2.qianqian.com/data2/pic/b87f6c40e9978722d26ae6c4233ce3ab/269396579/269396579.jpg
         * artist : EXO
         * provider : 11
         */

        private List<MvListBeen> mv_list;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getHavemore() {
            return havemore;
        }

        public void setHavemore(String havemore) {
            this.havemore = havemore;
        }

        public List<MvListBeen> getMv_list() {
            return mv_list;
        }

        public void setMv_list(List<MvListBeen> mv_list) {
            this.mv_list = mv_list;
        }

        public static class MvListBeen {
            private String mv_id;
            private String all_artist_id;
            private String title;
            private String aliastitle;
            private String subtitle;
            private String play_nums;
            private String publishtime;
            private String del_status;
            private String artist_id;
            private String thumbnail;
            private String thumbnail2;
            private String artist;
            private String provider;

            public String getMv_id() {
                return mv_id;
            }

            public void setMv_id(String mv_id) {
                this.mv_id = mv_id;
            }

            public String getAll_artist_id() {
                return all_artist_id;
            }

            public void setAll_artist_id(String all_artist_id) {
                this.all_artist_id = all_artist_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getAliastitle() {
                return aliastitle;
            }

            public void setAliastitle(String aliastitle) {
                this.aliastitle = aliastitle;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public String getPlay_nums() {
                return play_nums;
            }

            public void setPlay_nums(String play_nums) {
                this.play_nums = play_nums;
            }

            public String getPublishtime() {
                return publishtime;
            }

            public void setPublishtime(String publishtime) {
                this.publishtime = publishtime;
            }

            public String getDel_status() {
                return del_status;
            }

            public void setDel_status(String del_status) {
                this.del_status = del_status;
            }

            public String getArtist_id() {
                return artist_id;
            }

            public void setArtist_id(String artist_id) {
                this.artist_id = artist_id;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getThumbnail2() {
                return thumbnail2;
            }

            public void setThumbnail2(String thumbnail2) {
                this.thumbnail2 = thumbnail2;
            }

            public String getArtist() {
                return artist;
            }

            public void setArtist(String artist) {
                this.artist = artist;
            }

            public String getProvider() {
                return provider;
            }

            public void setProvider(String provider) {
                this.provider = provider;
            }
        }
    }
}
