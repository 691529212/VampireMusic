package vampire.com.baidumusic.fragment.search;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vampire.com.baidumusic.R;

/**
 * Created vampires by *Vampire* on 16/8/30.
 */
public class SearchAdapter extends BaseAdapter {
    private static final String TAG = "Vampire_SearchAdapter";
    private Context context;
    private List<SearchBean.ResultBean.SongInfoBean.SongListBean> album_list;
    private String xx;

    public void setXx(String xx) {
        this.xx = xx;
        notifyDataSetChanged();
    }

    public SearchAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    public void setAlbum_list(List<SearchBean.ResultBean.SongInfoBean.SongListBean> album_list) {
        this.album_list = album_list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return album_list != null ? album_list.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return album_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyViewHolder myViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.lv_item_songs_total, null);
            myViewHolder = new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        } else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }

        String author = album_list.get(position).getAuthor();
        String song = album_list.get(position).getTitle();
        Log.d(TAG, author + song);
        myViewHolder.author.setText(album_list.get(position).getAuthor());
        myViewHolder.song.setText(album_list.get(position).getTitle());

        SpannableStringBuilder authorBuilder = new SpannableStringBuilder(album_list.get(position).getAuthor());
        SpannableStringBuilder songBuilder = new SpannableStringBuilder(album_list.get(position).getTitle());
        int startIndex = album_list.get(position).getAuthor().indexOf(xx);
        int startIndexSong = album_list.get(position).getTitle().indexOf(xx);
        int xxLength = xx.length();
        if (startIndex >= 0) {
            authorBuilder.setSpan(new ForegroundColorSpan(0xff93d4ca), startIndex, startIndex + xxLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myViewHolder.author.setText(authorBuilder);
        }
        if (startIndexSong >= 0) {
            songBuilder.setSpan(new ForegroundColorSpan(0xff93d4ca), startIndexSong, startIndexSong + xxLength, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            myViewHolder.song.setText(songBuilder);
        }
        return convertView;
    }

    private class MyViewHolder {
        private TextView author;
        private TextView song;

        public MyViewHolder(View view) {
            author = (TextView) view.findViewById(R.id.tv_albums_name);
            song = (TextView) view.findViewById(R.id.tv_songs_name);

        }
    }
}
