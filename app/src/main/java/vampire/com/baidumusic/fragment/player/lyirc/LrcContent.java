package vampire.com.baidumusic.fragment.player.lyirc;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class LrcContent {
    private static final String TAG = "Vampire_LrcContent";

    private String lrcStr;  //歌词内容
    private int lrcTime;    //歌词当前时间
    public String getLrcStr() {
        return lrcStr;
    }
    public void setLrcStr(String lrcStr) {
        this.lrcStr = lrcStr;
    }
    public int getLrcTime() {
        return lrcTime;
    }
    public void setLrcTime(int lrcTime) {
        this.lrcTime = lrcTime;
    }
}
