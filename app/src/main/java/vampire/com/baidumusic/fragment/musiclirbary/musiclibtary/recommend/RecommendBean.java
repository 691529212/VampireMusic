package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.recommend;

import java.util.List;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class RecommendBean {
    private static final String TAG = "Vampire_RecommendBean";
    /**
     * pic : [{"randpic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_14712431468c09cec409af87ca2f3efcbdf76dcbdd.jpg","randpic_ios5":"","randpic_desc":"下一秒","randpic_ipad":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_147124315982f638b978811f43f4020e052b64cb9b.jpg","randpic_qq":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_147124316865320b037e8be250c451c1710b681c62.jpg","randpic_2":"bos_client_147124315457891a35dd3dfda36354eeffaa7a109b","randpic_iphone6":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_147124315457891a35dd3dfda36354eeffaa7a109b.jpg","special_type":0,"ipad_desc":"下一秒","is_publish":"111101","mo_type":"2","type":2,"code":"268925841"},{"randpic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470985085bc1ecba34c7dde87376a5e477ae97589.jpg","randpic_ios5":"","randpic_desc":"爱豆秀4","randpic_ipad":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470985093cb6843ba92aa5b84ae888bc544fd37db.jpg","randpic_qq":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470985098e802a92e7ef477552468023fd25521f5.jpg","randpic_2":"bos_client_1470985089a6d5de516dcd4806721eaa2e813f6197","randpic_iphone6":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470985089a6d5de516dcd4806721eaa2e813f6197.jpg","special_type":0,"ipad_desc":"爱豆秀4","is_publish":"111100","mo_type":"4","type":6,"code":"http://music.baidu.com/cms/webview/topic_activity/idolshow4/"},{"randpic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984040b6ee2988309d4ac7a6c65a1f12b6f3cd.jpg","randpic_ios5":"","randpic_desc":"独家MV：齐秦《爱在星空下》","randpic_ipad":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984049565101ad8379ad9d33346c9f34717973.jpg","randpic_qq":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984056702f797510502f0361d153ff7e789c13.jpg","randpic_2":"bos_client_1470984043361234da102bfe37a30b40af407704c2","randpic_iphone6":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984043361234da102bfe37a30b40af407704c2.jpg","special_type":0,"ipad_desc":"独家MV：齐秦《爱在星空下》","is_publish":"111101","mo_type":"4","type":6,"code":"http://music.baidu.com/cms/webview/bigwig/qiqinmv/"},{"randpic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470987219c4339e15635965a907a0f3d2f9647916.jpg","randpic_ios5":"","randpic_desc":"新歌榜32","randpic_ipad":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470987230a2b915d04d057b778f60ccce8f202a60.jpg","randpic_qq":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_14712288768e69576c5d26544492196a5dbbc9bfad.jpg","randpic_2":"bos_client_14709872229b51eaf5d2f23abc6dcde9fb61e1b84f","randpic_iphone6":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_14709872229b51eaf5d2f23abc6dcde9fb61e1b84f.jpg","special_type":0,"ipad_desc":"新歌榜32","is_publish":"111101","mo_type":"5","type":7,"code":"7061"},{"randpic":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984503f5ea5886fcde029e6301df6bee374475.jpg","randpic_ios5":"","randpic_desc":"Lost In The Stars","randpic_ipad":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984515d92513d300da9a515282be41fae3369a.jpg","randpic_qq":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984523d96c066413b5f3c82383cdbc8209007b.jpg","randpic_2":"bos_client_1470984507ef8962d82baf5dbc56cc73e62b349efe","randpic_iphone6":"http://business.cdn.qianqian.com/qianqian/pic/bos_client_1470984507ef8962d82baf5dbc56cc73e62b349efe.jpg","special_type":0,"ipad_desc":"Lost In The Stars","is_publish":"111101","mo_type":"2","type":2,"code":"268791363"}]
     * error_code : 22000
     */

    private int error_code;
    /**
     * randpic : http://business.cdn.qianqian.com/qianqian/pic/bos_client_14712431468c09cec409af87ca2f3efcbdf76dcbdd.jpg
     * randpic_ios5 :
     * randpic_desc : 下一秒
     * randpic_ipad : http://business.cdn.qianqian.com/qianqian/pic/bos_client_147124315982f638b978811f43f4020e052b64cb9b.jpg
     * randpic_qq : http://business.cdn.qianqian.com/qianqian/pic/bos_client_147124316865320b037e8be250c451c1710b681c62.jpg
     * randpic_2 : bos_client_147124315457891a35dd3dfda36354eeffaa7a109b
     * randpic_iphone6 : http://business.cdn.qianqian.com/qianqian/pic/bos_client_147124315457891a35dd3dfda36354eeffaa7a109b.jpg
     * special_type : 0
     * ipad_desc : 下一秒
     * is_publish : 111101
     * mo_type : 2
     * type : 2
     * code : 268925841
     */

    private List<PicBean> pic;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public List<PicBean> getPic() {
        return pic;
    }

    public void setPic(List<PicBean> pic) {
        this.pic = pic;
    }

    public static class PicBean {
        private String randpic;
        private String randpic_ios5;
        private String randpic_desc;
        private String randpic_ipad;
        private String randpic_qq;
        private String randpic_2;
        private String randpic_iphone6;
        private int special_type;
        private String ipad_desc;
        private String is_publish;
        private String mo_type;
        private int type;
        private String code;

        public String getRandpic() {
            return randpic;
        }

        public void setRandpic(String randpic) {
            this.randpic = randpic;
        }

        public String getRandpic_ios5() {
            return randpic_ios5;
        }

        public void setRandpic_ios5(String randpic_ios5) {
            this.randpic_ios5 = randpic_ios5;
        }

        public String getRandpic_desc() {
            return randpic_desc;
        }

        public void setRandpic_desc(String randpic_desc) {
            this.randpic_desc = randpic_desc;
        }

        public String getRandpic_ipad() {
            return randpic_ipad;
        }

        public void setRandpic_ipad(String randpic_ipad) {
            this.randpic_ipad = randpic_ipad;
        }

        public String getRandpic_qq() {
            return randpic_qq;
        }

        public void setRandpic_qq(String randpic_qq) {
            this.randpic_qq = randpic_qq;
        }

        public String getRandpic_2() {
            return randpic_2;
        }

        public void setRandpic_2(String randpic_2) {
            this.randpic_2 = randpic_2;
        }

        public String getRandpic_iphone6() {
            return randpic_iphone6;
        }

        public void setRandpic_iphone6(String randpic_iphone6) {
            this.randpic_iphone6 = randpic_iphone6;
        }

        public int getSpecial_type() {
            return special_type;
        }

        public void setSpecial_type(int special_type) {
            this.special_type = special_type;
        }

        public String getIpad_desc() {
            return ipad_desc;
        }

        public void setIpad_desc(String ipad_desc) {
            this.ipad_desc = ipad_desc;
        }

        public String getIs_publish() {
            return is_publish;
        }

        public void setIs_publish(String is_publish) {
            this.is_publish = is_publish;
        }

        public String getMo_type() {
            return mo_type;
        }

        public void setMo_type(String mo_type) {
            this.mo_type = mo_type;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
