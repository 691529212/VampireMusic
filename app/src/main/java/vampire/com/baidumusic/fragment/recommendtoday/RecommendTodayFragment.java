package vampire.com.baidumusic.fragment.recommendtoday;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class RecommendTodayFragment extends BaseFragment {
    private static final String TAG = "Vampire_RacommendTodatActivity";

    @Override
    protected int setLayout() {
        return R.layout.activity_recommend_today;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
