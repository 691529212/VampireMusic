package vampire.com.baidumusic.fragment.player.inside;

import android.graphics.Bitmap;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import vampire.com.baidumusic.R;

/**
 * Created vampires by *Vampire* on 16/9/8.
 */
public class MyImageListener implements ImageLoader.ImageListener{

    // 当图片请求下来是  设置给imageView;
    // 或是请求失败后 , 给该imageView添加错误图片;
    private ImageView imageView;

    public MyImageListener(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
        // 图片请求成功
        // 从response中获取图片(该方法会在发送网络请求的同时 被回调一次, 这时 bitmap为空)
        Bitmap bitmap =response.getBitmap();
        if (bitmap==null){
            imageView.setImageResource(R.mipmap.fourth);
        }else {
            // 如果bitmap有值  代表图片请求成功
            // 开始设置圆形
            CircleDrawable circleDrawable =new CircleDrawable(bitmap);
            imageView.setImageDrawable(circleDrawable);
            AlphaAnimation alphaAnimation =new AlphaAnimation(0,1);
            alphaAnimation.setDuration(3000);
            imageView.setAnimation(alphaAnimation);
            alphaAnimation.start();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        // 图片请求失败
        imageView.setImageResource(R.mipmap.fourth);
    }
}
