package vampire.com.baidumusic.fragment;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import cn.bmob.v3.BmobUser;
import vampire.com.baidumusic.aty.main.ActivityTemp;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.aty.main.MainAdapter;
import vampire.com.baidumusic.aty.main.signin.SignInActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.fragment.mine.MineFragment;
import vampire.com.baidumusic.fragment.musiclirbary.MusicLirbiryFragment;
import vampire.com.baidumusic.fragment.search.SearchFragment;
import vampire.com.baidumusic.fragment.showing.ShowingFragment;
import vampire.com.baidumusic.fragment.singsong.SingsongFragment;
import vampire.com.baidumusic.tools.event.RequestState;
import vampire.com.baidumusic.tools.event.SongIdEvent;


/**
 * Created BaiDuMusic by *Vampire* on 16/8/18.
 */
public class MainFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Vampire_MainFragment";
    private ImageView ruler;
    private ImageView user;


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getState(RequestState requestState) {
        if (requestState.isPlaying()) {
            user.startAnimation(ratate());
            ruler.startAnimation(rulerPlaying());
        } else {
            ruler.startAnimation(rulerNotPlaying());
            user.clearAnimation();
        }
    }

    // 播放状态
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setPlayerState(SongIdEvent playerState) {
        user.startAnimation(ratate());
        ruler.startAnimation(rulerPlaying());
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initView() {
        EventBus.getDefault().register(this);
        TabLayout tabLayout = bindView(R.id.tab_layout_main);
        ViewPager viewPager = bindView(R.id.view_pager_main);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new MineFragment());
        fragments.add(new MusicLirbiryFragment());
        fragments.add(new SingsongFragment());
        fragments.add(new ShowingFragment());

        MainAdapter adapter = new MainAdapter(getActivity().getSupportFragmentManager(), fragments);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(0xfff75519, 0xff9c9c9c);


        Button search = bindView(R.id.btn_search_main);

        user = bindView(R.id.btn_user_main);
        ruler = bindView(R.id.iv_ruler);
        ruler.startAnimation(rulerNotPlaying());

        BmobUser bmobUser = BmobUser.getCurrentUser();
        if (bmobUser != null) {
            // 用户名非空 , 且用户名不是默认账号
            Log.d(TAG, bmobUser.getUsername());
            // 四象封印
            if (bmobUser.getUsername() != null && !bmobUser.getUsername().equals("0")) {
                user.setImageResource(R.mipmap.fy);
                user.setClickable(false);
                ruler.setVisibility(View.VISIBLE);
            } else {
                user.setOnClickListener(this);
                ruler.setVisibility(View.GONE);
            }
        } else {
            user.setOnClickListener(this);
            ruler.setVisibility(View.GONE);
        }
        search.setOnClickListener(this);


    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search_main:
                MainActivity mainActivity = (MainActivity) getActivity();
                SearchFragment searchFragment = new SearchFragment();
                //mainActivity.updateView(searchFragment);
                startActivity(new Intent(getActivity(), ActivityTemp.class));
                break;
            case R.id.btn_user_main:
                // 登录界面
                Intent intent = new Intent(getActivity(), SignInActivity.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        BmobUser bmobUser = BmobUser.getCurrentUser();
        if (bmobUser != null) {
            // 用户名非空 , 且用户名不是默认账号
            Log.d(TAG, bmobUser.getUsername());
            // 四象封印
            if (bmobUser.getUsername() != null && !bmobUser.getUsername().equals("0")) {
                user.setImageResource(R.mipmap.fy);
                user.setClickable(false);
                ruler.setVisibility(View.VISIBLE);
            } else {
                user.setOnClickListener(this);
                ruler.setVisibility(View.GONE);
            }
        } else {
            user.setOnClickListener(this);
            ruler.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private RotateAnimation ratate() {
        RotateAnimation rotateAnimation = new RotateAnimation(720, -720, Animation.RELATIVE_TO_SELF, 0.5f
                , Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(4000);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        return rotateAnimation;
    }

    private RotateAnimation rulerNotPlaying() {
        RotateAnimation rotateAnimation = new RotateAnimation(0, -60, Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 0f);
        rotateAnimation.setDuration(1500);
        rotateAnimation.setFillAfter(true);
        return rotateAnimation;
    }

    private RotateAnimation rulerPlaying() {
        RotateAnimation rotateAnimation = new RotateAnimation(-45, -0, Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 0f);
        rotateAnimation.setDuration(1500);
        rotateAnimation.setFillAfter(true);
        return rotateAnimation;
    }


}
