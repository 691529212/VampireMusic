package vampire.com.baidumusic.fragment.singsong;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class SingsongFragment extends BaseFragment  {
    private static final String TAG = "Vampire_SingsongFragment";

    @Override
    protected int setLayout() {
        return R.layout.fragment_sing;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
