package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.ranking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/16.
 */
public class RankFragmentAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_RankFragmentAdapter";
    private Context  context;
    private ArrayList<RankFragmentBean.ContentBean> contentBeen;

    public RankFragmentAdapter(ArrayList<RankFragmentBean.ContentBean> contentBeen, Context context) {
        this.contentBeen = contentBeen;
        this.context = context;
        notifyDataSetChanged();
    }

    public void setContentBeen(ArrayList<RankFragmentBean.ContentBean> contentBeen) {
        this.contentBeen = contentBeen;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contentBeen!=null?contentBeen.size():0;
    }

    @Override
    public Object getItem(int position) {
        return contentBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =LayoutInflater.from(context);
        MyViewHolder myViewHolder=null;
        if (convertView == null){
            convertView=inflater.inflate(R.layout.lv_item_rank,null);
            myViewHolder=new MyViewHolder(convertView);
            convertView.setTag(myViewHolder);
        }else {
            myViewHolder = (MyViewHolder) convertView.getTag();
        }
        RankFragmentBean.ContentBean contentBean =contentBeen.get(position);

        ArrayList<RankFragmentBean.ContentBean.ContentBeans> contentBeans =
                (ArrayList<RankFragmentBean.ContentBean.ContentBeans>) contentBeen.get(position).getContent();
        NetTool netTool =new NetTool();
        netTool.getImage(contentBean.getPic_s210(),myViewHolder.pic);
        myViewHolder.rankName.setText(contentBean.getName());
        // 三甲颜色渐变
        int[] color ={0xffec0f1e,0xfff37a25,0xfff3e545};
        // 三甲数字变化
        String[] num ={"1","2","3"};

        myViewHolder.rank1.setTextColor(color[0]);
        myViewHolder.rank1.setText(num[0]);
        myViewHolder.firstName.setText(contentBeans.get(0).getAuthor());
        myViewHolder.firstMusic.setText(contentBeans.get(0).getTitle());

        myViewHolder.rank2.setText(num[1]);
        myViewHolder.rank2.setTextColor(color[1]);
        myViewHolder.secondName.setText(contentBeans.get(1).getAuthor());
        myViewHolder.secondMusic.setText(contentBeans.get(1).getTitle());

        myViewHolder.rank3.setText(num[2]);
        myViewHolder.rank3.setTextColor(color[2]);
        myViewHolder.thridName.setText(contentBeans.get(2).getAuthor());
        myViewHolder.thridMusic.setText(contentBeans.get(2).getTitle());


        return convertView;
    }
    private class MyViewHolder{
        private ImageView pic;
        private TextView rankName;
        private TextView firstName;
        private TextView firstMusic;
        private TextView rank1;
        private TextView secondName;
        private TextView secondMusic;
        private TextView rank2;
        private TextView thridName;
        private TextView thridMusic;
        private TextView rank3;

        private MyViewHolder(View view){
            pic= (ImageView) view.findViewById(R.id.iv_rank_item);
            rankName = (TextView) view.findViewById(R.id.tv_rank_name);

            firstName = (TextView) view.findViewById(R.id.tv_rank_author_first);
            firstMusic = (TextView) view.findViewById(R.id.tv_rank_title_first);
            rank1  = (TextView) view.findViewById(R.id.tv_in_lv_rank1);

            secondName = (TextView) view.findViewById(R.id.tv_rank_title_2);
            secondMusic = (TextView) view.findViewById(R.id.tv_rank_author_2);
            rank2 = (TextView) view.findViewById(R.id.tv_in_lv_rank2);

            thridName = (TextView) view.findViewById(R.id.tv_rank_author_3);
            thridMusic = (TextView) view.findViewById(R.id.tv_rank_title_3);
            rank3 = (TextView) view.findViewById(R.id.tv_in_lv_rank3);

        }

    }


}
