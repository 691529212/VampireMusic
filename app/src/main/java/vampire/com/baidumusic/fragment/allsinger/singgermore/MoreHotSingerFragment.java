package vampire.com.baidumusic.fragment.allsinger.singgermore;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.AllSingerBean;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/18.
 */
public class MoreHotSingerFragment extends BaseFragment {
    private static final String TAG = "Vampire_MoreHotSinger";
    private HotSingerAdapter hotSingerAdapter;
    private PullToRefreshGridView grapeGridView;
    private List<AllSingerBean.ArtistBean> artistBeen;
    private FragmentTransaction transaction;
    private Bundle bundle;
    private GridView mGridView;
    private int a;
    private int b;


    @Override
    protected int setLayout() {
        return R.layout.activity_singer_more;
    }

    @Override
    protected void initView() {
        FragmentManager manager = getFragmentManager();
        transaction = manager.beginTransaction();

        grapeGridView = bindView(R.id.ggv_singer_hot);
        hotSingerAdapter = new HotSingerAdapter(getContext());
        grapeGridView.setAdapter(hotSingerAdapter);
        mGridView = grapeGridView.getRefreshableView();
        bundle = getArguments();
        a = bundle.getInt(KeyValues.MORE_HOT_SINGER_A); // 加载数量
        b = bundle.getInt(KeyValues.MORE_HOT_SINGER_B); // 初始加载位置

        grapeGridView.setMode(PullToRefreshBase.Mode.BOTH);

        grapeGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {

            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
                int urlA = bundle.getInt(KeyValues.MORE_HOT_SINGER_A);
                int urlB = bundle.getInt(KeyValues.MORE_HOT_SINGER_B);
                mNetTool.getNetData(URLVlaues.HOT_SINGER_A + urlA + URLVlaues.HOT_SINGER_BODAY + urlB + URLVlaues.HOT_SINGER_B, AllSingerBean.class, new NetTool.NetListener<AllSingerBean>() {
                    @Override
                    public void onSuccess(AllSingerBean allSingerBean) {
                        artistBeen = allSingerBean.getArtist();
                        hotSingerAdapter.setArtistBeen(artistBeen);
                        grapeGridView.onRefreshComplete();
                    }
                    @Override
                    public void onError(String errorMsg) {

                    }
                });
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                b += 30;
                mNetTool.getNetData(URLVlaues.HOT_SINGER_A + a + URLVlaues.HOT_SINGER_BODAY + b + URLVlaues.HOT_SINGER_B, AllSingerBean.class, new NetTool.NetListener<AllSingerBean>() {
                    @Override
                    public void onSuccess(AllSingerBean allSingerBean) {

                        List<AllSingerBean.ArtistBean> list = allSingerBean.getArtist();
                        artistBeen.addAll(list);
                        hotSingerAdapter.addArtistBeen(list);
                        grapeGridView.onRefreshComplete();
                    }

                    @Override
                    public void onError(String errorMsg) {

                    }
                });

            }
        });


    }

    @Override
    protected void initData() {
        bundle = getArguments();
        int urlA = bundle.getInt(KeyValues.MORE_HOT_SINGER_A);
        int urlB = bundle.getInt(KeyValues.MORE_HOT_SINGER_B);

        mNetTool.getNetData(URLVlaues.HOT_SINGER_A + urlA + URLVlaues.HOT_SINGER_BODAY + urlB + URLVlaues.HOT_SINGER_B, AllSingerBean.class, new NetTool.NetListener<AllSingerBean>() {

            @Override
            public void onSuccess(AllSingerBean allSingerBean) {
                artistBeen = allSingerBean.getArtist();
                hotSingerAdapter.setArtistBeen(artistBeen);
            }

            @Override
            public void onError(String errorMsg) {

            }

        });

        grapeGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailFragment detailFragment = new DetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(KeyValues.RANK_TYPE, artistBeen.get(position).getTing_uid());

                bundle.putString(KeyValues.RANK_ARTIST_ID, artistBeen.get(position).getArtist_id());

                bundle.putString(KeyValues.RANK_NAME, artistBeen.get(position).getName());
                bundle.putString("songs_total", artistBeen.get(position).getSongs_total());
                bundle.putString("albums_total", artistBeen.get(position).getAlbums_total());
                bundle.putString(KeyValues.RANK_PIC, artistBeen.get(position).getAvatar_big());
                bundle.putInt(KeyValues.MY_TYPE_ID, 1);

                detailFragment.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(detailFragment);
            }
        });

    }
}
