package vampire.com.baidumusic.fragment.search;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.FormatCodeUtil;
import vampire.com.baidumusic.tools.event.SongIdEvent;
import vampire.com.baidumusic.tools.event.SongIdEventBean;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created vampires by *Vampire* on 16/8/30.
 */
public class SearchFragment extends BaseFragment {
    private static final String TAG = "Vampire_SearchFragment";
    private EditText editText;
    private SearchAdapter adapter;
    private List<SearchBean.ResultBean.AlbumInfoBean.AlbumListBean> album_list;
    private List<String> songName;
    private List<String> songIds;
    private List<String> author;
    private List<SearchBean.ResultBean.SongInfoBean.SongListBean> song_list;
    private List<String> pic;

    @Override
    protected int setLayout() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initView() {
        editText = bindView(R.id.et_for_search);
        editText.setFocusable(true);
        Button btnSearch = bindView(R.id.btn_search_search);

        final ListView listView = bindView(R.id.lv_search);
        adapter = new SearchAdapter(getContext());
        listView.setAdapter(adapter);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                song_list = new ArrayList<SearchBean.ResultBean.SongInfoBean.SongListBean>();
                album_list = new ArrayList<SearchBean.ResultBean.AlbumInfoBean.AlbumListBean>();
                String url = URLVlaues.SEARCH_A + FormatCodeUtil.codingFormat(editText.getText().toString()) + URLVlaues.SEARCH_B;
                Log.d(TAG, url);
                mNetTool.getNetData(url,
                        SearchBean.class, new NetTool.NetListener<SearchBean>() {
                            @Override
                            public void onSuccess(SearchBean searchBean) {

                                if (searchBean.getResult().getAlbum_info() != null) {
                                    album_list = searchBean.getResult().getAlbum_info().getAlbum_list();
                                    song_list = searchBean.getResult().getSong_info().getSong_list();
                                    adapter.setAlbum_list(song_list);
                                    adapter.setXx(editText.getText().toString());
                                }else {
                                    Toast.makeText(getContext(), "未找到内容", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(String errorMsg) {

                            }
                        });
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                author = new ArrayList<String>();
                songIds = new ArrayList<String>();
                songName = new ArrayList<String>();
                pic = new ArrayList<String>();

                for (SearchBean.ResultBean.SongInfoBean.SongListBean songListBean : song_list) {
                    author.add(songListBean.getAuthor());
                    songIds.add(songListBean.getSong_id());
                    songName.add(songListBean.getTitle());
                }
                for (SearchBean.ResultBean.AlbumInfoBean.AlbumListBean albumListBean : album_list) {
                    pic.add(albumListBean.getPic_small());
                }
                SongIdEventBean songIdEventBean = new SongIdEventBean(author, pic, position, songIds, songName);
                SongIdEvent songIdEvent = new SongIdEvent();
                songIdEvent.setSongIdEventBean(songIdEventBean);
                EventBus.getDefault().post(songIdEvent);
            }
        });

    }

    @Override
    protected void initData() {

    }
}
