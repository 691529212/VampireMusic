package vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.musiclist;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.fragment.allsinger.detailcolumii.DetailFragment;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MusicListFragment extends BaseFragment {
    private static final String TAG = "Vampire_MusicListFragment";
    private PullToRefreshGridView pullToRefreshGridView ;
    private GridView gridView;
    private MusicListGridAdapter musicListGridAdapter;
    private List<MusicListBean.ContentBean> list;

    @Override
    protected int setLayout() {
        return R.layout.fragment_music_libiry_fragment_list_fragment;
    }

    @Override
    protected void initView() {
        pullToRefreshGridView=bindView(R.id.ggv_music_list);
        musicListGridAdapter = new MusicListGridAdapter(getContext());
        pullToRefreshGridView.setAdapter(musicListGridAdapter);

        gridView =pullToRefreshGridView.getRefreshableView();
        pullToRefreshGridView.setMode(PullToRefreshBase.Mode.BOTH);
        pullToRefreshGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {
            int a = 1;

            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) { // 上啦刷新

                mNetTool.getNetData(URLVlaues.ALL_SONGLIST, MusicListBean.class, new NetTool.NetListener<MusicListBean>() {
                    @Override
                    public void onSuccess(MusicListBean musicListBean) {
                        list=  musicListBean.getContent();
                        musicListGridAdapter.setContentBeen(list);
                        pullToRefreshGridView.onRefreshComplete();
                    }

                    @Override
                    public void onError(String errorMsg) {

                    }
                });

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {

                a++;
                mNetTool.getNetData(URLVlaues.ALL_SONGLIST_A+ a+URLVlaues.ALL_SONGLIST_B, MusicListBean.class, new NetTool.NetListener<MusicListBean>() {
                    @Override
                    public void onSuccess(MusicListBean musicListBean) {

                        List<MusicListBean.ContentBean> contentBeanList=  musicListBean.getContent();
                        musicListGridAdapter.addContentBeen(contentBeanList);
                        list.addAll(contentBeanList);
                        pullToRefreshGridView.onRefreshComplete();
                    }
                    @Override
                    public void onError(String errorMsg) {

                    }
                });
            }
        });



    }

    @Override
    protected void initData() {
        mNetTool.getNetData(URLVlaues.ALL_SONGLIST, MusicListBean.class, new NetTool.NetListener<MusicListBean>() {
            @Override
            public void onSuccess(MusicListBean musicListBean) {
                list = musicListBean.getContent();
                musicListGridAdapter.setContentBeen(list);

            }

            @Override
            public void onError(String errorMsg) {

            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle =new Bundle();
                MusicListBean.ContentBean listBean =list.get(position);
                bundle.putString(KeyValues.RANK_PIC,listBean.getPic_w300());
                bundle.putString(KeyValues.RANK_TYPE,listBean.getListid());
                bundle.putString(KeyValues.RANK_NAME,listBean.getTitle());
                bundle.putInt(KeyValues.MY_TYPE_ID,2);
                DetailFragment fragment=new DetailFragment();
                fragment.setArguments(bundle);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.updateView(fragment);
            }
        });

    }
}
