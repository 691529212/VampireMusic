package vampire.com.baidumusic.fragment.myfavorite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.tools.dbtools.DBHistoryPlay;
import vampire.com.baidumusic.tools.dbtools.DBMyMusicList;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/27.
 */
public class MyFavoriteAdapter extends BaseAdapter{
    private static final String TAG = "Vampire_MyFavoriteAdapter";
    private ArrayList<DBMyMusicList> dbMyMusicLists ;
    private Context context;

    public MyFavoriteAdapter(Context context) {
        this.context = context;
        notifyDataSetChanged();
    }

    public void setDbMyMusicLists(ArrayList<DBMyMusicList> dbMyMusicLists) {
        this.dbMyMusicLists = dbMyMusicLists;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return dbMyMusicLists!=null?dbMyMusicLists.size():0;
    }

    @Override
    public Object getItem(int position) {
        return dbMyMusicLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHoklder  myViewHoklder;
        if (convertView == null){
         convertView = LayoutInflater.from(context).inflate(R.layout.lv_in_my_favorite,null);
            myViewHoklder =new MyViewHoklder(convertView);
            convertView.setTag(myViewHoklder);
        }else {
            myViewHoklder = (MyViewHoklder) convertView.getTag();
        }
        myViewHoklder.author.setText(dbMyMusicLists.get(position).getAuthor());
        myViewHoklder.song.setText(dbMyMusicLists.get(position).getSongName());

        return convertView;
    }

    private class MyViewHoklder {
        private TextView author;
        private TextView song;
        private ImageView deital;
        public MyViewHoklder(View view) {
            author = (TextView) view.findViewById(R.id.tv_my_favorite_author);
            song = (TextView) view.findViewById(R.id.tv_my_favorite_song);
            deital = (ImageView) view.findViewById(R.id.iv_my_favorite_detail);
        }
    }
}
