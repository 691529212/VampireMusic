package vampire.com.baidumusic.fragment.musiclirbary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.musiclist.MusicListFragment;
import vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.mv.MVFragment;
import vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.radio.RadioFragment;
import vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.ranking.RankFragment;
import vampire.com.baidumusic.fragment.musiclirbary.musiclibtary.recommend.RecommendFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MusicLirbiryFragment extends BaseFragment {
    private static final String TAG = "Vampire_musicLirbiryFragment";
    private ViewPager viewPager;
    private MusicReceiver musicReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        musicReceiver = new MusicReceiver();
        IntentFilter filter =new IntentFilter();
        filter.addAction("vampire.com.baidumusic.fragment.musicLirbaryFragment.fragmenstInMusicLibtary.recommendFragment.MoveToList");
        getContext().registerReceiver(musicReceiver,filter);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(musicReceiver);
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_music_libiry_fragment;
    }

    @Override
    protected void initView() {
        TabLayout tabLayout =bindView(R.id.tablayout_music_library_fragment);
        viewPager = bindView(R.id.view_pager_music_library_fragment);
        ArrayList<Fragment> fragments =new ArrayList<>();
        fragments.add(new RecommendFragment()); // 推荐
        fragments.add(new RankFragment());// 排行
        fragments.add(new MusicListFragment()); // 歌单
        fragments.add(new RadioFragment());// 电台
        fragments.add(new MVFragment());// MV

        MusicLibraryAdapter adapter = new MusicLibraryAdapter(getChildFragmentManager(),fragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(0xff2a2a2a,0xff029af7);

    }

    @Override
    protected void initData() {

    }
    public class MusicReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action =intent.getAction();
            switch (action){
                case "vampire.com.baidumusic.fragment.musicLirbaryFragment.fragmenstInMusicLibtary.recommendFragment.MoveToList":
                    viewPager.setCurrentItem(2);
                    break;
            }
        }
    }

}
