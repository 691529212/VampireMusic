package vampire.com.baidumusic.fragment.player.lyirc;

import android.os.Bundle;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.event.ProgressEvent;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class LyricFragment extends BaseFragment {
    private static final String TAG = "Vampire_LyircFragment";
    private LrcProgress lrcProgress;
    private LrcView lrcView;
    private String url;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        url = "";
    }

    // 获取歌词
    @Subscribe
    public void getLyric(ProgressEvent progressEvent){
        // 由于一直传递信息 故仅当歌词地址改变是时才改变
        if (!progressEvent.getProgressBean().getLrc().equals(url)) {
            url =progressEvent.getProgressBean().getLrc();
            lrcView.setLrcPath(progressEvent.getProgressBean().getLrc());
        }
        if (lrcView.hasLrc()) lrcView.changeCurrent(progressEvent.getProgressBean().getTime());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_lyirc;
    }

    @Override
    protected void initView() {
        lrcView = bindView(R.id.lyirc_LrcView);
        lrcProgress = new LrcProgress();
    }

    @Override
    protected void initData() {

    }
}
