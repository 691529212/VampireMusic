package vampire.com.baidumusic.fragment.allsinger.detailcolumii;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import vampire.com.baidumusic.Interface.OnClickDetailItem;
import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.fragment.allsinger.singer.songs.SongsTitalBean;
import vampire.com.baidumusic.aty.main.MainActivity;
import vampire.com.baidumusic.tools.dbtools.DBMyFavoriteList;
import vampire.com.baidumusic.tools.dbtools.DBTool;
import vampire.com.baidumusic.tools.event.SongIdEvent;
import vampire.com.baidumusic.tools.event.SongIdEventBean;
import vampire.com.baidumusic.tools.nettools.NetTool;
import vampire.com.baidumusic.values.KeyValues;
import vampire.com.baidumusic.values.URLVlaues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/17.
 */
public class DetailFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "Vampire_DetailActivity";
    private Bundle bundle;
    private ArrayList<String> strings;
    private ImageView imageView;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private LinearLayoutManager manager;
    private RecyclerView recyclerView;
    private DetailRVadapter adapter;
    private FloatingActionButton floatingActionButton;
    private List<RankDeatilBean.SongListBean> listBeen;// 排行
    private List<SongsTitalBean.SonglistBean> songlist;// 歌手
    private List<SongListDeatilBean.ContentBean> contentBeen;// 歌单
    private int myTypeId;
    private List<String> list;
    private String songsId;
    private List<String> song;
    private List<String> author;
    private String song1;
    private String author1;
    private TabLayout tabLayout;
    private String pic;
    private String url;
    private boolean isFavorite;
    private ImageView favorite;


    @Override
    protected int setLayout() {
        return R.layout.activity_singer_detail;
    }


    @Override
    protected void initView() {

        imageView = bindView(R.id.iv_detail);
        collapsingToolbarLayout = bindView(R.id.toolbar_layout);
        collapsingToolbarLayout.setExpandedTitleColor(0xc6c6c6c6);
        recyclerView = bindView(R.id.rv_singer_detail);
        manager = new LinearLayoutManager(getContext());

        Button playAll = bindView(R.id.btn_detail_play_all);
        playAll.setOnClickListener(this);

        floatingActionButton = bindView(R.id.fab);
        floatingActionButton.setOnClickListener(this);

        bundle = getArguments();

        myTypeId = bundle.getInt(KeyValues.MY_TYPE_ID);
        tabLayout = bindView(R.id.tab_singer_detail);
        pic = bundle.getString(KeyValues.RANK_PIC);
        url = bundle.getString(KeyValues.RANK_TYPE);
        mNetTool.getImage(pic, imageView); // 背景图
        collapsingToolbarLayout.setTitle(bundle.getString(KeyValues.RANK_NAME)); // 标题文字


        switch (myTypeId) {
            case 1:// 歌手
                recyclerView.setLayoutManager(manager);
                adapter = new DetailRVadapter(getContext(), bundle.getInt(KeyValues.MY_TYPE_ID));
                recyclerView.setAdapter(adapter);
                floatingActionButton.setVisibility(View.INVISIBLE);
                strings = new ArrayList<>();
                strings.add("歌曲(" + bundle.getString("songs_total") + ")");
                strings.add("专辑（" + bundle.getString("albums_total") + "）");
                tabLayout.setTabTextColors(0xffd6d6d6, 0xff02bbf0);
                TabLayout.Tab tab1 = tabLayout.newTab();
                TabLayout.Tab tab2 = tabLayout.newTab();
                tab1.setText(strings.get(0));
                tab2.setText(strings.get(1));
                tabLayout.addTab(tab1);
                tabLayout.addTab(tab2);
                tab1.isSelected();//设置默认选中
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        int position = tab.getPosition();
                        switch (position) {
                            case 0:
                                recyclerView.setLayoutManager(manager);
                                adapter = new DetailRVadapter(getContext(), bundle.getInt(KeyValues.MY_TYPE_ID));
                                recyclerView.setAdapter(adapter);
                                singerDate();
                                break;
                            case 1:
                                // 专辑列表  敬请期待
                                break;
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

                singerDate();

                break;
            case 2:// 歌单
                tabLayout.setVisibility(View.GONE);

                adapter = new DetailRVadapter(getContext(), myTypeId);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(manager);
                mNetTool.getNetData(URLVlaues.SONGLIST_DETAIL_Front + url + URLVlaues.SONGLIST_DETAIL_BEHIND,
                        SongListDeatilBean.class, new NetTool.NetListener<SongListDeatilBean>() {
                            @Override
                            public void onSuccess(SongListDeatilBean songListDeatilBean) {
                                contentBeen = songListDeatilBean.getContent();// 歌单集合
                                adapter.setContentBeen(contentBeen);
                                clickItem();
                            }

                            @Override
                            public void onError(String errorMsg) {

                            }
                        });

                break;
            case 3:// 排行

                tabLayout.setVisibility(View.GONE);
                adapter = new DetailRVadapter(getContext(), myTypeId);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(manager);

                mNetTool.getNetData(URLVlaues.TOP_SONG_FRONT + url + URLVlaues.TOP_SONG_BEHIND,
                        RankDeatilBean.class, new NetTool.NetListener<RankDeatilBean>() {
                            @Override
                            public void onSuccess(RankDeatilBean rankDeatilBean) {
                                listBeen = rankDeatilBean.getSong_list(); // 排行榜集合
                                adapter.setListBeen(listBeen);
                                clickItem();

                            }

                            @Override
                            public void onError(String errorMsg) {

                            }
                        });
        }

    }

    @Override
    protected void initData() {
        favorite = bindView(R.id.btn_favorite);

        DBTool.getInstance().queryMyFavoriteList(collapsingToolbarLayout.getTitle().toString(), new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {
                Log.d(TAG, "list.size():" + list.size());
                if (list.size() == 0) {
                    Log.d(TAG, "未收藏");
                    favorite.setImageResource(R.mipmap.like);
                    isFavorite = false;
                } else {
                    Log.d(TAG, "已收藏");
                    favorite.setImageResource(R.mipmap.like_checked);
                    isFavorite = true;
                }
            }
        });

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFavorite) {
                    DBMyFavoriteList dbMyFavoriteList = new DBMyFavoriteList(collapsingToolbarLayout.getTitle().toString(),
                            pic, url, myTypeId);
                    DBTool.getInstance().insertMyFavoriteList(dbMyFavoriteList);
                    favorite.setImageResource(R.mipmap.like_checked);
                    isFavorite = true;
                } else {
                    DBTool.getInstance().delMyFavoriteList(url);
                    favorite.setImageResource(R.mipmap.like);
                    isFavorite = false;
                }
            }
        });
    }

    private void clickItem() {
        adapter.setOnClickDetailItem(new OnClickDetailItem() {

            @Override
            public void OnClickDetailItem(final int position, final String songId) {
                switch (myTypeId) {
                    case 1:
                        list = new ArrayList();
                        song = new ArrayList();
                        author = new ArrayList();
                        for (int i = 0; i < songlist.size(); i++) {
                            songsId = songlist.get(i).getSong_id();
                            song1 = songlist.get(i).getTitle();
                            author1 = songlist.get(i).getAuthor();
                            list.add(songsId);
                            song.add(song1);
                            author.add(author1);
                        }
                        break;

                    case 2:
                        list = new ArrayList();
                        song = new ArrayList();
                        author = new ArrayList();
                        for (int i = 0; i < contentBeen.size(); i++) {
                            songsId = contentBeen.get(i).getSong_id();
                            song1 = contentBeen.get(i).getTitle();
                            author1 = contentBeen.get(i).getAuthor();
                            list.add(songsId);
                            song.add(song1);
                            author.add(author1);
                        }
                        break;

                    case 3:
                        list = new ArrayList();
                        song = new ArrayList();
                        author = new ArrayList();
                        for (int i = 0; i < listBeen.size(); i++) {
                            songsId = listBeen.get(i).getSong_id();
                            song1 = listBeen.get(i).getTitle();
                            author1 = listBeen.get(i).getAuthor();
                            list.add(songsId);
                            song.add(song1);
                            author.add(author1);
                        }
                        break;
                }

                // 传值
                SongIdEventBean songIdEventBean = new SongIdEventBean(author, position, list, song);
                SongIdEvent songIdEvent = new SongIdEvent();
                songIdEvent.setSongIdEventBean(songIdEventBean);
                EventBus.getDefault().post(songIdEvent);
            }
        });
    }

    private void singerDate() {
        Log.d(TAG, URLVlaues.SINGER_A + url + URLVlaues.SINGER_B + bundle.getString(KeyValues.RANK_ARTIST_ID) + URLVlaues.SINGER_C);

        mNetTool.getNetData(URLVlaues.SINGER_A + url + URLVlaues.SINGER_B + bundle.getString(KeyValues.RANK_ARTIST_ID) + URLVlaues.SINGER_C,
                SongsTitalBean.class,
                new NetTool.NetListener<SongsTitalBean>() {
                    @Override
                    public void onSuccess(SongsTitalBean songsTitalBean) {
                        songlist = new ArrayList<SongsTitalBean.SonglistBean>();
                        songlist = songsTitalBean.getSonglist();
                        adapter.setSonglistBeen(songlist);
                        clickItem();

                    }

                    @Override
                    public void onError(String errorMsg) {

                    }
                });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fab:
                //功能一致 向下击穿
            case R.id.btn_detail_play_all:
                switch (myTypeId) {
                    case 1:
                        list = new ArrayList();
                        song = new ArrayList();
                        author = new ArrayList();
                        for (int i = 0; i < songlist.size(); i++) {
                            songsId = songlist.get(i).getSong_id();
                            song1 = songlist.get(i).getTitle();
                            author1 = songlist.get(i).getAuthor();
                            list.add(songsId);
                            song.add(song1);
                            author.add(author1);
                        }
                        break;
                    case 2:
                        list = new ArrayList();
                        song = new ArrayList();
                        author = new ArrayList();
                        for (int i = 0; i < contentBeen.size(); i++) {
                            songsId = contentBeen.get(i).getSong_id();
                            song1 = contentBeen.get(i).getTitle();
                            author1 = contentBeen.get(i).getAuthor();
                            list.add(songsId);
                            song.add(song1);
                            author.add(author1);
                        }
                        break;
                    case 3:
                        list = new ArrayList();
                        song = new ArrayList();
                        author = new ArrayList();
                        for (int i = 0; i < listBeen.size(); i++) {
                            songsId = listBeen.get(i).getSong_id();
                            song1 = listBeen.get(i).getTitle();
                            author1 = listBeen.get(i).getAuthor();
                            list.add(songsId);
                            song.add(song1);
                            author.add(author1);
                        }
                        break;
                }

                // 传值
                SongIdEventBean songIdEventBean = new SongIdEventBean(author, 0, list, song);
                SongIdEvent songIdEvent = new SongIdEvent();
                songIdEvent.setSongIdEventBean(songIdEventBean);
                EventBus.getDefault().post(songIdEvent);
                break;


        }
    }
}
