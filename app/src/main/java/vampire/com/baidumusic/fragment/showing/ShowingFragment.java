package vampire.com.baidumusic.fragment.showing;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class ShowingFragment extends BaseFragment {
    private static final String TAG = "Vampire_ShowingFragment";

    @Override
    protected int setLayout() {
        return R.layout.fragment_show;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {

    }
}
