package vampire.com.baidumusic.fragment.player.inside;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.BaseFragment;
import vampire.com.baidumusic.tools.dbtools.DBMusicList;
import vampire.com.baidumusic.tools.dbtools.DBMyMusicList;
import vampire.com.baidumusic.tools.dbtools.DBTool;
import vampire.com.baidumusic.tools.event.BottomInformationEvent;
import vampire.com.baidumusic.tools.event.IsSelected;
import vampire.com.baidumusic.tools.event.NameSongIdBean;
import vampire.com.baidumusic.tools.event.PlayerBeanEvent;
import vampire.com.baidumusic.tools.event.ProgressEvent;
import vampire.com.baidumusic.tools.event.SaveSong;
import vampire.com.baidumusic.values.KeyValues;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/22.
 */
public class PICFragment extends BaseFragment {
    private static final String TAG = "Vampire_PICFragment";
    private ImageView imageView;
    private String killer = new String();
    private ImageView checkBox;
    private NameSongIdBean nameSongIdBean;
    private boolean selected;
    private String musicId;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getPlayerBean(ProgressEvent progressEvent){
        String url =progressEvent.getProgressBean().getPic();
        if (!url.equals(killer)) {
            killer = progressEvent.getProgressBean().getPic();
            mNetTool.getImage(progressEvent.getProgressBean().getPic(), imageView);
            nameSongIdBean = progressEvent.getNameSongIdBean();

        }
        musicId = progressEvent.getNameSongIdBean().getSongId();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void isFavourt(IsSelected isSelected){
        selected = isSelected.isSelected();
        Log.d(TAG, "get");
        if (isSelected.isSelected()) {
            checkBox.setImageResource(R.mipmap.like_checked);
        }else {
            checkBox.setImageResource(R.mipmap.like);
        }

    }

    public ImageView getCheckBox() {
        return checkBox;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_pic;
    }

    @Override
    protected void initView() {

        DBTool.getInstance().queryDBMyMusicList(musicId, new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {
                Log.d(TAG, "go");
                if (list.size() == 0) {
                    checkBox.setImageResource(R.mipmap.like);
                    selected = false;
                } else {
                    checkBox.setImageResource(R.mipmap.like_checked);
                    selected = true;
                }
            }
        });

        imageView = bindView(R.id.iv_pic);
        Bundle bundle =getArguments();
        mNetTool.getImage(bundle.getString(KeyValues.PIC_HUGE),imageView);

        ImageView download = bindView(R.id.iv_downLoad);
        checkBox = bindView(R.id.cbx_like);
        ImageView detail =bindView(R.id.iv_detail);

        DBTool.getInstance().querDBMusicListByBoolean(new DBTool.QueryListener() {
            @Override
            public <T> void onQueryListener(List<T> list) {
                if (list.size()>0) {
                    List<DBMusicList> musicLists = (List<DBMusicList>) list;
                    mNetTool.getImage(musicLists.get(0).getPic(), imageView);
                }
            }
        });

    }

    @Override
    protected void initData() {

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "selected:" + selected);
                if (selected){
                    // 删
                    DBTool.getInstance().delDBMyMusicListBySongId(nameSongIdBean.getSongId());
                    checkBox.setImageResource(R.mipmap.like);
                    selected = false;
                }else {
                    // 增
                    DBMyMusicList dbMyMusicList =new DBMyMusicList(nameSongIdBean.getName(),
                            nameSongIdBean.getSongId(),nameSongIdBean.getSongname());
                    DBTool.getInstance().instertMyDBMusicList(dbMyMusicList);
                    checkBox.setImageResource(R.mipmap.like_checked);
                    selected = true;

                }
            }
        });
    }
}
