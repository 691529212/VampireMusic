package vampire.com.baidumusic.base;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public abstract class BaseFragment extends Fragment {
    private static final String TAG = "Vampire_BaseFragment";
    protected NetTool mNetTool;

    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mNetTool = new NetTool(this);
        return inflater.inflate(setLayout(),container,false);
    }

    protected abstract @LayoutRes int setLayout();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    protected abstract void initView();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    protected  abstract void  initData();

    protected <T extends View>T bindView (int id){

        return (T)getView().findViewById(id);
    }

    protected <T extends View>T bindView (View view, int id){
        return (T)view.findViewById(id);
    }

    @Override
    public void onDestroy() {
        mNetTool.onDestroy();//取消未完成的网络请求
        super.onDestroy();
    }
}
