package vampire.com.baidumusic.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import vampire.com.baidumusic.tools.nettools.NetTool;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public abstract class BaseActivity extends AppCompatActivity{
    private static final String TAG = "Vampire_BaseActivity";
    protected NetTool mNetTool;//网络请求的工具类
    protected View rootView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNetTool = new NetTool(this);//设置了tag是自己,当Activity被销毁的时候,可以通过这个tag来取消此Activity的所
        if (setLayout() != 0) {
            rootView = LayoutInflater.from(this).inflate(setLayout(),null);
            setContentView(rootView);//绑定布局
        } else {
            //没有绑定布局,弹出错误日志
            Log.e("BaseAty","Activity:"+this.getClass().getSimpleName()+" 没有绑定布局");
        }
        initView();
        initData();
    }

    protected abstract int setLayout();
    protected abstract void initView();
    protected abstract void initData();

    protected <T extends View> T bindView(int id){
        return (T)findViewById(id);
    }

    //在Activity销毁的时候,把未完成的网络请求取消了
    @Override
    protected void onDestroy() {
        mNetTool.onDestroy();
        mNetTool = null;
        super.onDestroy();
    }
}
