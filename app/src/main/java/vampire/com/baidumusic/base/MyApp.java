package vampire.com.baidumusic.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/15.
 */
public class MyApp extends Application{
    private static final String TAG = "Vampire_MyApp";

    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        Log.d(TAG, "onCreate() called with: " + "");
    }

    public  static Context getContext(){
        return mContext;
    }


}
