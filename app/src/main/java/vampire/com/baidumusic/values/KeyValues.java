package vampire.com.baidumusic.values;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/18.
 */
public class KeyValues {
    private static final String TAG = "Vampire_KeyValues";

    public static String MORE_HOT_SINGER_A ="more_hot_singer_a";
    public static String MORE_HOT_SINGER_B ="more_hot_singer_b";

    public static String MY_TYPE_ID = "myTypeId";

    public static String LIST_PIC = "pic_w300";
    public static String LIST_TITLE = "title";
    public static String LIST_ID = "listid";

    public static String RANK_PIC = "pic_s210";
    public static String RANK_NAME = "name";
    public static String RANK_TYPE = "type";
    public static String RANK_ARTIST_ID = "artist_id";

    public static String SERVICE_SONG_NAME ="service_song_name";
    public static String SERVICE_SONG_AUTHOR ="service_song_author";

    public static String SONG_LIST = "song_list";


    public static String PIC_HUGE= "pic_huge";

    public static String MV_URL= "mv_url";








}
