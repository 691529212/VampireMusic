package vampire.com.baidumusic.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.volley.Request;

import java.lang.reflect.Method;

import vampire.com.baidumusic.R;
import vampire.com.baidumusic.base.MyApp;
import vampire.com.baidumusic.tools.event.ProgressBean;

/**
 * Created BaiDuMusic by *Vampire* on 16/8/25.
 */
public class PlayerNotification {
    private NotificationManagerCompat notificationManagerCompat;
    private NotificationManager notificationManager;
    private Context context;
    private Notification notification;


    public PlayerNotification(Context context) {
        this.context = context;
    }

    public Notification getNotification() {
        return notification;
    }

    public NotificationManagerCompat getNotificationManagerCompat() {
        return notificationManagerCompat;
    }

    private Notification createNotify(String author, String song, int id) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ccccccc);
        builder.setAutoCancel(false);// 点击不消失

        RemoteViews remoteViews = getRemoteViews(author, song, id);
        builder.setContent(remoteViews);

        return builder.build();
    }

    private RemoteViews getRemoteViews(String author, String song, int id) {
        // 设置 自定义布局
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.remot_notify);

        PendingIntent back = PendingIntent.getBroadcast(context, 001, new Intent("com.baidumusic.notification.back"), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent next = PendingIntent.getBroadcast(context, 001, new Intent("com.baidumusic.notification.next"), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent state = PendingIntent.getBroadcast(context, 001, new Intent("com.baidumusic.notification.state"), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent clear = PendingIntent.getBroadcast(context, 001, new Intent("com.baidumusic.notification.clear"), PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent turn = PendingIntent.getBroadcast(context, 001, new Intent("com.baidumusic.notification.turn"), PendingIntent.FLAG_CANCEL_CURRENT);

        remoteViews.setOnClickPendingIntent(R.id.notify_turn, turn);

        remoteViews.setOnClickPendingIntent(R.id.notify_back, back);
        remoteViews.setOnClickPendingIntent(R.id.notify_next, next);
        remoteViews.setOnClickPendingIntent(R.id.notify_cleab, clear);
        remoteViews.setOnClickPendingIntent(R.id.notify_state, state);

        remoteViews.setImageViewResource(R.id.notify_state, id);

        remoteViews.setTextViewText(R.id.notify_author, author);
        remoteViews.setTextViewText(R.id.notify_song, song);

        return remoteViews;

    }

    public void showNotify(String author, String song, int id) {

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManagerCompat = NotificationManagerCompat.from(context);

        //获取NotifyCation对象
        notification = createNotify(author, song, id);
        // 设置滑动不取消
        notification.flags = Notification.FLAG_NO_CLEAR;

        notification.defaults = Notification.DEFAULT_LIGHTS;
        // 设置单一通知
        notificationManager.notify(1, notification);

//        try {
//            Intent intent =new Intent(MyApp.getContext(),Class.forName("vampire.com.baidumusic.aty.main.MainActivity"));
//            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            //PendingIntent为一个特殊的Intent,通过getBroadcast或者getActivity或者getService得到.
//            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
////            notification.setLatestEventInfo(this, str, str, pendingIntent);
//            notification.flags |= Notification.FLAG_ONGOING_EVENT;
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }

    }

}
